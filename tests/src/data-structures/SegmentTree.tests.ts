QUnit.module('SegmentTree', () =>
{
  QUnit.test('should build tree for input array #0 with length of power of two', (assert) =>
  {
    const array = [-1, 2];
    const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);

    assert.deepEqual(segmentTree.segmentTree, [-1, -1, 2]);
    assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
  });

  QUnit.test('should build tree for input array #1 with length of power of two', (assert) =>
  {
    const array = [-1, 2, 4, 0];
    const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);

    assert.deepEqual(segmentTree.segmentTree, [-1, -1, 0, -1, 2, 4, 0]);
    assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
  });

  QUnit.test('should build tree for input array #0 with length not of power of two', (assert) =>
  {
    const array = [0, 1, 2];
    const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);

    assert.deepEqual(segmentTree.segmentTree, [0, 0, 2, 0, 1, null, null]);
    assert.deepEqual(segmentTree.segmentTree.length, (2 * 4) - 1);
  });

  QUnit.test('should build tree for input array #1 with length not of power of two', (assert) =>
  {
    const array = [-1, 3, 4, 0, 2, 1];
    const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);

    assert.deepEqual(segmentTree.segmentTree, [
      -1, -1, 0, -1, 4, 0, 1, -1, 3, null, null, 0, 2, null, null,
    ]);
    assert.deepEqual(segmentTree.segmentTree.length, (2 * 8) - 1);
  });

  QUnit.test('should build max array', (assert) =>
  {
    const array = [-1, 2, 4, 0];
    const segmentTree = new ds.SegmentTree(array, Math.max, -Infinity);

    assert.deepEqual(segmentTree.segmentTree, [4, 2, 4, -1, 2, 4, 0]);
    assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
  });

  QUnit.test('should build sum array', (assert) =>
  {
    const array = [-1, 2, 4, 0];
    const segmentTree = new ds.SegmentTree(array, (a, b) => (a + b), 0);

    assert.deepEqual(segmentTree.segmentTree, [5, 1, 4, -1, 2, 4, 0]);
    assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
  });

  QUnit.test('should do min range query on power of two length array', (assert) =>
  {
    const array = [-1, 3, 4, 0, 2, 1];
    const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);

    assert.deepEqual(segmentTree.rangeQuery(0, 5), -1);
    assert.deepEqual(segmentTree.rangeQuery(0, 2), -1);
    assert.deepEqual(segmentTree.rangeQuery(1, 3), 0);
    assert.deepEqual(segmentTree.rangeQuery(2, 4), 0);
    assert.deepEqual(segmentTree.rangeQuery(4, 5), 1);
    assert.deepEqual(segmentTree.rangeQuery(2, 2), 4);
  });

  QUnit.test('should do min range query on not power of two length array', (assert) =>
  {
    const array = [-1, 2, 4, 0];
    const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);

    assert.deepEqual(segmentTree.rangeQuery(0, 4), -1);
    assert.deepEqual(segmentTree.rangeQuery(0, 1), -1);
    assert.deepEqual(segmentTree.rangeQuery(1, 3), 0);
    assert.deepEqual(segmentTree.rangeQuery(1, 2), 2);
    assert.deepEqual(segmentTree.rangeQuery(2, 3), 0);
    assert.deepEqual(segmentTree.rangeQuery(2, 2), 4);
  });

  QUnit.test('should do max range query', (assert) =>
  {
    const array = [-1, 3, 4, 0, 2, 1];
    const segmentTree = new ds.SegmentTree(array, Math.max, -Infinity);

    assert.deepEqual(segmentTree.rangeQuery(0, 5), 4);
    assert.deepEqual(segmentTree.rangeQuery(0, 1), 3);
    assert.deepEqual(segmentTree.rangeQuery(1, 3), 4);
    assert.deepEqual(segmentTree.rangeQuery(2, 4), 4);
    assert.deepEqual(segmentTree.rangeQuery(4, 5), 2);
    assert.deepEqual(segmentTree.rangeQuery(3, 3), 0);
  });

  QUnit.test('should do sum range query', (assert) =>
  {
    const array = [-1, 3, 4, 0, 2, 1];
    const segmentTree = new ds.SegmentTree(array, (a, b) => (a + b), 0);

    assert.deepEqual(segmentTree.rangeQuery(0, 5), 9);
    assert.deepEqual(segmentTree.rangeQuery(0, 1), 2);
    assert.deepEqual(segmentTree.rangeQuery(1, 3), 7);
    assert.deepEqual(segmentTree.rangeQuery(2, 4), 6);
    assert.deepEqual(segmentTree.rangeQuery(4, 5), 3);
    assert.deepEqual(segmentTree.rangeQuery(3, 3), 0);
  });
});

QUnit.module('Queue', () =>
{
    QUnit.test('should create empty queue', (assert) =>
    {
        const queue = new ds.Queue();

        assert.deepEqual(queue["linkedList"] != null, true);
    });

    QUnit.test('should enqueue data to queue', (assert) =>
    {
        const queue = new ds.Queue();

        queue.enqueue(1);
        queue.enqueue(2);

        assert.deepEqual(queue.toString(), '1,2');
    });

    QUnit.test('should be possible to enqueue/dequeue objects', (assert) =>
    {
        const queue = new ds.Queue<{ value: string, key: string }>();

        queue.enqueue({ value: 'test1', key: 'key1' });
        queue.enqueue({ value: 'test2', key: 'key2' });

        const stringifier = value => `${value.key}:${value.value}`;

        assert.deepEqual(queue.toString(stringifier), 'key1:test1,key2:test2');
        assert.deepEqual(queue.dequeue().value, 'test1');
        assert.deepEqual(queue.dequeue().value, 'test2');
    });

    QUnit.test('should peek data from queue', (assert) =>
    {
        const queue = new ds.Queue();

        assert.deepEqual(queue.peek(), null);

        queue.enqueue(1);
        queue.enqueue(2);

        assert.deepEqual(queue.peek(), 1);
        assert.deepEqual(queue.peek(), 1);
    });

    QUnit.test('should check if queue is empty', (assert) =>
    {
        const queue = new ds.Queue();

        assert.deepEqual(queue.isEmpty(), true);

        queue.enqueue(1);

        assert.deepEqual(queue.isEmpty(), false);
    });

    QUnit.test('should dequeue from queue in FIFO order', (assert) =>
    {
        const queue = new ds.Queue();

        queue.enqueue(1);
        queue.enqueue(2);

        assert.deepEqual(queue.dequeue(), 1);
        assert.deepEqual(queue.dequeue(), 2);
        assert.deepEqual(queue.dequeue(), null);
        assert.deepEqual(queue.isEmpty(), true);
    });
});

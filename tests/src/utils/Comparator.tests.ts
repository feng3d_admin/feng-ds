QUnit.module('Comparator', () =>
{
    QUnit.test('should compare with default comparator function', (assert) =>
    {
        const comparator = new ds.Comparator();

        assert.deepEqual(comparator.equal(0, 0), true);
        assert.deepEqual(comparator.equal(0, 1), false);
        assert.deepEqual(comparator.equal('a', 'a'), true);
        assert.deepEqual(comparator.lessThan(1, 2), true);
        assert.deepEqual(comparator.lessThan(-1, 2), true);
        assert.deepEqual(comparator.lessThan('a', 'b'), true);
        assert.deepEqual(comparator.lessThan('a', 'ab'), true);
        assert.deepEqual(comparator.lessThan(10, 2), false);
        assert.deepEqual(comparator.lessThanOrEqual(10, 2), false);
        assert.deepEqual(comparator.lessThanOrEqual(1, 1), true);
        assert.deepEqual(comparator.lessThanOrEqual(0, 0), true);
        assert.deepEqual(comparator.greaterThan(0, 0), false);
        assert.deepEqual(comparator.greaterThan(10, 0), true);
        assert.deepEqual(comparator.greaterThanOrEqual(10, 0), true);
        assert.deepEqual(comparator.greaterThanOrEqual(10, 10), true);
        assert.deepEqual(comparator.greaterThanOrEqual(0, 10), false);
    });

    QUnit.test('should compare with custom comparator function', (assert) =>
    {
        const comparator = new ds.Comparator((a: string, b: string) =>
        {
            if (a.length === b.length)
            {
                return 0;
            }

            return a.length < b.length ? -1 : 1;
        });

        assert.deepEqual(comparator.equal('a', 'b'), true);
        assert.deepEqual(comparator.equal('a', ''), false);
        assert.deepEqual(comparator.lessThan('b', 'aa'), true);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'aa'), false);
        assert.deepEqual(comparator.greaterThanOrEqual('aa', 'a'), true);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'a'), true);

        comparator.reverse();

        assert.deepEqual(comparator.equal('a', 'b'), true);
        assert.deepEqual(comparator.equal('a', ''), false);
        assert.deepEqual(comparator.lessThan('b', 'aa'), false);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'aa'), true);
        assert.deepEqual(comparator.greaterThanOrEqual('aa', 'a'), false);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'a'), true);
    });
});

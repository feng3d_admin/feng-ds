QUnit.module('AvlTree', () => {
    QUnit.test('should do simple left-left rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(4);
        tree.insert(3);
        tree.insert(2);
        assert.deepEqual(tree.toString(), '2,3,4');
        assert.deepEqual(tree.root.value, 3);
        assert.deepEqual(tree.root.height, 1);
        tree.insert(1);
        assert.deepEqual(tree.toString(), '1,2,3,4');
        assert.deepEqual(tree.root.value, 3);
        assert.deepEqual(tree.root.height, 2);
        tree.insert(0);
        assert.deepEqual(tree.toString(), '0,1,2,3,4');
        assert.deepEqual(tree.root.value, 3);
        assert.deepEqual(tree.root.left.value, 1);
        assert.deepEqual(tree.root.height, 2);
    });
    QUnit.test('should do complex left-left rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(30);
        tree.insert(20);
        tree.insert(40);
        tree.insert(10);
        assert.deepEqual(tree.root.value, 30);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '10,20,30,40');
        tree.insert(25);
        assert.deepEqual(tree.root.value, 30);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '10,20,25,30,40');
        tree.insert(5);
        assert.deepEqual(tree.root.value, 20);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '5,10,20,25,30,40');
    });
    QUnit.test('should do simple right-right rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        assert.deepEqual(tree.toString(), '2,3,4');
        assert.deepEqual(tree.root.value, 3);
        assert.deepEqual(tree.root.height, 1);
        tree.insert(5);
        assert.deepEqual(tree.toString(), '2,3,4,5');
        assert.deepEqual(tree.root.value, 3);
        assert.deepEqual(tree.root.height, 2);
        tree.insert(6);
        assert.deepEqual(tree.toString(), '2,3,4,5,6');
        assert.deepEqual(tree.root.value, 3);
        assert.deepEqual(tree.root.right.value, 5);
        assert.deepEqual(tree.root.height, 2);
    });
    QUnit.test('should do complex right-right rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(30);
        tree.insert(20);
        tree.insert(40);
        tree.insert(50);
        assert.deepEqual(tree.root.value, 30);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '20,30,40,50');
        tree.insert(35);
        assert.deepEqual(tree.root.value, 30);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '20,30,35,40,50');
        tree.insert(55);
        assert.deepEqual(tree.root.value, 40);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '20,30,35,40,50,55');
    });
    QUnit.test('should do left-right rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(30);
        tree.insert(20);
        tree.insert(25);
        assert.deepEqual(tree.root.height, 1);
        assert.deepEqual(tree.root.value, 25);
        assert.deepEqual(tree.toString(), '20,25,30');
    });
    QUnit.test('should do right-left rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(30);
        tree.insert(40);
        tree.insert(35);
        assert.deepEqual(tree.root.height, 1);
        assert.deepEqual(tree.root.value, 35);
        assert.deepEqual(tree.toString(), '30,35,40');
    });
    QUnit.test('should create balanced tree: case #1', (assert) => {
        // @see: https://www.youtube.com/watch?v=rbg7Qf8GkQ4&t=839s
        const tree = new ds.AvlTree();
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        assert.deepEqual(tree.root.value, 2);
        assert.deepEqual(tree.root.height, 1);
        assert.deepEqual(tree.toString(), '1,2,3');
        tree.insert(6);
        assert.deepEqual(tree.root.value, 2);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '1,2,3,6');
        tree.insert(15);
        assert.deepEqual(tree.root.value, 2);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '1,2,3,6,15');
        tree.insert(-2);
        assert.deepEqual(tree.root.value, 2);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '-2,1,2,3,6,15');
        tree.insert(-5);
        assert.deepEqual(tree.root.value, 2);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '-5,-2,1,2,3,6,15');
        tree.insert(-8);
        assert.deepEqual(tree.root.value, 2);
        assert.deepEqual(tree.root.height, 3);
        assert.deepEqual(tree.toString(), '-8,-5,-2,1,2,3,6,15');
    });
    QUnit.test('should create balanced tree: case #2', (assert) => {
        // @see https://www.youtube.com/watch?v=7m94k2Qhg68
        const tree = new ds.AvlTree();
        tree.insert(43);
        tree.insert(18);
        tree.insert(22);
        tree.insert(9);
        tree.insert(21);
        tree.insert(6);
        assert.deepEqual(tree.root.value, 18);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '6,9,18,21,22,43');
        tree.insert(8);
        assert.deepEqual(tree.root.value, 18);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.toString(), '6,8,9,18,21,22,43');
    });
    QUnit.test('should do left right rotation and keeping left right node safe', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(30);
        tree.insert(15);
        tree.insert(40);
        tree.insert(10);
        tree.insert(18);
        tree.insert(35);
        tree.insert(45);
        tree.insert(5);
        tree.insert(12);
        assert.deepEqual(tree.toString(), '5,10,12,15,18,30,35,40,45');
        assert.deepEqual(tree.root.height, 3);
        tree.insert(11);
        assert.deepEqual(tree.toString(), '5,10,11,12,15,18,30,35,40,45');
        assert.deepEqual(tree.root.height, 3);
    });
    QUnit.test('should do left right rotation and keeping left right node safe', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(30);
        tree.insert(15);
        tree.insert(40);
        tree.insert(10);
        tree.insert(18);
        tree.insert(35);
        tree.insert(45);
        tree.insert(42);
        tree.insert(47);
        assert.deepEqual(tree.toString(), '10,15,18,30,35,40,42,45,47');
        assert.deepEqual(tree.root.height, 3);
        tree.insert(43);
        assert.deepEqual(tree.toString(), '10,15,18,30,35,40,42,43,45,47');
        assert.deepEqual(tree.root.height, 3);
    });
    QUnit.test('should remove values from the tree with right-right rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(10);
        tree.insert(20);
        tree.insert(30);
        tree.insert(40);
        assert.deepEqual(tree.toString(), '10,20,30,40');
        tree.remove(10);
        assert.deepEqual(tree.toString(), '20,30,40');
        assert.deepEqual(tree.root.value, 30);
        assert.deepEqual(tree.root.left.value, 20);
        assert.deepEqual(tree.root.right.value, 40);
        assert.deepEqual(tree.root.balanceFactor, 0);
    });
    QUnit.test('should remove values from the tree with left-left rotation', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(10);
        tree.insert(20);
        tree.insert(30);
        tree.insert(5);
        assert.deepEqual(tree.toString(), '5,10,20,30');
        tree.remove(30);
        assert.deepEqual(tree.toString(), '5,10,20');
        assert.deepEqual(tree.root.value, 10);
        assert.deepEqual(tree.root.left.value, 5);
        assert.deepEqual(tree.root.right.value, 20);
        assert.deepEqual(tree.root.balanceFactor, 0);
    });
    QUnit.test('should keep balance after removal', (assert) => {
        const tree = new ds.AvlTree();
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        tree.insert(5);
        tree.insert(6);
        tree.insert(7);
        tree.insert(8);
        tree.insert(9);
        assert.deepEqual(tree.toString(), '1,2,3,4,5,6,7,8,9');
        assert.deepEqual(tree.root.value, 4);
        assert.deepEqual(tree.root.height, 3);
        assert.deepEqual(tree.root.balanceFactor, -1);
        tree.remove(8);
        assert.deepEqual(tree.root.value, 4);
        assert.deepEqual(tree.root.balanceFactor, -1);
        tree.remove(9);
        assert.deepEqual(tree.contains(8), false);
        assert.deepEqual(tree.contains(9), false);
        assert.deepEqual(tree.toString(), '1,2,3,4,5,6,7');
        assert.deepEqual(tree.root.value, 4);
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.root.balanceFactor, 0);
    });
});
QUnit.module('BinarySearchTree', () => {
    QUnit.test('should create binary search tree', (assert) => {
        const bst = new ds.BinarySearchTree();
        assert.deepEqual(bst.root.value, null);
        assert.deepEqual(bst.root.left, null);
        assert.deepEqual(bst.root.right, null);
    });
    QUnit.test('should insert values', (assert) => {
        const bst = new ds.BinarySearchTree();
        const insertedNode1 = bst.insert(10);
        const insertedNode2 = bst.insert(20);
        bst.insert(5);
        assert.deepEqual(bst.toString(), '5,10,20');
        assert.deepEqual(insertedNode1.value, 10);
        assert.deepEqual(insertedNode2.value, 20);
    });
    QUnit.test('should check if value exists', (assert) => {
        const bst = new ds.BinarySearchTree();
        bst.insert(10);
        bst.insert(20);
        bst.insert(5);
        assert.deepEqual(bst.contains(20), true);
        assert.deepEqual(bst.contains(40), false);
    });
    QUnit.test('should remove nodes', (assert) => {
        const bst = new ds.BinarySearchTree();
        bst.insert(10);
        bst.insert(20);
        bst.insert(5);
        assert.deepEqual(bst.toString(), '5,10,20');
        const removed1 = bst.remove(5);
        assert.deepEqual(bst.toString(), '10,20');
        assert.deepEqual(removed1, true);
        const removed2 = bst.remove(20);
        assert.deepEqual(bst.toString(), '10');
        assert.deepEqual(removed2, true);
    });
    QUnit.test('should insert object values', (assert) => {
        const nodeValueCompareFunction = (a, b) => {
            const normalizedA = a || { value: null };
            const normalizedB = b || { value: null };
            if (normalizedA.value === normalizedB.value) {
                return 0;
            }
            return normalizedA.value < normalizedB.value ? -1 : 1;
        };
        const obj1 = { key: 'obj1', value: 1, toString: () => 'obj1' };
        const obj2 = { key: 'obj2', value: 2, toString: () => 'obj2' };
        const obj3 = { key: 'obj3', value: 3, toString: () => 'obj3' };
        const bst = new ds.BinarySearchTree(nodeValueCompareFunction);
        bst.insert(obj2);
        bst.insert(obj3);
        bst.insert(obj1);
        assert.deepEqual(bst.toString(), 'obj1,obj2,obj3');
    });
    QUnit.test('should be traversed to sorted array', (assert) => {
        const bst = new ds.BinarySearchTree();
        bst.insert(10);
        bst.insert(-10);
        bst.insert(20);
        bst.insert(-20);
        bst.insert(25);
        bst.insert(6);
        assert.deepEqual(bst.toString(), '-20,-10,6,10,20,25');
        assert.deepEqual(bst.root.height, 2);
        bst.insert(4);
        assert.deepEqual(bst.toString(), '-20,-10,4,6,10,20,25');
        assert.deepEqual(bst.root.height, 3);
    });
});
QUnit.module('BinarySearchTreeNode', () => {
    QUnit.test('should create binary search tree', (assert) => {
        const bstNode = new ds.BinarySearchTreeNode(2);
        assert.deepEqual(bstNode.value, 2);
        assert.deepEqual(bstNode.left, null);
        assert.deepEqual(bstNode.right, null);
    });
    QUnit.test('should insert in itself if it is empty', (assert) => {
        const bstNode = new ds.BinarySearchTreeNode();
        bstNode.insert(1);
        assert.deepEqual(bstNode.value, 1);
        assert.deepEqual(bstNode.left, null);
        assert.deepEqual(bstNode.right, null);
    });
    QUnit.test('should insert nodes in correct order', (assert) => {
        const bstNode = new ds.BinarySearchTreeNode(2);
        const insertedNode1 = bstNode.insert(1);
        assert.deepEqual(insertedNode1.value, 1);
        assert.deepEqual(bstNode.toString(), '1,2');
        assert.deepEqual(bstNode.contains(1), true);
        assert.deepEqual(bstNode.contains(3), false);
        const insertedNode2 = bstNode.insert(3);
        assert.deepEqual(insertedNode2.value, 3);
        assert.deepEqual(bstNode.toString(), '1,2,3');
        assert.deepEqual(bstNode.contains(3), true);
        assert.deepEqual(bstNode.contains(4), false);
        bstNode.insert(7);
        assert.deepEqual(bstNode.toString(), '1,2,3,7');
        assert.deepEqual(bstNode.contains(7), true);
        assert.deepEqual(bstNode.contains(8), false);
        bstNode.insert(4);
        assert.deepEqual(bstNode.toString(), '1,2,3,4,7');
        assert.deepEqual(bstNode.contains(4), true);
        assert.deepEqual(bstNode.contains(8), false);
        bstNode.insert(6);
        assert.deepEqual(bstNode.toString(), '1,2,3,4,6,7');
        assert.deepEqual(bstNode.contains(6), true);
        assert.deepEqual(bstNode.contains(8), false);
    });
    QUnit.test('should not insert duplicates', (assert) => {
        const bstNode = new ds.BinarySearchTreeNode(2);
        bstNode.insert(1);
        assert.deepEqual(bstNode.toString(), '1,2');
        assert.deepEqual(bstNode.contains(1), true);
        assert.deepEqual(bstNode.contains(3), false);
        bstNode.insert(1);
        assert.deepEqual(bstNode.toString(), '1,2');
        assert.deepEqual(bstNode.contains(1), true);
        assert.deepEqual(bstNode.contains(3), false);
    });
    QUnit.test('should find min node', (assert) => {
        const node = new ds.BinarySearchTreeNode(10);
        node.insert(20);
        node.insert(30);
        node.insert(5);
        node.insert(40);
        node.insert(1);
        assert.deepEqual(node.findMin() != null, true);
        assert.deepEqual(node.findMin().value, 1);
    });
    QUnit.test('should be possible to attach meta information to binary search tree nodes', (assert) => {
        const node = new ds.BinarySearchTreeNode(10);
        node.insert(20);
        const node1 = node.insert(30);
        node.insert(5);
        node.insert(40);
        const node2 = node.insert(1);
        node.meta.set('color', 'red');
        node1.meta.set('color', 'black');
        node2.meta.set('color', 'white');
        assert.deepEqual(node.meta.get('color'), 'red');
        assert.deepEqual(node.findMin() != null, true);
        assert.deepEqual(node.findMin().value, 1);
        assert.deepEqual(node.findMin().meta.get('color'), 'white');
        assert.deepEqual(node.find(30).meta.get('color'), 'black');
    });
    QUnit.test('should find node', (assert) => {
        const node = new ds.BinarySearchTreeNode(10);
        node.insert(20);
        node.insert(30);
        node.insert(5);
        node.insert(40);
        node.insert(1);
        assert.deepEqual(node.find(6), null);
        assert.deepEqual(node.find(5) != null, true);
        assert.deepEqual(node.find(5).value, 5);
    });
    QUnit.test('should remove leaf nodes', (assert) => {
        const bstRootNode = new ds.BinarySearchTreeNode();
        bstRootNode.insert(10);
        bstRootNode.insert(20);
        bstRootNode.insert(5);
        assert.deepEqual(bstRootNode.toString(), '5,10,20');
        const removed1 = bstRootNode.remove(5);
        assert.deepEqual(bstRootNode.toString(), '10,20');
        assert.deepEqual(removed1, true);
        const removed2 = bstRootNode.remove(20);
        assert.deepEqual(bstRootNode.toString(), '10');
        assert.deepEqual(removed2, true);
    });
    QUnit.test('should remove nodes with one child', (assert) => {
        const bstRootNode = new ds.BinarySearchTreeNode();
        bstRootNode.insert(10);
        bstRootNode.insert(20);
        bstRootNode.insert(5);
        bstRootNode.insert(30);
        assert.deepEqual(bstRootNode.toString(), '5,10,20,30');
        bstRootNode.remove(20);
        assert.deepEqual(bstRootNode.toString(), '5,10,30');
        bstRootNode.insert(1);
        assert.deepEqual(bstRootNode.toString(), '1,5,10,30');
        bstRootNode.remove(5);
        assert.deepEqual(bstRootNode.toString(), '1,10,30');
    });
    QUnit.test('should remove nodes with two children', (assert) => {
        const bstRootNode = new ds.BinarySearchTreeNode();
        bstRootNode.insert(10);
        bstRootNode.insert(20);
        bstRootNode.insert(5);
        bstRootNode.insert(30);
        bstRootNode.insert(15);
        bstRootNode.insert(25);
        assert.deepEqual(bstRootNode.toString(), '5,10,15,20,25,30');
        assert.deepEqual(bstRootNode.find(20).left.value, 15);
        assert.deepEqual(bstRootNode.find(20).right.value, 30);
        bstRootNode.remove(20);
        assert.deepEqual(bstRootNode.toString(), '5,10,15,25,30');
        bstRootNode.remove(15);
        assert.deepEqual(bstRootNode.toString(), '5,10,25,30');
        bstRootNode.remove(10);
        assert.deepEqual(bstRootNode.toString(), '5,25,30');
        assert.deepEqual(bstRootNode.value, 25);
        bstRootNode.remove(25);
        assert.deepEqual(bstRootNode.toString(), '5,30');
        bstRootNode.remove(5);
        assert.deepEqual(bstRootNode.toString(), '30');
    });
    QUnit.test('should remove node with no parent', (assert) => {
        const bstRootNode = new ds.BinarySearchTreeNode();
        assert.deepEqual(bstRootNode.toString(), '');
        bstRootNode.insert(1);
        bstRootNode.insert(2);
        assert.deepEqual(bstRootNode.toString(), '1,2');
        bstRootNode.remove(1);
        assert.deepEqual(bstRootNode.toString(), '2');
        bstRootNode.remove(2);
        assert.deepEqual(bstRootNode.toString(), '');
    });
    QUnit.test('should throw error when trying to remove not existing node', (assert) => {
        const bstRootNode = new ds.BinarySearchTreeNode();
        bstRootNode.insert(10);
        bstRootNode.insert(20);
        function removeNotExistingElementFromTree() {
            bstRootNode.remove(30);
        }
        var error0 = false;
        try {
            removeNotExistingElementFromTree();
        }
        catch (error) {
            error0 = true;
        }
        assert.deepEqual(error0, true);
    });
    QUnit.test('should be possible to use objects as node values', (assert) => {
        const nodeValueComparatorCallback = (a, b) => {
            const normalizedA = a || { value: null };
            const normalizedB = b || { value: null };
            if (normalizedA.value === normalizedB.value) {
                return 0;
            }
            return normalizedA.value < normalizedB.value ? -1 : 1;
        };
        const obj1 = { key: 'obj1', value: 1, toString: () => 'obj1' };
        const obj2 = { key: 'obj2', value: 2, toString: () => 'obj2' };
        const obj3 = { key: 'obj3', value: 3, toString: () => 'obj3' };
        const bstNode = new ds.BinarySearchTreeNode(obj2, nodeValueComparatorCallback);
        bstNode.insert(obj1);
        assert.deepEqual(bstNode.toString(), 'obj1,obj2');
        assert.deepEqual(bstNode.contains(obj1), true);
        assert.deepEqual(bstNode.contains(obj3), false);
        bstNode.insert(obj3);
        assert.deepEqual(bstNode.toString(), 'obj1,obj2,obj3');
        assert.deepEqual(bstNode.contains(obj3), true);
        assert.deepEqual(bstNode.findMin().value, obj1);
    });
    QUnit.test('should abandon removed node', (assert) => {
        const rootNode = new ds.BinarySearchTreeNode('foo');
        rootNode.insert('bar');
        const childNode = rootNode.find('bar');
        rootNode.remove('bar');
        assert.deepEqual(childNode.parent, null);
    });
});
QUnit.module('BinaryTreeNode', () => {
    QUnit.test('should create node', (assert) => {
        const node = new ds.BinaryTreeNode();
        assert.deepEqual(node.value, null);
        assert.deepEqual(node.left, null);
        assert.deepEqual(node.right, null);
        const leftNode = new ds.BinaryTreeNode(1);
        const rightNode = new ds.BinaryTreeNode(3);
        const rootNode = new ds.BinaryTreeNode(2);
        rootNode
            .setLeft(leftNode)
            .setRight(rightNode);
        assert.deepEqual(rootNode.value, 2);
        assert.deepEqual(rootNode.left.value, 1);
        assert.deepEqual(rootNode.right.value, 3);
    });
    QUnit.test('should set parent', (assert) => {
        const leftNode = new ds.BinaryTreeNode(1);
        const rightNode = new ds.BinaryTreeNode(3);
        const rootNode = new ds.BinaryTreeNode(2);
        rootNode
            .setLeft(leftNode)
            .setRight(rightNode);
        assert.deepEqual(rootNode.parent, null);
        assert.deepEqual(rootNode.left.parent.value, 2);
        assert.deepEqual(rootNode.right.parent.value, 2);
        assert.deepEqual(rootNode.right.parent, rootNode);
    });
    QUnit.test('should traverse node', (assert) => {
        const leftNode = new ds.BinaryTreeNode(1);
        const rightNode = new ds.BinaryTreeNode(3);
        const rootNode = new ds.BinaryTreeNode(2);
        rootNode
            .setLeft(leftNode)
            .setRight(rightNode);
        assert.deepEqual(rootNode.traverseInOrder(), [1, 2, 3]);
        assert.deepEqual(rootNode.toString(), '1,2,3');
    });
    QUnit.test('should remove child node', (assert) => {
        const leftNode = new ds.BinaryTreeNode(1);
        const rightNode = new ds.BinaryTreeNode(3);
        const rootNode = new ds.BinaryTreeNode(2);
        rootNode
            .setLeft(leftNode)
            .setRight(rightNode);
        assert.deepEqual(rootNode.traverseInOrder(), [1, 2, 3]);
        assert.deepEqual(rootNode.removeChild(rootNode.left), true);
        assert.deepEqual(rootNode.traverseInOrder(), [2, 3]);
        assert.deepEqual(rootNode.removeChild(rootNode.right), true);
        assert.deepEqual(rootNode.traverseInOrder(), [2]);
        assert.deepEqual(rootNode.removeChild(rootNode.right), false);
        assert.deepEqual(rootNode.traverseInOrder(), [2]);
    });
    QUnit.test('should replace child node', (assert) => {
        const leftNode = new ds.BinaryTreeNode(1);
        const rightNode = new ds.BinaryTreeNode(3);
        const rootNode = new ds.BinaryTreeNode(2);
        rootNode
            .setLeft(leftNode)
            .setRight(rightNode);
        assert.deepEqual(rootNode.traverseInOrder(), [1, 2, 3]);
        const replacementNode = new ds.BinaryTreeNode(5);
        rightNode.setRight(replacementNode);
        assert.deepEqual(rootNode.traverseInOrder(), [1, 2, 3, 5]);
        assert.deepEqual(rootNode.replaceChild(rootNode.right, rootNode.right.right), true);
        assert.deepEqual(rootNode.right.value, 5);
        assert.deepEqual(rootNode.right.right, null);
        assert.deepEqual(rootNode.traverseInOrder(), [1, 2, 5]);
        assert.deepEqual(rootNode.replaceChild(rootNode.right, rootNode.right.right), false);
        assert.deepEqual(rootNode.traverseInOrder(), [1, 2, 5]);
        assert.deepEqual(rootNode.replaceChild(rootNode.right, replacementNode), true);
        assert.deepEqual(rootNode.traverseInOrder(), [1, 2, 5]);
        assert.deepEqual(rootNode.replaceChild(rootNode.left, replacementNode), true);
        assert.deepEqual(rootNode.traverseInOrder(), [5, 2, 5]);
        assert.deepEqual(rootNode.replaceChild(new ds.BinaryTreeNode(), new ds.BinaryTreeNode()), false);
    });
    QUnit.test('should calculate node height', (assert) => {
        const root = new ds.BinaryTreeNode(1);
        const left = new ds.BinaryTreeNode(3);
        const right = new ds.BinaryTreeNode(2);
        const grandLeft = new ds.BinaryTreeNode(5);
        const grandRight = new ds.BinaryTreeNode(6);
        const grandGrandLeft = new ds.BinaryTreeNode(7);
        assert.deepEqual(root.height, 0);
        assert.deepEqual(root.balanceFactor, 0);
        root
            .setLeft(left)
            .setRight(right);
        assert.deepEqual(root.height, 1);
        assert.deepEqual(left.height, 0);
        assert.deepEqual(root.balanceFactor, 0);
        left
            .setLeft(grandLeft)
            .setRight(grandRight);
        assert.deepEqual(root.height, 2);
        assert.deepEqual(left.height, 1);
        assert.deepEqual(grandLeft.height, 0);
        assert.deepEqual(grandRight.height, 0);
        assert.deepEqual(root.balanceFactor, 1);
        grandLeft.setLeft(grandGrandLeft);
        assert.deepEqual(root.height, 3);
        assert.deepEqual(left.height, 2);
        assert.deepEqual(grandLeft.height, 1);
        assert.deepEqual(grandRight.height, 0);
        assert.deepEqual(grandGrandLeft.height, 0);
        assert.deepEqual(root.balanceFactor, 2);
    });
    QUnit.test('should calculate node height for right nodes as well', (assert) => {
        const root = new ds.BinaryTreeNode(1);
        const right = new ds.BinaryTreeNode(2);
        root.setRight(right);
        assert.deepEqual(root.height, 1);
        assert.deepEqual(right.height, 0);
        assert.deepEqual(root.balanceFactor, -1);
    });
    QUnit.test('should set null for left and right node', (assert) => {
        const root = new ds.BinaryTreeNode(2);
        const left = new ds.BinaryTreeNode(1);
        const right = new ds.BinaryTreeNode(3);
        root.setLeft(left);
        root.setRight(right);
        assert.deepEqual(root.left.value, 1);
        assert.deepEqual(root.right.value, 3);
        root.setLeft(null);
        root.setRight(null);
        assert.deepEqual(root.left, null);
        assert.deepEqual(root.right, null);
    });
    QUnit.test('should be possible to create node with object as a value', (assert) => {
        const obj1 = { key: 'object_1', toString: () => 'object_1' };
        const obj2 = { key: 'object_2' };
        const node1 = new ds.BinaryTreeNode(obj1);
        const node2 = new ds.BinaryTreeNode(obj2);
        node1.setLeft(node2);
        assert.deepEqual(node1.value, obj1);
        assert.deepEqual(node2.value, obj2);
        assert.deepEqual(node1.left.value, obj2);
        node1.removeChild(node2);
        assert.deepEqual(node1.value, obj1);
        assert.deepEqual(node2.value, obj2);
        assert.deepEqual(node1.left, null);
        assert.deepEqual(node1.toString(), 'object_1');
        assert.deepEqual(node2.toString(), '[object Object]');
    });
    QUnit.test('should be possible to attach meta information to the node', (assert) => {
        const redNode = new ds.BinaryTreeNode(1);
        const blackNode = new ds.BinaryTreeNode(2);
        redNode.meta.set('color', 'red');
        blackNode.meta.set('color', 'black');
        assert.deepEqual(redNode.meta.get('color'), 'red');
        assert.deepEqual(blackNode.meta.get('color'), 'black');
    });
    QUnit.test('should detect right uncle', (assert) => {
        const grandParent = new ds.BinaryTreeNode('grand-parent');
        const parent = new ds.BinaryTreeNode('parent');
        const uncle = new ds.BinaryTreeNode('uncle');
        const child = new ds.BinaryTreeNode('child');
        assert.deepEqual(grandParent.uncle, undefined);
        assert.deepEqual(parent.uncle, undefined);
        grandParent.setLeft(parent);
        assert.deepEqual(parent.uncle, undefined);
        assert.deepEqual(child.uncle, undefined);
        parent.setLeft(child);
        assert.deepEqual(child.uncle, undefined);
        grandParent.setRight(uncle);
        assert.deepEqual(parent.uncle, undefined);
        assert.deepEqual(child.uncle != undefined, true);
        assert.deepEqual(child.uncle, uncle);
    });
    QUnit.test('should detect left uncle', (assert) => {
        const grandParent = new ds.BinaryTreeNode('grand-parent');
        const parent = new ds.BinaryTreeNode('parent');
        const uncle = new ds.BinaryTreeNode('uncle');
        const child = new ds.BinaryTreeNode('child');
        assert.deepEqual(grandParent.uncle, undefined);
        assert.deepEqual(parent.uncle, undefined);
        grandParent.setRight(parent);
        assert.deepEqual(parent.uncle, undefined);
        assert.deepEqual(child.uncle, undefined);
        parent.setRight(child);
        assert.deepEqual(child.uncle, undefined);
        grandParent.setLeft(uncle);
        assert.deepEqual(parent.uncle, undefined);
        assert.deepEqual(child.uncle != undefined, true);
        assert.deepEqual(child.uncle, uncle);
    });
    QUnit.test('should be possible to set node values', (assert) => {
        const node = new ds.BinaryTreeNode('initial_value');
        assert.deepEqual(node.value, 'initial_value');
        node.setValue('new_value');
        assert.deepEqual(node.value, 'new_value');
    });
    QUnit.test('should be possible to copy node', (assert) => {
        const root = new ds.BinaryTreeNode('root');
        const left = new ds.BinaryTreeNode('left');
        const right = new ds.BinaryTreeNode('right');
        root
            .setLeft(left)
            .setRight(right);
        assert.deepEqual(root.toString(), 'left,root,right');
        const newRoot = new ds.BinaryTreeNode('new_root');
        const newLeft = new ds.BinaryTreeNode('new_left');
        const newRight = new ds.BinaryTreeNode('new_right');
        newRoot
            .setLeft(newLeft)
            .setRight(newRight);
        assert.deepEqual(newRoot.toString(), 'new_left,new_root,new_right');
        ds.BinaryTreeNode.copyNode(root, newRoot);
        assert.deepEqual(root.toString(), 'left,root,right');
        assert.deepEqual(newRoot.toString(), 'left,root,right');
    });
});
QUnit.module('BloomFilter', () => {
    let bloomFilter;
    const people = [
        'Bruce Wayne',
        'Clark Kent',
        'Barry Allen',
    ];
    QUnit.test('should have methods named "insert" and "mayContain"', (assert) => {
        var bloomFilter = new ds.BloomFilter();
        assert.deepEqual(typeof bloomFilter.insert, 'function');
        assert.deepEqual(typeof bloomFilter.mayContain, 'function');
    });
    QUnit.test('should create a new filter store with the appropriate methods', (assert) => {
        var bloomFilter = new ds.BloomFilter();
        const store = bloomFilter.createStore(18);
        assert.deepEqual(typeof store.getValue, 'function');
        assert.deepEqual(typeof store.setValue, 'function');
    });
    QUnit.test('should hash deterministically with all 3 hash functions', (assert) => {
        var bloomFilter = new ds.BloomFilter();
        const str1 = 'apple';
        assert.deepEqual(bloomFilter.hash1(str1), bloomFilter.hash1(str1));
        assert.deepEqual(bloomFilter.hash2(str1), bloomFilter.hash2(str1));
        assert.deepEqual(bloomFilter.hash3(str1), bloomFilter.hash3(str1));
        assert.deepEqual(bloomFilter.hash1(str1), 14);
        assert.deepEqual(bloomFilter.hash2(str1), 43);
        assert.deepEqual(bloomFilter.hash3(str1), 10);
        const str2 = 'orange';
        assert.deepEqual(bloomFilter.hash1(str2), bloomFilter.hash1(str2));
        assert.deepEqual(bloomFilter.hash2(str2), bloomFilter.hash2(str2));
        assert.deepEqual(bloomFilter.hash3(str2), bloomFilter.hash3(str2));
        assert.deepEqual(bloomFilter.hash1(str2), 0);
        assert.deepEqual(bloomFilter.hash2(str2), 61);
        assert.deepEqual(bloomFilter.hash3(str2), 10);
    });
    QUnit.test('should create an array with 3 hash values', (assert) => {
        var bloomFilter = new ds.BloomFilter();
        assert.deepEqual(bloomFilter.getHashValues('abc').length, 3);
        assert.deepEqual(bloomFilter.getHashValues('abc'), [66, 63, 54]);
    });
    QUnit.test('should insert strings correctly and return true when checking for inserted values', (assert) => {
        var bloomFilter = new ds.BloomFilter();
        people.forEach(person => bloomFilter.insert(person));
        assert.deepEqual(bloomFilter.mayContain('Bruce Wayne'), true);
        assert.deepEqual(bloomFilter.mayContain('Clark Kent'), true);
        assert.deepEqual(bloomFilter.mayContain('Barry Allen'), true);
        assert.deepEqual(bloomFilter.mayContain('Tony Stark'), false);
    });
});
QUnit.module('DisjointSet', () => {
    QUnit.test('should throw error when trying to union and check not existing sets', (assert) => {
        function mergeNotExistingSets() {
            const disjointSet = new ds.DisjointSet();
            disjointSet.union('A', 'B');
        }
        function checkNotExistingSets() {
            const disjointSet = new ds.DisjointSet();
            disjointSet.inSameSet('A', 'B');
        }
        var error0 = false, error1 = false;
        try {
            mergeNotExistingSets();
        }
        catch (error) {
            error0 = true;
        }
        try {
            checkNotExistingSets();
        }
        catch (error) {
            error1 = true;
        }
        assert.deepEqual(error0, true);
        assert.deepEqual(error1, true);
    });
    QUnit.test('should do basic manipulations on disjoint set', (assert) => {
        const disjointSet = new ds.DisjointSet();
        assert.deepEqual(disjointSet.find('A'), null);
        assert.deepEqual(disjointSet.find('B'), null);
        disjointSet.makeSet('A');
        assert.deepEqual(disjointSet.find('A'), 'A');
        assert.deepEqual(disjointSet.find('B'), null);
        disjointSet.makeSet('B');
        assert.deepEqual(disjointSet.find('A'), 'A');
        assert.deepEqual(disjointSet.find('B'), 'B');
        disjointSet.makeSet('C');
        assert.deepEqual(disjointSet.inSameSet('A', 'B'), false);
        disjointSet.union('A', 'B');
        assert.deepEqual(disjointSet.find('A'), 'A');
        assert.deepEqual(disjointSet.find('B'), 'A');
        assert.deepEqual(disjointSet.inSameSet('A', 'B'), true);
        assert.deepEqual(disjointSet.inSameSet('B', 'A'), true);
        assert.deepEqual(disjointSet.inSameSet('A', 'C'), false);
        disjointSet.union('A', 'A');
        disjointSet.union('B', 'C');
        assert.deepEqual(disjointSet.find('A'), 'A');
        assert.deepEqual(disjointSet.find('B'), 'A');
        assert.deepEqual(disjointSet.find('C'), 'A');
        assert.deepEqual(disjointSet.inSameSet('A', 'B'), true);
        assert.deepEqual(disjointSet.inSameSet('B', 'C'), true);
        assert.deepEqual(disjointSet.inSameSet('A', 'C'), true);
        disjointSet
            .makeSet('E')
            .makeSet('F')
            .makeSet('G')
            .makeSet('H')
            .makeSet('I');
        disjointSet
            .union('E', 'F')
            .union('F', 'G')
            .union('G', 'H')
            .union('H', 'I');
        assert.deepEqual(disjointSet.inSameSet('A', 'I'), false);
        assert.deepEqual(disjointSet.inSameSet('E', 'I'), true);
        disjointSet.union('I', 'C');
        assert.deepEqual(disjointSet.find('I'), 'E');
        assert.deepEqual(disjointSet.inSameSet('A', 'I'), true);
    });
    QUnit.test('should union smaller set with bigger one making bigger one to be new root', (assert) => {
        const disjointSet = new ds.DisjointSet();
        disjointSet
            .makeSet('A')
            .makeSet('B')
            .makeSet('C')
            .union('B', 'C')
            .union('A', 'C');
        assert.deepEqual(disjointSet.find('A'), 'B');
    });
    QUnit.test('should do basic manipulations on disjoint set with custom key extractor', (assert) => {
        const keyExtractor = value => value.key;
        const disjointSet = new ds.DisjointSet(keyExtractor);
        const itemA = { key: 'A', value: 1 };
        const itemB = { key: 'B', value: 2 };
        const itemC = { key: 'C', value: 3 };
        assert.deepEqual(disjointSet.find(itemA), null);
        assert.deepEqual(disjointSet.find(itemB), null);
        disjointSet.makeSet(itemA);
        assert.deepEqual(disjointSet.find(itemA), 'A');
        assert.deepEqual(disjointSet.find(itemB), null);
        disjointSet.makeSet(itemB);
        assert.deepEqual(disjointSet.find(itemA), 'A');
        assert.deepEqual(disjointSet.find(itemB), 'B');
        disjointSet.makeSet(itemC);
        assert.deepEqual(disjointSet.inSameSet(itemA, itemB), false);
        disjointSet.union(itemA, itemB);
        assert.deepEqual(disjointSet.find(itemA), 'A');
        assert.deepEqual(disjointSet.find(itemB), 'A');
        assert.deepEqual(disjointSet.inSameSet(itemA, itemB), true);
        assert.deepEqual(disjointSet.inSameSet(itemB, itemA), true);
        assert.deepEqual(disjointSet.inSameSet(itemA, itemC), false);
        disjointSet.union(itemA, itemC);
        assert.deepEqual(disjointSet.find(itemA), 'A');
        assert.deepEqual(disjointSet.find(itemB), 'A');
        assert.deepEqual(disjointSet.find(itemC), 'A');
        assert.deepEqual(disjointSet.inSameSet(itemA, itemB), true);
        assert.deepEqual(disjointSet.inSameSet(itemB, itemC), true);
        assert.deepEqual(disjointSet.inSameSet(itemA, itemC), true);
    });
});
QUnit.module('DisjointSetItem', () => {
    QUnit.test('should do basic manipulation with disjoint set item', (assert) => {
        const itemA = new ds.DisjointSetItem('A');
        const itemB = new ds.DisjointSetItem('B');
        const itemC = new ds.DisjointSetItem('C');
        const itemD = new ds.DisjointSetItem('D');
        assert.deepEqual(itemA.getRank(), 0);
        assert.deepEqual(itemA.getChildren(), []);
        assert.deepEqual(itemA.getKey(), 'A');
        assert.deepEqual(itemA.getRoot(), itemA);
        assert.deepEqual(itemA.isRoot(), true);
        assert.deepEqual(itemB.isRoot(), true);
        itemA.addChild(itemB);
        itemD.setParent(itemC);
        assert.deepEqual(itemA.getRank(), 1);
        assert.deepEqual(itemC.getRank(), 1);
        assert.deepEqual(itemB.getRank(), 0);
        assert.deepEqual(itemD.getRank(), 0);
        assert.deepEqual(itemA.getChildren().length, 1);
        assert.deepEqual(itemC.getChildren().length, 1);
        assert.deepEqual(itemA.getChildren()[0], itemB);
        assert.deepEqual(itemC.getChildren()[0], itemD);
        assert.deepEqual(itemB.getChildren().length, 0);
        assert.deepEqual(itemD.getChildren().length, 0);
        assert.deepEqual(itemA.getRoot(), itemA);
        assert.deepEqual(itemB.getRoot(), itemA);
        assert.deepEqual(itemC.getRoot(), itemC);
        assert.deepEqual(itemD.getRoot(), itemC);
        assert.deepEqual(itemA.isRoot(), true);
        assert.deepEqual(itemB.isRoot(), false);
        assert.deepEqual(itemC.isRoot(), true);
        assert.deepEqual(itemD.isRoot(), false);
        itemA.addChild(itemC);
        assert.deepEqual(itemA.isRoot(), true);
        assert.deepEqual(itemB.isRoot(), false);
        assert.deepEqual(itemC.isRoot(), false);
        assert.deepEqual(itemD.isRoot(), false);
        assert.deepEqual(itemA.getRank(), 3);
        assert.deepEqual(itemB.getRank(), 0);
        assert.deepEqual(itemC.getRank(), 1);
    });
    QUnit.test('should do basic manipulation with disjoint set item with custom key extractor', (assert) => {
        const keyExtractor = (value) => {
            return value.key;
        };
        const itemA = new ds.DisjointSetItem({ key: 'A', value: 1 }, keyExtractor);
        const itemB = new ds.DisjointSetItem({ key: 'B', value: 2 }, keyExtractor);
        const itemC = new ds.DisjointSetItem({ key: 'C', value: 3 }, keyExtractor);
        const itemD = new ds.DisjointSetItem({ key: 'D', value: 4 }, keyExtractor);
        assert.deepEqual(itemA.getRank(), 0);
        assert.deepEqual(itemA.getChildren(), []);
        assert.deepEqual(itemA.getKey(), 'A');
        assert.deepEqual(itemA.getRoot(), itemA);
        assert.deepEqual(itemA.isRoot(), true);
        assert.deepEqual(itemB.isRoot(), true);
        itemA.addChild(itemB);
        itemD.setParent(itemC);
        assert.deepEqual(itemA.getRank(), 1);
        assert.deepEqual(itemC.getRank(), 1);
        assert.deepEqual(itemB.getRank(), 0);
        assert.deepEqual(itemD.getRank(), 0);
        assert.deepEqual(itemA.getChildren().length, 1);
        assert.deepEqual(itemC.getChildren().length, 1);
        assert.deepEqual(itemA.getChildren()[0], itemB);
        assert.deepEqual(itemC.getChildren()[0], itemD);
        assert.deepEqual(itemB.getChildren().length, 0);
        assert.deepEqual(itemD.getChildren().length, 0);
        assert.deepEqual(itemA.getRoot(), itemA);
        assert.deepEqual(itemB.getRoot(), itemA);
        assert.deepEqual(itemC.getRoot(), itemC);
        assert.deepEqual(itemD.getRoot(), itemC);
        assert.deepEqual(itemA.isRoot(), true);
        assert.deepEqual(itemB.isRoot(), false);
        assert.deepEqual(itemC.isRoot(), true);
        assert.deepEqual(itemD.isRoot(), false);
        itemA.addChild(itemC);
        assert.deepEqual(itemA.isRoot(), true);
        assert.deepEqual(itemB.isRoot(), false);
        assert.deepEqual(itemC.isRoot(), false);
        assert.deepEqual(itemD.isRoot(), false);
        assert.deepEqual(itemA.getRank(), 3);
        assert.deepEqual(itemB.getRank(), 0);
        assert.deepEqual(itemC.getRank(), 1);
    });
});
QUnit.module('DoublyLinkedList', () => {
    QUnit.test('should create empty linked list', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        assert.deepEqual(linkedList.toString(), '');
    });
    QUnit.test('should append node to linked list', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        assert.deepEqual(linkedList.head, null);
        assert.deepEqual(linkedList.tail, null);
        linkedList.append(1);
        linkedList.append(2);
        assert.deepEqual(linkedList.head.next.value, 2);
        assert.deepEqual(linkedList.tail.previous.value, 1);
        assert.deepEqual(linkedList.toString(), '1,2');
    });
    QUnit.test('should prepend node to linked list', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        linkedList.prepend(2);
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 2);
        linkedList.append(1);
        linkedList.prepend(3);
        assert.deepEqual(linkedList.head.next.next.previous, linkedList.head.next);
        assert.deepEqual(linkedList.tail.previous.next, linkedList.tail);
        assert.deepEqual(linkedList.tail.previous.value, 2);
        assert.deepEqual(linkedList.toString(), '3,2,1');
    });
    QUnit.test('should create linked list from array', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        linkedList.fromArray([1, 1, 2, 3, 3, 3, 4, 5]);
        assert.deepEqual(linkedList.toString(), '1,1,2,3,3,3,4,5');
    });
    QUnit.test('should delete node by value from linked list', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        assert.deepEqual(linkedList.deleteAll(5), null);
        linkedList.append(1);
        linkedList.append(1);
        linkedList.append(2);
        linkedList.append(3);
        linkedList.append(3);
        linkedList.append(3);
        linkedList.append(4);
        linkedList.append(5);
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 5);
        const deletedNode = linkedList.deleteAll(3);
        assert.deepEqual(deletedNode.value, 3);
        assert.deepEqual(linkedList.tail.previous.previous.value, 2);
        assert.deepEqual(linkedList.toString(), '1,1,2,4,5');
        linkedList.deleteAll(3);
        assert.deepEqual(linkedList.toString(), '1,1,2,4,5');
        linkedList.deleteAll(1);
        assert.deepEqual(linkedList.toString(), '2,4,5');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.head.next.next, linkedList.tail);
        assert.deepEqual(linkedList.tail.previous.previous, linkedList.head);
        assert.deepEqual(linkedList.tail.value, 5);
        linkedList.deleteAll(5);
        assert.deepEqual(linkedList.toString(), '2,4');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 4);
        linkedList.deleteAll(4);
        assert.deepEqual(linkedList.toString(), '2');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 2);
        assert.deepEqual(linkedList.head, linkedList.tail);
        linkedList.deleteAll(2);
        assert.deepEqual(linkedList.toString(), '');
    });
    QUnit.test('should delete linked list tail', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        assert.deepEqual(linkedList.deleteTail(), null);
        linkedList.append(1);
        linkedList.append(2);
        linkedList.append(3);
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 3);
        const deletedNode1 = linkedList.deleteTail();
        assert.deepEqual(deletedNode1, 3);
        assert.deepEqual(linkedList.toString(), '1,2');
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 2);
        const deletedNode2 = linkedList.deleteTail();
        assert.deepEqual(deletedNode2, 2);
        assert.deepEqual(linkedList.toString(), '1');
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 1);
        const deletedNode3 = linkedList.deleteTail();
        assert.deepEqual(deletedNode3, 1);
        assert.deepEqual(linkedList.toString(), '');
        assert.deepEqual(linkedList.head, null);
        assert.deepEqual(linkedList.tail, null);
    });
    QUnit.test('should delete linked list head', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        assert.deepEqual(linkedList.deleteHead(), null);
        linkedList.append(1);
        linkedList.append(2);
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 2);
        const deletedNode1 = linkedList.deleteHead();
        assert.deepEqual(deletedNode1, 1);
        assert.deepEqual(linkedList.head.previous, null);
        assert.deepEqual(linkedList.toString(), '2');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 2);
        const deletedNode2 = linkedList.deleteHead();
        assert.deepEqual(deletedNode2, 2);
        assert.deepEqual(linkedList.toString(), '');
        assert.deepEqual(linkedList.head, null);
        assert.deepEqual(linkedList.tail, null);
    });
    QUnit.test('should be possible to store objects in the list and to print them out', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        const nodeValue1 = { value: 1, key: 'key1' };
        const nodeValue2 = { value: 2, key: 'key2' };
        linkedList
            .append(nodeValue1)
            .prepend(nodeValue2);
        const nodeStringifier = value => `${value.key}:${value.value}`;
        assert.deepEqual(linkedList.toString(nodeStringifier), 'key2:2,key1:1');
    });
    QUnit.test('should find node by value', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        assert.deepEqual(linkedList.find(5), null);
        linkedList.append(1);
        assert.deepEqual(linkedList.find(1) != null, true);
        linkedList
            .append(2)
            .append(3);
        const node = linkedList.find(2);
        assert.deepEqual(node.value, 2);
        assert.deepEqual(linkedList.find(5), null);
    });
    QUnit.test('should find node by callback', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        linkedList
            .append({ value: 1, key: 'test1' })
            .append({ value: 2, key: 'test2' })
            .append({ value: 3, key: 'test3' });
        const node = linkedList.findByFunc(value => value.key === 'test2');
        assert.deepEqual(node != null, true);
        assert.deepEqual(node.value.value, 2);
        assert.deepEqual(node.value.key, 'test2');
        assert.deepEqual(linkedList.findByFunc(value => value.key === 'test5'), null);
    });
    QUnit.test('should find node by means of custom compare function', (assert) => {
        const comparatorFunction = (a, b) => {
            if (a.customValue === b.customValue) {
                return 0;
            }
            return a.customValue < b.customValue ? -1 : 1;
        };
        const linkedList = new ds.DoublyLinkedList(comparatorFunction);
        linkedList
            .append({ value: 1, customValue: 'test1' })
            .append({ value: 2, customValue: 'test2' })
            .append({ value: 3, customValue: 'test3' });
        const node = linkedList.find({ value: 2, customValue: 'test2' });
        assert.deepEqual(node != null, true);
        assert.deepEqual(node.value.value, 2);
        assert.deepEqual(node.value.customValue, 'test2');
        assert.deepEqual(linkedList.find({ value: 2, customValue: 'test5' }), null);
    });
    QUnit.test('should reverse linked list', (assert) => {
        const linkedList = new ds.DoublyLinkedList();
        // Add test values to linked list.
        linkedList
            .append(1)
            .append(2)
            .append(3)
            .append(4);
        assert.deepEqual(linkedList.toString(), '1,2,3,4');
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 4);
        // Reverse linked list.
        linkedList.reverse();
        assert.deepEqual(linkedList.toString(), '4,3,2,1');
        assert.deepEqual(linkedList.head.previous, null);
        assert.deepEqual(linkedList.head.value, 4);
        assert.deepEqual(linkedList.head.next.value, 3);
        assert.deepEqual(linkedList.head.next.next.value, 2);
        assert.deepEqual(linkedList.head.next.next.next.value, 1);
        assert.deepEqual(linkedList.tail.next, null);
        assert.deepEqual(linkedList.tail.value, 1);
        assert.deepEqual(linkedList.tail.previous.value, 2);
        assert.deepEqual(linkedList.tail.previous.previous.value, 3);
        assert.deepEqual(linkedList.tail.previous.previous.previous.value, 4);
        // Reverse linked list back to initial state.
        linkedList.reverse();
        assert.deepEqual(linkedList.toString(), '1,2,3,4');
        assert.deepEqual(linkedList.head.previous, null);
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.head.next.value, 2);
        assert.deepEqual(linkedList.head.next.next.value, 3);
        assert.deepEqual(linkedList.head.next.next.next.value, 4);
        assert.deepEqual(linkedList.tail.next, null);
        assert.deepEqual(linkedList.tail.value, 4);
        assert.deepEqual(linkedList.tail.previous.value, 3);
        assert.deepEqual(linkedList.tail.previous.previous.value, 2);
        assert.deepEqual(linkedList.tail.previous.previous.previous.value, 1);
    });
});
QUnit.module('FenwickTree', () => {
    QUnit.test('should create empty fenwick tree of correct size', (assert) => {
        const tree1 = new ds.FenwickTree(5);
        assert.deepEqual(tree1.treeArray.length, 5 + 1);
        for (let i = 0; i < 5; i += 1) {
            assert.deepEqual(tree1.treeArray[i], 0);
        }
        const tree2 = new ds.FenwickTree(50);
        assert.deepEqual(tree2.treeArray.length, 50 + 1);
    });
    QUnit.test('should create correct fenwick tree', (assert) => {
        const inputArray = [3, 2, -1, 6, 5, 4, -3, 3, 7, 2, 3];
        const tree = new ds.FenwickTree(inputArray.length);
        assert.deepEqual(tree.treeArray.length, inputArray.length + 1);
        inputArray.forEach((value, index) => {
            tree.increase(index + 1, value);
        });
        assert.deepEqual(tree.treeArray, [0, 3, 5, -1, 10, 5, 9, -3, 19, 7, 9, 3]);
        assert.deepEqual(tree.query(1), 3);
        assert.deepEqual(tree.query(2), 5);
        assert.deepEqual(tree.query(3), 4);
        assert.deepEqual(tree.query(4), 10);
        assert.deepEqual(tree.query(5), 15);
        assert.deepEqual(tree.query(6), 19);
        assert.deepEqual(tree.query(7), 16);
        assert.deepEqual(tree.query(8), 19);
        assert.deepEqual(tree.query(9), 26);
        assert.deepEqual(tree.query(10), 28);
        assert.deepEqual(tree.query(11), 31);
        assert.deepEqual(tree.queryRange(1, 1), 3);
        assert.deepEqual(tree.queryRange(1, 2), 5);
        assert.deepEqual(tree.queryRange(2, 4), 7);
        assert.deepEqual(tree.queryRange(6, 9), 11);
        tree.increase(3, 1);
        assert.deepEqual(tree.query(1), 3);
        assert.deepEqual(tree.query(2), 5);
        assert.deepEqual(tree.query(3), 5);
        assert.deepEqual(tree.query(4), 11);
        assert.deepEqual(tree.query(5), 16);
        assert.deepEqual(tree.query(6), 20);
        assert.deepEqual(tree.query(7), 17);
        assert.deepEqual(tree.query(8), 20);
        assert.deepEqual(tree.query(9), 27);
        assert.deepEqual(tree.query(10), 29);
        assert.deepEqual(tree.query(11), 32);
        assert.deepEqual(tree.queryRange(1, 1), 3);
        assert.deepEqual(tree.queryRange(1, 2), 5);
        assert.deepEqual(tree.queryRange(2, 4), 8);
        assert.deepEqual(tree.queryRange(6, 9), 11);
    });
    QUnit.test('should correctly execute queries', (assert) => {
        const tree = new ds.FenwickTree(5);
        tree.increase(1, 4);
        tree.increase(3, 7);
        assert.deepEqual(tree.query(1), 4);
        assert.deepEqual(tree.query(3), 11);
        assert.deepEqual(tree.query(5), 11);
        assert.deepEqual(tree.queryRange(2, 3), 7);
        tree.increase(2, 5);
        assert.deepEqual(tree.query(5), 16);
        tree.increase(1, 3);
        assert.deepEqual(tree.queryRange(1, 1), 7);
        assert.deepEqual(tree.query(5), 19);
        assert.deepEqual(tree.queryRange(1, 5), 19);
    });
    QUnit.test('should throw exceptions', (assert) => {
        const tree = new ds.FenwickTree(5);
        const increaseAtInvalidLowIndex = () => {
            tree.increase(0, 1);
        };
        const increaseAtInvalidHighIndex = () => {
            tree.increase(10, 1);
        };
        const queryInvalidLowIndex = () => {
            tree.query(0);
        };
        const queryInvalidHighIndex = () => {
            tree.query(10);
        };
        const rangeQueryInvalidIndex = () => {
            tree.queryRange(3, 2);
        };
        var error0 = false;
        var error1 = false;
        var error2 = false;
        var error3 = false;
        var error4 = false;
        try {
            increaseAtInvalidLowIndex();
        }
        catch (error) {
            error0 = true;
        }
        try {
            increaseAtInvalidHighIndex();
        }
        catch (error) {
            error1 = true;
        }
        try {
            queryInvalidLowIndex();
        }
        catch (error) {
            error2 = true;
        }
        try {
            queryInvalidHighIndex();
        }
        catch (error) {
            error3 = true;
        }
        try {
            rangeQueryInvalidIndex();
        }
        catch (error) {
            error4 = true;
        }
        assert.deepEqual(error0, true);
        assert.deepEqual(error1, true);
        assert.deepEqual(error2, true);
        assert.deepEqual(error3, true);
        assert.deepEqual(error4, true);
    });
});
QUnit.module('Graph', () => {
    QUnit.test('should add vertices to graph', (assert) => {
        const graph = new ds.Graph();
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        graph
            .addVertex(vertexA)
            .addVertex(vertexB);
        assert.deepEqual(graph.toString(), 'A,B');
        assert.deepEqual(graph.getVertexByKey(vertexA.getKey()), vertexA);
        assert.deepEqual(graph.getVertexByKey(vertexB.getKey()), vertexB);
    });
    QUnit.test('should add edges to undirected graph', (assert) => {
        const graph = new ds.Graph();
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        graph.addEdge(edgeAB);
        assert.deepEqual(graph.getAllVertices().length, 2);
        assert.deepEqual(graph.getAllVertices()[0], vertexA);
        assert.deepEqual(graph.getAllVertices()[1], vertexB);
        const graphVertexA = graph.getVertexByKey(vertexA.getKey());
        const graphVertexB = graph.getVertexByKey(vertexB.getKey());
        assert.deepEqual(graph.toString(), 'A,B');
        assert.deepEqual(graphVertexA != undefined, true);
        assert.deepEqual(graphVertexB != undefined, true);
        assert.deepEqual(graph.getVertexByKey('not existing') == undefined, true);
        assert.deepEqual(graphVertexA.getNeighbors().length, 1);
        assert.deepEqual(graphVertexA.getNeighbors()[0], vertexB);
        assert.deepEqual(graphVertexA.getNeighbors()[0], graphVertexB);
        assert.deepEqual(graphVertexB.getNeighbors().length, 1);
        assert.deepEqual(graphVertexB.getNeighbors()[0], vertexA);
        assert.deepEqual(graphVertexB.getNeighbors()[0], graphVertexA);
    });
    QUnit.test('should add edges to directed graph', (assert) => {
        const graph = new ds.Graph(true);
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        graph.addEdge(edgeAB);
        const graphVertexA = graph.getVertexByKey(vertexA.getKey());
        const graphVertexB = graph.getVertexByKey(vertexB.getKey());
        assert.deepEqual(graph.toString(), 'A,B');
        assert.deepEqual(graphVertexA != undefined, true);
        assert.deepEqual(graphVertexB != undefined, true);
        assert.deepEqual(graphVertexA.getNeighbors().length, 1);
        assert.deepEqual(graphVertexA.getNeighbors()[0], vertexB);
        assert.deepEqual(graphVertexA.getNeighbors()[0], graphVertexB);
        assert.deepEqual(graphVertexB.getNeighbors().length, 0);
    });
    QUnit.test('should find edge by vertices in undirected graph', (assert) => {
        const graph = new ds.Graph();
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB, 10);
        graph.addEdge(edgeAB);
        const graphEdgeAB = graph.findEdge(vertexA, vertexB);
        const graphEdgeBA = graph.findEdge(vertexB, vertexA);
        const graphEdgeAC = graph.findEdge(vertexA, vertexC);
        const graphEdgeCA = graph.findEdge(vertexC, vertexA);
        assert.deepEqual(graphEdgeAC, null);
        assert.deepEqual(graphEdgeCA, null);
        assert.deepEqual(graphEdgeAB, edgeAB);
        assert.deepEqual(graphEdgeBA, edgeAB);
        assert.deepEqual(graphEdgeAB.weight, 10);
    });
    QUnit.test('should find edge by vertices in directed graph', (assert) => {
        const graph = new ds.Graph(true);
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB, 10);
        graph.addEdge(edgeAB);
        const graphEdgeAB = graph.findEdge(vertexA, vertexB);
        const graphEdgeBA = graph.findEdge(vertexB, vertexA);
        const graphEdgeAC = graph.findEdge(vertexA, vertexC);
        const graphEdgeCA = graph.findEdge(vertexC, vertexA);
        assert.deepEqual(graphEdgeAC, null);
        assert.deepEqual(graphEdgeCA, null);
        assert.deepEqual(graphEdgeBA, null);
        assert.deepEqual(graphEdgeAB, edgeAB);
        assert.deepEqual(graphEdgeAB.weight, 10);
    });
    QUnit.test('should return vertex neighbors', (assert) => {
        const graph = new ds.Graph(true);
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeAC = new ds.GraphEdge(vertexA, vertexC);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeAC);
        const neighbors = graph.getNeighbors(vertexA);
        assert.deepEqual(neighbors.length, 2);
        assert.deepEqual(neighbors[0], vertexB);
        assert.deepEqual(neighbors[1], vertexC);
    });
    QUnit.test('should throw an error when trying to add edge twice', (assert) => {
        function addSameEdgeTwice() {
            const graph = new ds.Graph(true);
            const vertexA = new ds.GraphVertex('A');
            const vertexB = new ds.GraphVertex('B');
            const edgeAB = new ds.GraphEdge(vertexA, vertexB);
            graph
                .addEdge(edgeAB)
                .addEdge(edgeAB);
        }
        var error0 = false;
        try {
            addSameEdgeTwice();
        }
        catch (error) {
            error0 = true;
        }
        assert.deepEqual(error0, true);
    });
    QUnit.test('should return the list of all added edges', (assert) => {
        const graph = new ds.Graph(true);
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC);
        const edges = graph.getAllEdges();
        assert.deepEqual(edges.length, 2);
        assert.deepEqual(edges[0], edgeAB);
        assert.deepEqual(edges[1], edgeBC);
    });
    QUnit.test('should calculate total graph weight for default graph', (assert) => {
        const graph = new ds.Graph();
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC);
        const edgeCD = new ds.GraphEdge(vertexC, vertexD);
        const edgeAD = new ds.GraphEdge(vertexA, vertexD);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeCD)
            .addEdge(edgeAD);
        assert.deepEqual(graph.getWeight(), 0);
    });
    QUnit.test('should calculate total graph weight for weighted graph', (assert) => {
        const graph = new ds.Graph();
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB, 1);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC, 2);
        const edgeCD = new ds.GraphEdge(vertexC, vertexD, 3);
        const edgeAD = new ds.GraphEdge(vertexA, vertexD, 4);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeCD)
            .addEdge(edgeAD);
        assert.deepEqual(graph.getWeight(), 10);
    });
    QUnit.test('should be possible to delete edges from graph', (assert) => {
        const graph = new ds.Graph();
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC);
        const edgeAC = new ds.GraphEdge(vertexA, vertexC);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeAC);
        assert.deepEqual(graph.getAllEdges().length, 3);
        graph.deleteEdge(edgeAB);
        assert.deepEqual(graph.getAllEdges().length, 2);
        assert.deepEqual(graph.getAllEdges()[0].getKey(), edgeBC.getKey());
        assert.deepEqual(graph.getAllEdges()[1].getKey(), edgeAC.getKey());
    });
    QUnit.test('should should throw an error when trying to delete not existing edge', (assert) => {
        function deleteNotExistingEdge() {
            const graph = new ds.Graph();
            const vertexA = new ds.GraphVertex('A');
            const vertexB = new ds.GraphVertex('B');
            const vertexC = new ds.GraphVertex('C');
            const edgeAB = new ds.GraphEdge(vertexA, vertexB);
            const edgeBC = new ds.GraphEdge(vertexB, vertexC);
            graph.addEdge(edgeAB);
            graph.deleteEdge(edgeBC);
        }
        var error0 = false;
        try {
            deleteNotExistingEdge();
        }
        catch (error) {
            error0 = true;
        }
        assert.deepEqual(error0, true);
    });
    QUnit.test('should be possible to reverse graph', (assert) => {
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeAC = new ds.GraphEdge(vertexA, vertexC);
        const edgeCD = new ds.GraphEdge(vertexC, vertexD);
        const graph = new ds.Graph(true);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeAC)
            .addEdge(edgeCD);
        assert.deepEqual(graph.toString(), 'A,B,C,D');
        assert.deepEqual(graph.getAllEdges().length, 3);
        assert.deepEqual(graph.getNeighbors(vertexA).length, 2);
        assert.deepEqual(graph.getNeighbors(vertexA)[0].getKey(), vertexB.getKey());
        assert.deepEqual(graph.getNeighbors(vertexA)[1].getKey(), vertexC.getKey());
        assert.deepEqual(graph.getNeighbors(vertexB).length, 0);
        assert.deepEqual(graph.getNeighbors(vertexC).length, 1);
        assert.deepEqual(graph.getNeighbors(vertexC)[0].getKey(), vertexD.getKey());
        assert.deepEqual(graph.getNeighbors(vertexD).length, 0);
        graph.reverse();
        assert.deepEqual(graph.toString(), 'A,B,C,D');
        assert.deepEqual(graph.getAllEdges().length, 3);
        assert.deepEqual(graph.getNeighbors(vertexA).length, 0);
        assert.deepEqual(graph.getNeighbors(vertexB).length, 1);
        assert.deepEqual(graph.getNeighbors(vertexB)[0].getKey(), vertexA.getKey());
        assert.deepEqual(graph.getNeighbors(vertexC).length, 1);
        assert.deepEqual(graph.getNeighbors(vertexC)[0].getKey(), vertexA.getKey());
        assert.deepEqual(graph.getNeighbors(vertexD).length, 1);
        assert.deepEqual(graph.getNeighbors(vertexD)[0].getKey(), vertexC.getKey());
    });
    QUnit.test('should return vertices indices', (assert) => {
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC);
        const edgeCD = new ds.GraphEdge(vertexC, vertexD);
        const edgeBD = new ds.GraphEdge(vertexB, vertexD);
        const graph = new ds.Graph();
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeCD)
            .addEdge(edgeBD);
        const verticesIndices = graph.getVerticesIndices();
        assert.deepEqual(verticesIndices, {
            A: 0,
            B: 1,
            C: 2,
            D: 3,
        });
    });
    QUnit.test('should generate adjacency matrix for undirected graph', (assert) => {
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC);
        const edgeCD = new ds.GraphEdge(vertexC, vertexD);
        const edgeBD = new ds.GraphEdge(vertexB, vertexD);
        const graph = new ds.Graph();
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeCD)
            .addEdge(edgeBD);
        const adjacencyMatrix = graph.getAdjacencyMatrix();
        assert.deepEqual(adjacencyMatrix, [
            [Infinity, 0, Infinity, Infinity],
            [0, Infinity, 0, 0],
            [Infinity, 0, Infinity, 0],
            [Infinity, 0, 0, Infinity],
        ]);
    });
    QUnit.test('should generate adjacency matrix for directed graph', (assert) => {
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB, 2);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC, 1);
        const edgeCD = new ds.GraphEdge(vertexC, vertexD, 5);
        const edgeBD = new ds.GraphEdge(vertexB, vertexD, 7);
        const graph = new ds.Graph(true);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeCD)
            .addEdge(edgeBD);
        const adjacencyMatrix = graph.getAdjacencyMatrix();
        assert.deepEqual(adjacencyMatrix, [
            [Infinity, 2, Infinity, Infinity],
            [Infinity, Infinity, 1, 7],
            [Infinity, Infinity, Infinity, 5],
            [Infinity, Infinity, Infinity, Infinity],
        ]);
    });
});
QUnit.module('HashTable', (assert) => {
    QUnit.test('should create hash table of certain size', (assert) => {
        const defaultHashTable = new ds.HashTable();
        assert.deepEqual(defaultHashTable.buckets.length, 32);
        const biggerHashTable = new ds.HashTable(64);
        assert.deepEqual(biggerHashTable.buckets.length, 64);
    });
    QUnit.test('should generate proper hash for specified keys', (assert) => {
        const hashTable = new ds.HashTable();
        assert.deepEqual(hashTable.hash('a'), 1);
        assert.deepEqual(hashTable.hash('b'), 2);
        assert.deepEqual(hashTable.hash('abc'), 6);
    });
    QUnit.test('should set, read and delete data with collisions', (assert) => {
        const hashTable = new ds.HashTable(3);
        assert.deepEqual(hashTable.hash('a'), 1);
        assert.deepEqual(hashTable.hash('b'), 2);
        assert.deepEqual(hashTable.hash('c'), 0);
        assert.deepEqual(hashTable.hash('d'), 1);
        hashTable.set('a', 'sky-old');
        hashTable.set('a', 'sky');
        hashTable.set('b', 'sea');
        hashTable.set('c', 'earth');
        hashTable.set('d', 'ocean');
        assert.deepEqual(hashTable.has('x'), false);
        assert.deepEqual(hashTable.has('b'), true);
        assert.deepEqual(hashTable.has('c'), true);
        const stringifier = value => `${value.key}:${value.value}`;
        assert.deepEqual(hashTable.buckets[0].toString(stringifier), 'c:earth');
        assert.deepEqual(hashTable.buckets[1].toString(stringifier), 'a:sky,d:ocean');
        assert.deepEqual(hashTable.buckets[2].toString(stringifier), 'b:sea');
        assert.deepEqual(hashTable.get('a'), 'sky');
        assert.deepEqual(hashTable.get('d'), 'ocean');
        assert.deepEqual(hashTable.get('x'), undefined);
        hashTable.delete('a');
        assert.deepEqual(hashTable.delete('not-existing'), null);
        assert.deepEqual(hashTable.get('a'), undefined);
        assert.deepEqual(hashTable.get('d'), 'ocean');
        hashTable.set('d', 'ocean-new');
        assert.deepEqual(hashTable.get('d'), 'ocean-new');
    });
    QUnit.test('should be possible to add objects to hash table', (assert) => {
        const hashTable = new ds.HashTable();
        hashTable.set('objectKey', { prop1: 'a', prop2: 'b' });
        const object = hashTable.get('objectKey');
        assert.deepEqual(object.prop1, 'a');
        assert.deepEqual(object.prop2, 'b');
    });
    QUnit.test('should track actual keys', (assert) => {
        const hashTable = new ds.HashTable(3);
        hashTable.set('a', 'sky-old');
        hashTable.set('a', 'sky');
        hashTable.set('b', 'sea');
        hashTable.set('c', 'earth');
        hashTable.set('d', 'ocean');
        assert.deepEqual(hashTable.getKeys(), ['a', 'b', 'c', 'd']);
        assert.deepEqual(hashTable.has('a'), true);
        assert.deepEqual(hashTable.has('x'), false);
        hashTable.delete('a');
        assert.deepEqual(hashTable.has('a'), false);
        assert.deepEqual(hashTable.has('b'), true);
        assert.deepEqual(hashTable.has('x'), false);
    });
});
QUnit.module('LinkedList', () => {
    QUnit.test('should create empty linked list', (assert) => {
        const linkedList = new ds.LinkedList();
        assert.deepEqual(linkedList.toString(), '');
    });
    QUnit.test('should append node to linked list', (assert) => {
        const linkedList = new ds.LinkedList();
        assert.deepEqual(linkedList.head, null);
        assert.deepEqual(linkedList.tail, null);
        linkedList.append(1);
        linkedList.append(2);
        assert.deepEqual(linkedList.toString(), '1,2');
        assert.deepEqual(linkedList.tail.next, null);
    });
    QUnit.test('should prepend node to linked list', (assert) => {
        const linkedList = new ds.LinkedList();
        linkedList.prepend(2);
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 2);
        linkedList.append(1);
        linkedList.prepend(3);
        assert.deepEqual(linkedList.toString(), '3,2,1');
    });
    QUnit.test('should delete node by value from linked list', (assert) => {
        const linkedList = new ds.LinkedList();
        assert.deepEqual(linkedList.deleteAll(5), null);
        linkedList.append(1);
        linkedList.append(1);
        linkedList.append(2);
        linkedList.append(3);
        linkedList.append(3);
        linkedList.append(3);
        linkedList.append(4);
        linkedList.append(5);
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 5);
        const deletedNode = linkedList.deleteAll(3);
        assert.deepEqual(deletedNode.value, 3);
        assert.deepEqual(linkedList.toString(), '1,1,2,4,5');
        linkedList.deleteAll(3);
        assert.deepEqual(linkedList.toString(), '1,1,2,4,5');
        linkedList.deleteAll(1);
        assert.deepEqual(linkedList.toString(), '2,4,5');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 5);
        linkedList.deleteAll(5);
        assert.deepEqual(linkedList.toString(), '2,4');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 4);
        linkedList.deleteAll(4);
        assert.deepEqual(linkedList.toString(), '2');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 2);
        linkedList.deleteAll(2);
        assert.deepEqual(linkedList.toString(), '');
    });
    QUnit.test('should delete linked list tail', (assert) => {
        const linkedList = new ds.LinkedList();
        linkedList.append(1);
        linkedList.append(2);
        linkedList.append(3);
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 3);
        const deletedNode1 = linkedList.deleteTail();
        assert.deepEqual(deletedNode1, 3);
        assert.deepEqual(linkedList.toString(), '1,2');
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 2);
        const deletedNode2 = linkedList.deleteTail();
        assert.deepEqual(deletedNode2, 2);
        assert.deepEqual(linkedList.toString(), '1');
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 1);
        const deletedNode3 = linkedList.deleteTail();
        assert.deepEqual(deletedNode3, 1);
        assert.deepEqual(linkedList.toString(), '');
        assert.deepEqual(linkedList.head, null);
        assert.deepEqual(linkedList.tail, null);
    });
    QUnit.test('should delete linked list head', (assert) => {
        const linkedList = new ds.LinkedList();
        assert.deepEqual(linkedList.deleteHead(), null);
        linkedList.append(1);
        linkedList.append(2);
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 2);
        const deletedNode1 = linkedList.deleteHead();
        assert.deepEqual(deletedNode1, 1);
        assert.deepEqual(linkedList.toString(), '2');
        assert.deepEqual(linkedList.head.value, 2);
        assert.deepEqual(linkedList.tail.value, 2);
        const deletedNode2 = linkedList.deleteHead();
        assert.deepEqual(deletedNode2, 2);
        assert.deepEqual(linkedList.toString(), '');
        assert.deepEqual(linkedList.head, null);
        assert.deepEqual(linkedList.tail, null);
    });
    QUnit.test('should be possible to store objects in the list and to print them out', (assert) => {
        const linkedList = new ds.LinkedList();
        const nodeValue1 = { value: 1, key: 'key1' };
        const nodeValue2 = { value: 2, key: 'key2' };
        linkedList
            .append(nodeValue1)
            .prepend(nodeValue2);
        const nodeStringifier = value => `${value.key}:${value.value}`;
        assert.deepEqual(linkedList.toString(nodeStringifier), 'key2:2,key1:1');
    });
    QUnit.test('should find node by value', (assert) => {
        const linkedList = new ds.LinkedList();
        assert.deepEqual(linkedList.find(5), null);
        linkedList.append(1);
        assert.deepEqual(linkedList.find(1) != null, true);
        linkedList
            .append(2)
            .append(3);
        const node = linkedList.find(2);
        assert.deepEqual(node.value, 2);
        assert.deepEqual(linkedList.find(5), null);
    });
    QUnit.test('should find node by callback', (assert) => {
        const linkedList = new ds.LinkedList();
        linkedList
            .append({ value: 1, key: 'test1' })
            .append({ value: 2, key: 'test2' })
            .append({ value: 3, key: 'test3' });
        const node = linkedList.findByFunc(value => value.key === 'test2');
        assert.deepEqual(node != null, true);
        assert.deepEqual(node.value.value, 2);
        assert.deepEqual(node.value.key, 'test2');
        assert.deepEqual(linkedList.findByFunc(value => value.key === 'test5'), null);
    });
    QUnit.test('should create linked list from array', (assert) => {
        const linkedList = new ds.LinkedList();
        linkedList.fromArray([1, 1, 2, 3, 3, 3, 4, 5]);
        assert.deepEqual(linkedList.toString(), '1,1,2,3,3,3,4,5');
    });
    QUnit.test('should find node by means of custom compare function', (assert) => {
        const comparatorFunction = (a, b) => {
            if (a.customValue === b.customValue) {
                return 0;
            }
            return a.customValue < b.customValue ? -1 : 1;
        };
        const linkedList = new ds.LinkedList(comparatorFunction);
        linkedList
            .append({ value: 1, customValue: 'test1' })
            .append({ value: 2, customValue: 'test2' })
            .append({ value: 3, customValue: 'test3' });
        const node = linkedList.find({ value: 2, customValue: 'test2' });
        assert.deepEqual(node != null, true);
        assert.deepEqual(node.value.value, 2);
        assert.deepEqual(node.value.customValue, 'test2');
        assert.deepEqual(linkedList.find({ value: 2, customValue: 'test5' }), null);
    });
    QUnit.test('should reverse linked list', (assert) => {
        const linkedList = new ds.LinkedList();
        // Add test values to linked list.
        linkedList
            .append(1)
            .append(2)
            .append(3);
        assert.deepEqual(linkedList.toString(), '1,2,3');
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 3);
        // Reverse linked list.
        linkedList.reverse();
        assert.deepEqual(linkedList.toString(), '3,2,1');
        assert.deepEqual(linkedList.head.value, 3);
        assert.deepEqual(linkedList.tail.value, 1);
        // Reverse linked list back to initial state.
        linkedList.reverse();
        assert.deepEqual(linkedList.toString(), '1,2,3');
        assert.deepEqual(linkedList.head.value, 1);
        assert.deepEqual(linkedList.tail.value, 3);
    });
});
QUnit.module("MaxHeap", () => {
    QUnit.test("MaxHeap", (assert) => {
        const maxHeap = new ds.MaxHeap();
        assert.deepEqual(maxHeap.peek(), null);
        assert.deepEqual(maxHeap.isEmpty(), true);
    });
    QUnit.test("should add items to the heap and heapify it up", (assert) => {
        const maxHeap = new ds.MaxHeap();
        maxHeap.add(5);
        assert.deepEqual(maxHeap.isEmpty(), false);
        assert.deepEqual(maxHeap.peek(), 5);
        assert.deepEqual(maxHeap.toString(), '5');
        maxHeap.add(3);
        assert.deepEqual(maxHeap.peek(), 5);
        assert.deepEqual(maxHeap.toString(), '5,3');
        maxHeap.add(10);
        assert.deepEqual(maxHeap.peek(), 10);
        assert.deepEqual(maxHeap.toString(), '10,3,5');
        maxHeap.add(1);
        assert.deepEqual(maxHeap.peek(), 10);
        assert.deepEqual(maxHeap.toString(), '10,3,5,1');
        maxHeap.add(1);
        assert.deepEqual(maxHeap.peek(), 10);
        assert.deepEqual(maxHeap.toString(), '10,3,5,1,1');
        assert.deepEqual(maxHeap.poll(), 10);
        assert.deepEqual(maxHeap.toString(), '5,3,1,1');
        assert.deepEqual(maxHeap.poll(), 5);
        assert.deepEqual(maxHeap.toString(), '3,1,1');
        assert.deepEqual(maxHeap.poll(), 3);
        assert.deepEqual(maxHeap.toString(), '1,1');
    });
    QUnit.test('should poll items from the heap and heapify it down', (assert) => {
        const maxHeap = new ds.MaxHeap();
        maxHeap.add(5);
        maxHeap.add(3);
        maxHeap.add(10);
        maxHeap.add(11);
        maxHeap.add(1);
        assert.deepEqual(maxHeap.toString(), '11,10,5,3,1');
        assert.deepEqual(maxHeap.poll(), 11);
        assert.deepEqual(maxHeap.toString(), '10,3,5,1');
        assert.deepEqual(maxHeap.poll(), 10);
        assert.deepEqual(maxHeap.toString(), '5,3,1');
        assert.deepEqual(maxHeap.poll(), 5);
        assert.deepEqual(maxHeap.toString(), '3,1');
        assert.deepEqual(maxHeap.poll(), 3);
        assert.deepEqual(maxHeap.toString(), '1');
        assert.deepEqual(maxHeap.poll(), 1);
        assert.deepEqual(maxHeap.toString(), '');
        assert.deepEqual(maxHeap.poll(), null);
        assert.deepEqual(maxHeap.toString(), '');
    });
    QUnit.test('should heapify down through the right branch as well', (assert) => {
        const maxHeap = new ds.MaxHeap();
        maxHeap.add(3);
        maxHeap.add(12);
        maxHeap.add(10);
        assert.deepEqual(maxHeap.toString(), '12,3,10');
        maxHeap.add(11);
        assert.deepEqual(maxHeap.toString(), '12,11,10,3');
        assert.deepEqual(maxHeap.poll(), 12);
        assert.deepEqual(maxHeap.toString(), '11,3,10');
    });
    QUnit.test('should be possible to find item indices in heap', (assert) => {
        const maxHeap = new ds.MaxHeap();
        maxHeap.add(3);
        maxHeap.add(12);
        maxHeap.add(10);
        maxHeap.add(11);
        maxHeap.add(11);
        assert.deepEqual(maxHeap.toString(), '12,11,10,3,11');
        assert.deepEqual(maxHeap.find(5), []);
        assert.deepEqual(maxHeap.find(12), [0]);
        assert.deepEqual(maxHeap.find(11), [1, 4]);
    });
    QUnit.test('should be possible to remove items from heap with heapify down', (assert) => {
        const maxHeap = new ds.MaxHeap();
        maxHeap.add(3);
        maxHeap.add(12);
        maxHeap.add(10);
        maxHeap.add(11);
        maxHeap.add(11);
        assert.deepEqual(maxHeap.toString(), '12,11,10,3,11');
        assert.deepEqual(maxHeap.remove(12).toString(), '11,11,10,3');
        assert.deepEqual(maxHeap.remove(12).peek(), 11);
        assert.deepEqual(maxHeap.remove(11).toString(), '10,3');
        assert.deepEqual(maxHeap.remove(10).peek(), 3);
    });
    QUnit.test('should be possible to remove items from heap with heapify up', (assert) => {
        const maxHeap = new ds.MaxHeap();
        maxHeap.add(3);
        maxHeap.add(10);
        maxHeap.add(5);
        maxHeap.add(6);
        maxHeap.add(7);
        maxHeap.add(4);
        maxHeap.add(6);
        maxHeap.add(8);
        maxHeap.add(2);
        maxHeap.add(1);
        assert.deepEqual(maxHeap.toString(), '10,8,6,7,6,4,5,3,2,1');
        assert.deepEqual(maxHeap.remove(4).toString(), '10,8,6,7,6,1,5,3,2');
        assert.deepEqual(maxHeap.remove(3).toString(), '10,8,6,7,6,1,5,2');
        assert.deepEqual(maxHeap.remove(5).toString(), '10,8,6,7,6,1,2');
        assert.deepEqual(maxHeap.remove(10).toString(), '8,7,6,2,6,1');
        assert.deepEqual(maxHeap.remove(6).toString(), '8,7,1,2');
        assert.deepEqual(maxHeap.remove(2).toString(), '8,7,1');
        assert.deepEqual(maxHeap.remove(1).toString(), '8,7');
        assert.deepEqual(maxHeap.remove(7).toString(), '8');
        assert.deepEqual(maxHeap.remove(8).toString(), '');
    });
    QUnit.test('should be possible to remove items from heap with custom finding comparator', (assert) => {
        const maxHeap = new ds.MaxHeap();
        maxHeap.add('a');
        maxHeap.add('bb');
        maxHeap.add('ccc');
        maxHeap.add('dddd');
        assert.deepEqual(maxHeap.toString(), 'dddd,ccc,bb,a');
        const comparator = new ds.Comparator((a, b) => {
            if (a.length === b.length) {
                return 0;
            }
            return a.length < b.length ? -1 : 1;
        });
        maxHeap.remove('hey', comparator);
        assert.deepEqual(maxHeap.toString(), 'dddd,a,bb');
    });
});
QUnit.module('MinHeap', (assert) => {
    QUnit.test('should create an empty min heap', (assert) => {
        const minHeap = new ds.MinHeap();
        assert.deepEqual(minHeap.peek(), null);
        assert.deepEqual(minHeap.isEmpty(), true);
    });
    QUnit.test('should add items to the heap and heapify it up', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add(5);
        assert.deepEqual(minHeap.isEmpty(), false);
        assert.deepEqual(minHeap.peek(), 5);
        assert.deepEqual(minHeap.toString(), '5');
        minHeap.add(3);
        assert.deepEqual(minHeap.peek(), 3);
        assert.deepEqual(minHeap.toString(), '3,5');
        minHeap.add(10);
        assert.deepEqual(minHeap.peek(), 3);
        assert.deepEqual(minHeap.toString(), '3,5,10');
        minHeap.add(1);
        assert.deepEqual(minHeap.peek(), 1);
        assert.deepEqual(minHeap.toString(), '1,3,10,5');
        minHeap.add(1);
        assert.deepEqual(minHeap.peek(), 1);
        assert.deepEqual(minHeap.toString(), '1,1,10,5,3');
        assert.deepEqual(minHeap.poll(), 1);
        assert.deepEqual(minHeap.toString(), '1,3,10,5');
        assert.deepEqual(minHeap.poll(), 1);
        assert.deepEqual(minHeap.toString(), '3,5,10');
        assert.deepEqual(minHeap.poll(), 3);
        assert.deepEqual(minHeap.toString(), '5,10');
    });
    QUnit.test('should poll items from the heap and heapify it down', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add(5);
        minHeap.add(3);
        minHeap.add(10);
        minHeap.add(11);
        minHeap.add(1);
        assert.deepEqual(minHeap.toString(), '1,3,10,11,5');
        assert.deepEqual(minHeap.poll(), 1);
        assert.deepEqual(minHeap.toString(), '3,5,10,11');
        assert.deepEqual(minHeap.poll(), 3);
        assert.deepEqual(minHeap.toString(), '5,11,10');
        assert.deepEqual(minHeap.poll(), 5);
        assert.deepEqual(minHeap.toString(), '10,11');
        assert.deepEqual(minHeap.poll(), 10);
        assert.deepEqual(minHeap.toString(), '11');
        assert.deepEqual(minHeap.poll(), 11);
        assert.deepEqual(minHeap.toString(), '');
        assert.deepEqual(minHeap.poll(), null);
        assert.deepEqual(minHeap.toString(), '');
    });
    QUnit.test('should heapify down through the right branch as well', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add(3);
        minHeap.add(12);
        minHeap.add(10);
        assert.deepEqual(minHeap.toString(), '3,12,10');
        minHeap.add(11);
        assert.deepEqual(minHeap.toString(), '3,11,10,12');
        assert.deepEqual(minHeap.poll(), 3);
        assert.deepEqual(minHeap.toString(), '10,11,12');
    });
    QUnit.test('should be possible to find item indices in heap', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add(3);
        minHeap.add(12);
        minHeap.add(10);
        minHeap.add(11);
        minHeap.add(11);
        assert.deepEqual(minHeap.toString(), '3,11,10,12,11');
        assert.deepEqual(minHeap.find(5), []);
        assert.deepEqual(minHeap.find(3), [0]);
        assert.deepEqual(minHeap.find(11), [1, 4]);
    });
    QUnit.test('should be possible to remove items from heap with heapify down', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add(3);
        minHeap.add(12);
        minHeap.add(10);
        minHeap.add(11);
        minHeap.add(11);
        assert.deepEqual(minHeap.toString(), '3,11,10,12,11');
        assert.deepEqual(minHeap.remove(3).toString(), '10,11,11,12');
        assert.deepEqual(minHeap.remove(3).peek(), 10);
        assert.deepEqual(minHeap.remove(11).toString(), '10,12');
        assert.deepEqual(minHeap.remove(3).peek(), 10);
    });
    QUnit.test('should be possible to remove items from heap with heapify up', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add(3);
        minHeap.add(10);
        minHeap.add(5);
        minHeap.add(6);
        minHeap.add(7);
        minHeap.add(4);
        minHeap.add(6);
        minHeap.add(8);
        minHeap.add(2);
        minHeap.add(1);
        assert.deepEqual(minHeap.toString(), '1,2,4,6,3,5,6,10,8,7');
        assert.deepEqual(minHeap.remove(8).toString(), '1,2,4,6,3,5,6,10,7');
        assert.deepEqual(minHeap.remove(7).toString(), '1,2,4,6,3,5,6,10');
        assert.deepEqual(minHeap.remove(1).toString(), '2,3,4,6,10,5,6');
        assert.deepEqual(minHeap.remove(2).toString(), '3,6,4,6,10,5');
        assert.deepEqual(minHeap.remove(6).toString(), '3,5,4,10');
        assert.deepEqual(minHeap.remove(10).toString(), '3,5,4');
        assert.deepEqual(minHeap.remove(5).toString(), '3,4');
        assert.deepEqual(minHeap.remove(3).toString(), '4');
        assert.deepEqual(minHeap.remove(4).toString(), '');
    });
    QUnit.test('should be possible to remove items from heap with custom finding comparator', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add('dddd');
        minHeap.add('ccc');
        minHeap.add('bb');
        minHeap.add('a');
        assert.deepEqual(minHeap.toString(), 'a,bb,ccc,dddd');
        const comparator = new ds.Comparator((a, b) => {
            if (a.length === b.length) {
                return 0;
            }
            return a.length < b.length ? -1 : 1;
        });
        minHeap.remove('hey', comparator);
        assert.deepEqual(minHeap.toString(), 'a,bb,dddd');
    });
    QUnit.test('should remove values from heap and correctly re-order the tree', (assert) => {
        const minHeap = new ds.MinHeap();
        minHeap.add(1);
        minHeap.add(2);
        minHeap.add(3);
        minHeap.add(4);
        minHeap.add(5);
        minHeap.add(6);
        minHeap.add(7);
        minHeap.add(8);
        minHeap.add(9);
        assert.deepEqual(minHeap.toString(), '1,2,3,4,5,6,7,8,9');
        minHeap.remove(2);
        assert.deepEqual(minHeap.toString(), '1,4,3,8,5,6,7,9');
        minHeap.remove(4);
        assert.deepEqual(minHeap.toString(), '1,5,3,8,9,6,7');
    });
});
QUnit.module('PolynomialHash', () => {
    QUnit.test('should calculate new hash based on previous one', (assert) => {
        const bases = [3, 79, 101, 3251, 13229, 122743, 3583213];
        const mods = [79, 101];
        const frameSizes = [5, 20];
        // @TODO: Provide Unicode support.
        const text = 'Lorem Ipsum is simply dummy text of the printing and ';
        // + 'typesetting industry. Lorem Ipsum has been the industry\'s standard '
        // + 'galley of type and \u{ffff} scrambled it to make a type specimen book. It '
        // + 'electronic 耀 typesetting, remaining essentially unchanged. It was '
        // // + 'popularised in the \u{20005} \u{20000}1960s with the release of Letraset sheets '
        // + 'publishing software like Aldus PageMaker 耀 including versions of Lorem.';
        // Check hashing for different prime base.
        bases.forEach((base) => {
            mods.forEach((modulus) => {
                const polynomialHash = new ds.PolynomialHash({ base, modulus });
                // Check hashing for different word lengths.
                frameSizes.forEach((frameSize) => {
                    let previousWord = text.substr(0, frameSize);
                    let previousHash = polynomialHash.hash(previousWord);
                    // Shift frame through the whole text.
                    for (let frameShift = 1; frameShift < (text.length - frameSize); frameShift += 1) {
                        const currentWord = text.substr(frameShift, frameSize);
                        const currentHash = polynomialHash.hash(currentWord);
                        const currentRollingHash = polynomialHash.roll(previousHash, previousWord, currentWord);
                        // Check that rolling hash is the same as directly calculated hash.
                        assert.deepEqual(currentRollingHash, currentHash);
                        previousWord = currentWord;
                        previousHash = currentHash;
                    }
                });
            });
        });
    });
    QUnit.test('should generate numeric hashed less than 100', (assert) => {
        const polynomialHash = new ds.PolynomialHash({ modulus: 100 });
        assert.deepEqual(polynomialHash.hash('Some long text that is used as a key'), 41);
        assert.deepEqual(polynomialHash.hash('Test'), 92);
        assert.deepEqual(polynomialHash.hash('a'), 97);
        assert.deepEqual(polynomialHash.hash('b'), 98);
        assert.deepEqual(polynomialHash.hash('c'), 99);
        assert.deepEqual(polynomialHash.hash('d'), 0);
        assert.deepEqual(polynomialHash.hash('e'), 1);
        assert.deepEqual(polynomialHash.hash('ab'), 87);
        // @TODO: Provide Unicode support.
        assert.deepEqual(polynomialHash.hash('\u{20000}'), 92);
    });
});
QUnit.module('PriorityQueue', () => {
    QUnit.test('should create default priority queue', (assert) => {
        const priorityQueue = new ds.PriorityQueue();
        assert.deepEqual(priorityQueue != null, true);
    });
    QUnit.test('should insert items to the queue and respect priorities', (assert) => {
        const priorityQueue = new ds.PriorityQueue();
        priorityQueue.add(10, 1);
        assert.deepEqual(priorityQueue.peek(), 10);
        priorityQueue.add(5, 2);
        assert.deepEqual(priorityQueue.peek(), 10);
        priorityQueue.add(100, 0);
        assert.deepEqual(priorityQueue.peek(), 100);
    });
    QUnit.test('should poll from queue with respect to priorities', (assert) => {
        const priorityQueue = new ds.PriorityQueue();
        priorityQueue.add(10, 1);
        priorityQueue.add(5, 2);
        priorityQueue.add(100, 0);
        priorityQueue.add(200, 0);
        assert.deepEqual(priorityQueue.poll(), 100);
        assert.deepEqual(priorityQueue.poll(), 200);
        assert.deepEqual(priorityQueue.poll(), 10);
        assert.deepEqual(priorityQueue.poll(), 5);
    });
    QUnit.test('should be possible to change priority of internal nodes', (assert) => {
        const priorityQueue = new ds.PriorityQueue();
        priorityQueue.add(10, 1);
        priorityQueue.add(5, 2);
        priorityQueue.add(100, 0);
        priorityQueue.add(200, 0);
        priorityQueue.changePriority(100, 10);
        priorityQueue.changePriority(10, 20);
        assert.deepEqual(priorityQueue.poll(), 200);
        assert.deepEqual(priorityQueue.poll(), 5);
        assert.deepEqual(priorityQueue.poll(), 100);
        assert.deepEqual(priorityQueue.poll(), 10);
    });
    QUnit.test('should be possible to change priority of head node', (assert) => {
        const priorityQueue = new ds.PriorityQueue();
        priorityQueue.add(10, 1);
        priorityQueue.add(5, 2);
        priorityQueue.add(100, 0);
        priorityQueue.add(200, 0);
        priorityQueue.changePriority(200, 10);
        priorityQueue.changePriority(10, 20);
        assert.deepEqual(priorityQueue.poll(), 100);
        assert.deepEqual(priorityQueue.poll(), 5);
        assert.deepEqual(priorityQueue.poll(), 200);
        assert.deepEqual(priorityQueue.poll(), 10);
    });
    QUnit.test('should be possible to change priority along with node addition', (assert) => {
        const priorityQueue = new ds.PriorityQueue();
        priorityQueue.add(10, 1);
        priorityQueue.add(5, 2);
        priorityQueue.add(100, 0);
        priorityQueue.add(200, 0);
        priorityQueue.changePriority(200, 10);
        priorityQueue.changePriority(10, 20);
        priorityQueue.add(15, 15);
        assert.deepEqual(priorityQueue.poll(), 100);
        assert.deepEqual(priorityQueue.poll(), 5);
        assert.deepEqual(priorityQueue.poll(), 200);
        assert.deepEqual(priorityQueue.poll(), 15);
        assert.deepEqual(priorityQueue.poll(), 10);
    });
    QUnit.test('should be possible to search in priority queue by value', (assert) => {
        const priorityQueue = new ds.PriorityQueue();
        priorityQueue.add(10, 1);
        priorityQueue.add(5, 2);
        priorityQueue.add(100, 0);
        priorityQueue.add(200, 0);
        priorityQueue.add(15, 15);
        assert.deepEqual(priorityQueue.hasValue(70), false);
        assert.deepEqual(priorityQueue.hasValue(15), true);
    });
});
QUnit.module('Queue', () => {
    QUnit.test('should create empty queue', (assert) => {
        const queue = new ds.Queue();
        assert.deepEqual(queue["linkedList"] != null, true);
    });
    QUnit.test('should enqueue data to queue', (assert) => {
        const queue = new ds.Queue();
        queue.enqueue(1);
        queue.enqueue(2);
        assert.deepEqual(queue.toString(), '1,2');
    });
    QUnit.test('should be possible to enqueue/dequeue objects', (assert) => {
        const queue = new ds.Queue();
        queue.enqueue({ value: 'test1', key: 'key1' });
        queue.enqueue({ value: 'test2', key: 'key2' });
        const stringifier = value => `${value.key}:${value.value}`;
        assert.deepEqual(queue.toString(stringifier), 'key1:test1,key2:test2');
        assert.deepEqual(queue.dequeue().value, 'test1');
        assert.deepEqual(queue.dequeue().value, 'test2');
    });
    QUnit.test('should peek data from queue', (assert) => {
        const queue = new ds.Queue();
        assert.deepEqual(queue.peek(), null);
        queue.enqueue(1);
        queue.enqueue(2);
        assert.deepEqual(queue.peek(), 1);
        assert.deepEqual(queue.peek(), 1);
    });
    QUnit.test('should check if queue is empty', (assert) => {
        const queue = new ds.Queue();
        assert.deepEqual(queue.isEmpty(), true);
        queue.enqueue(1);
        assert.deepEqual(queue.isEmpty(), false);
    });
    QUnit.test('should dequeue from queue in FIFO order', (assert) => {
        const queue = new ds.Queue();
        queue.enqueue(1);
        queue.enqueue(2);
        assert.deepEqual(queue.dequeue(), 1);
        assert.deepEqual(queue.dequeue(), 2);
        assert.deepEqual(queue.dequeue(), null);
        assert.deepEqual(queue.isEmpty(), true);
    });
});
QUnit.module('RedBlackTree', () => {
    QUnit.test('should always color first inserted node as black', (assert) => {
        const tree = new ds.RedBlackTree();
        const firstInsertedNode = tree.insert(10);
        assert.deepEqual(tree.isNodeColored(firstInsertedNode), true);
        assert.deepEqual(tree.isNodeBlack(firstInsertedNode), true);
        assert.deepEqual(tree.isNodeRed(firstInsertedNode), false);
        assert.deepEqual(tree.toString(), '10');
        assert.deepEqual(tree.root.height, 0);
    });
    QUnit.test('should always color new leaf node as red', (assert) => {
        const tree = new ds.RedBlackTree();
        const firstInsertedNode = tree.insert(10);
        const secondInsertedNode = tree.insert(15);
        const thirdInsertedNode = tree.insert(5);
        assert.deepEqual(tree.isNodeBlack(firstInsertedNode), true);
        assert.deepEqual(tree.isNodeRed(secondInsertedNode), true);
        assert.deepEqual(tree.isNodeRed(thirdInsertedNode), true);
        assert.deepEqual(tree.toString(), '5,10,15');
        assert.deepEqual(tree.root.height, 1);
    });
    QUnit.test('should balance itself', (assert) => {
        const tree = new ds.RedBlackTree();
        tree.insert(5);
        tree.insert(10);
        tree.insert(15);
        tree.insert(20);
        tree.insert(25);
        tree.insert(30);
        assert.deepEqual(tree.toString(), '5,10,15,20,25,30');
        assert.deepEqual(tree.root.height, 3);
    });
    QUnit.test('should balance itself when parent is black', (assert) => {
        const tree = new ds.RedBlackTree();
        const node1 = tree.insert(10);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        const node2 = tree.insert(-10);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeRed(node2), true);
        const node3 = tree.insert(20);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeRed(node2), true);
        assert.deepEqual(tree.isNodeRed(node3), true);
        const node4 = tree.insert(-20);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        const node5 = tree.insert(25);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        const node6 = tree.insert(6);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        assert.deepEqual(tree.isNodeRed(node6), true);
        assert.deepEqual(tree.toString(), '-20,-10,6,10,20,25');
        assert.deepEqual(tree.root.height, 2);
        const node7 = tree.insert(4);
        assert.deepEqual(tree.root.left.value, node2.value);
        assert.deepEqual(tree.toString(), '-20,-10,4,6,10,20,25');
        assert.deepEqual(tree.root.height, 3);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeRed(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeBlack(node4), true);
        assert.deepEqual(tree.isNodeBlack(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        assert.deepEqual(tree.isNodeBlack(node6), true);
        assert.deepEqual(tree.isNodeRed(node7), true);
    });
    QUnit.test('should balance itself when uncle is red', (assert) => {
        const tree = new ds.RedBlackTree();
        const node1 = tree.insert(10);
        const node2 = tree.insert(-10);
        const node3 = tree.insert(20);
        const node4 = tree.insert(-20);
        const node5 = tree.insert(6);
        const node6 = tree.insert(15);
        const node7 = tree.insert(25);
        const node8 = tree.insert(2);
        const node9 = tree.insert(8);
        assert.deepEqual(tree.toString(), '-20,-10,2,6,8,10,15,20,25');
        assert.deepEqual(tree.root.height, 3);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeRed(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeBlack(node4), true);
        assert.deepEqual(tree.isNodeBlack(node5), true);
        assert.deepEqual(tree.isNodeRed(node6), true);
        assert.deepEqual(tree.isNodeRed(node7), true);
        assert.deepEqual(tree.isNodeRed(node8), true);
        assert.deepEqual(tree.isNodeRed(node9), true);
        const node10 = tree.insert(4);
        assert.deepEqual(tree.toString(), '-20,-10,2,4,6,8,10,15,20,25');
        assert.deepEqual(tree.root.height, 3);
        assert.deepEqual(tree.root.value, node5.value);
        assert.deepEqual(tree.isNodeBlack(node5), true);
        assert.deepEqual(tree.isNodeRed(node1), true);
        assert.deepEqual(tree.isNodeRed(node2), true);
        assert.deepEqual(tree.isNodeRed(node10), true);
        assert.deepEqual(tree.isNodeRed(node6), true);
        assert.deepEqual(tree.isNodeRed(node7), true);
        assert.deepEqual(tree.isNodeBlack(node4), true);
        assert.deepEqual(tree.isNodeBlack(node8), true);
        assert.deepEqual(tree.isNodeBlack(node9), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
    });
    QUnit.test('should do left-left rotation', (assert) => {
        const tree = new ds.RedBlackTree();
        const node1 = tree.insert(10);
        const node2 = tree.insert(-10);
        const node3 = tree.insert(20);
        const node4 = tree.insert(7);
        const node5 = tree.insert(15);
        assert.deepEqual(tree.toString(), '-10,7,10,15,20');
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        const node6 = tree.insert(13);
        assert.deepEqual(tree.toString(), '-10,7,10,13,15,20');
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node5), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node6), true);
        assert.deepEqual(tree.isNodeRed(node3), true);
    });
    QUnit.test('should do left-right rotation', (assert) => {
        const tree = new ds.RedBlackTree();
        const node1 = tree.insert(10);
        const node2 = tree.insert(-10);
        const node3 = tree.insert(20);
        const node4 = tree.insert(7);
        const node5 = tree.insert(15);
        assert.deepEqual(tree.toString(), '-10,7,10,15,20');
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        const node6 = tree.insert(17);
        assert.deepEqual(tree.toString(), '-10,7,10,15,17,20');
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node6), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        assert.deepEqual(tree.isNodeRed(node3), true);
    });
    QUnit.test('should do recoloring, left-left and left-right rotation', (assert) => {
        const tree = new ds.RedBlackTree();
        const node1 = tree.insert(10);
        const node2 = tree.insert(-10);
        const node3 = tree.insert(20);
        const node4 = tree.insert(-20);
        const node5 = tree.insert(6);
        const node6 = tree.insert(15);
        const node7 = tree.insert(30);
        const node8 = tree.insert(1);
        const node9 = tree.insert(9);
        assert.deepEqual(tree.toString(), '-20,-10,1,6,9,10,15,20,30');
        assert.deepEqual(tree.root.height, 3);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeRed(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeBlack(node4), true);
        assert.deepEqual(tree.isNodeBlack(node5), true);
        assert.deepEqual(tree.isNodeRed(node6), true);
        assert.deepEqual(tree.isNodeRed(node7), true);
        assert.deepEqual(tree.isNodeRed(node8), true);
        assert.deepEqual(tree.isNodeRed(node9), true);
        tree.insert(4);
        assert.deepEqual(tree.toString(), '-20,-10,1,4,6,9,10,15,20,30');
        assert.deepEqual(tree.root.height, 3);
    });
    QUnit.test('should do right-left rotation', (assert) => {
        const tree = new ds.RedBlackTree();
        const node1 = tree.insert(10);
        const node2 = tree.insert(-10);
        const node3 = tree.insert(20);
        const node4 = tree.insert(-20);
        const node5 = tree.insert(6);
        const node6 = tree.insert(30);
        assert.deepEqual(tree.toString(), '-20,-10,6,10,20,30');
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node3), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        assert.deepEqual(tree.isNodeRed(node6), true);
        const node7 = tree.insert(25);
        const rightNode = tree.root.right;
        const rightLeftNode = rightNode.left;
        const rightRightNode = rightNode.right;
        assert.deepEqual(rightNode.value, node7.value);
        assert.deepEqual(rightLeftNode.value, node3.value);
        assert.deepEqual(rightRightNode.value, node6.value);
        assert.deepEqual(tree.toString(), '-20,-10,6,10,20,25,30');
        assert.deepEqual(tree.root.height, 2);
        assert.deepEqual(tree.isNodeBlack(node1), true);
        assert.deepEqual(tree.isNodeBlack(node2), true);
        assert.deepEqual(tree.isNodeBlack(node7), true);
        assert.deepEqual(tree.isNodeRed(node4), true);
        assert.deepEqual(tree.isNodeRed(node5), true);
        assert.deepEqual(tree.isNodeRed(node3), true);
        assert.deepEqual(tree.isNodeRed(node6), true);
    });
    QUnit.test('should do left-left rotation with left grand-parent', (assert) => {
        const tree = new ds.RedBlackTree();
        tree.insert(20);
        tree.insert(15);
        tree.insert(25);
        tree.insert(10);
        tree.insert(5);
        assert.deepEqual(tree.toString(), '5,10,15,20,25');
        assert.deepEqual(tree.root.height, 2);
    });
    QUnit.test('should do right-right rotation with left grand-parent', (assert) => {
        const tree = new ds.RedBlackTree();
        tree.insert(20);
        tree.insert(15);
        tree.insert(25);
        tree.insert(17);
        tree.insert(19);
        assert.deepEqual(tree.toString(), '15,17,19,20,25');
        assert.deepEqual(tree.root.height, 2);
    });
    QUnit.test('should throw an error when trying to remove node', (assert) => {
        const removeNodeFromRedBlackTree = () => {
            const tree = new ds.RedBlackTree();
            tree.remove(1);
        };
        var error0 = false;
        try {
            removeNodeFromRedBlackTree();
        }
        catch (error) {
            error0 = true;
        }
        assert.deepEqual(error0, true);
    });
});
QUnit.module('SegmentTree', () => {
    QUnit.test('should build tree for input array #0 with length of power of two', (assert) => {
        const array = [-1, 2];
        const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);
        assert.deepEqual(segmentTree.segmentTree, [-1, -1, 2]);
        assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
    });
    QUnit.test('should build tree for input array #1 with length of power of two', (assert) => {
        const array = [-1, 2, 4, 0];
        const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);
        assert.deepEqual(segmentTree.segmentTree, [-1, -1, 0, -1, 2, 4, 0]);
        assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
    });
    QUnit.test('should build tree for input array #0 with length not of power of two', (assert) => {
        const array = [0, 1, 2];
        const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);
        assert.deepEqual(segmentTree.segmentTree, [0, 0, 2, 0, 1, null, null]);
        assert.deepEqual(segmentTree.segmentTree.length, (2 * 4) - 1);
    });
    QUnit.test('should build tree for input array #1 with length not of power of two', (assert) => {
        const array = [-1, 3, 4, 0, 2, 1];
        const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);
        assert.deepEqual(segmentTree.segmentTree, [
            -1, -1, 0, -1, 4, 0, 1, -1, 3, null, null, 0, 2, null, null,
        ]);
        assert.deepEqual(segmentTree.segmentTree.length, (2 * 8) - 1);
    });
    QUnit.test('should build max array', (assert) => {
        const array = [-1, 2, 4, 0];
        const segmentTree = new ds.SegmentTree(array, Math.max, -Infinity);
        assert.deepEqual(segmentTree.segmentTree, [4, 2, 4, -1, 2, 4, 0]);
        assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
    });
    QUnit.test('should build sum array', (assert) => {
        const array = [-1, 2, 4, 0];
        const segmentTree = new ds.SegmentTree(array, (a, b) => (a + b), 0);
        assert.deepEqual(segmentTree.segmentTree, [5, 1, 4, -1, 2, 4, 0]);
        assert.deepEqual(segmentTree.segmentTree.length, (2 * array.length) - 1);
    });
    QUnit.test('should do min range query on power of two length array', (assert) => {
        const array = [-1, 3, 4, 0, 2, 1];
        const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);
        assert.deepEqual(segmentTree.rangeQuery(0, 5), -1);
        assert.deepEqual(segmentTree.rangeQuery(0, 2), -1);
        assert.deepEqual(segmentTree.rangeQuery(1, 3), 0);
        assert.deepEqual(segmentTree.rangeQuery(2, 4), 0);
        assert.deepEqual(segmentTree.rangeQuery(4, 5), 1);
        assert.deepEqual(segmentTree.rangeQuery(2, 2), 4);
    });
    QUnit.test('should do min range query on not power of two length array', (assert) => {
        const array = [-1, 2, 4, 0];
        const segmentTree = new ds.SegmentTree(array, Math.min, Infinity);
        assert.deepEqual(segmentTree.rangeQuery(0, 4), -1);
        assert.deepEqual(segmentTree.rangeQuery(0, 1), -1);
        assert.deepEqual(segmentTree.rangeQuery(1, 3), 0);
        assert.deepEqual(segmentTree.rangeQuery(1, 2), 2);
        assert.deepEqual(segmentTree.rangeQuery(2, 3), 0);
        assert.deepEqual(segmentTree.rangeQuery(2, 2), 4);
    });
    QUnit.test('should do max range query', (assert) => {
        const array = [-1, 3, 4, 0, 2, 1];
        const segmentTree = new ds.SegmentTree(array, Math.max, -Infinity);
        assert.deepEqual(segmentTree.rangeQuery(0, 5), 4);
        assert.deepEqual(segmentTree.rangeQuery(0, 1), 3);
        assert.deepEqual(segmentTree.rangeQuery(1, 3), 4);
        assert.deepEqual(segmentTree.rangeQuery(2, 4), 4);
        assert.deepEqual(segmentTree.rangeQuery(4, 5), 2);
        assert.deepEqual(segmentTree.rangeQuery(3, 3), 0);
    });
    QUnit.test('should do sum range query', (assert) => {
        const array = [-1, 3, 4, 0, 2, 1];
        const segmentTree = new ds.SegmentTree(array, (a, b) => (a + b), 0);
        assert.deepEqual(segmentTree.rangeQuery(0, 5), 9);
        assert.deepEqual(segmentTree.rangeQuery(0, 1), 2);
        assert.deepEqual(segmentTree.rangeQuery(1, 3), 7);
        assert.deepEqual(segmentTree.rangeQuery(2, 4), 6);
        assert.deepEqual(segmentTree.rangeQuery(4, 5), 3);
        assert.deepEqual(segmentTree.rangeQuery(3, 3), 0);
    });
});
QUnit.module('SimplePolynomialHash', () => {
    QUnit.test('should calculate new hash based on previous one', (assert) => {
        const bases = [3, 5];
        const frameSizes = [5, 10];
        const text = 'Lorem Ipsum is simply dummy text of the printing and '
            + 'typesetting industry. Lorem Ipsum has been the industry\'s standard '
            + 'galley of type and \u{ffff} scrambled it to make a type specimen book. It '
            + 'electronic 耀 typesetting, remaining essentially unchanged. It was '
            + 'popularised in the 1960s with the release of Letraset sheets '
            + 'publishing software like Aldus 耀 PageMaker including versions of Lorem.';
        // Check hashing for different prime base.
        bases.forEach((base) => {
            const polynomialHash = new ds.SimplePolynomialHash(base);
            // Check hashing for different word lengths.
            frameSizes.forEach((frameSize) => {
                let previousWord = text.substr(0, frameSize);
                let previousHash = polynomialHash.hash(previousWord);
                // Shift frame through the whole text.
                for (let frameShift = 1; frameShift < (text.length - frameSize); frameShift += 1) {
                    const currentWord = text.substr(frameShift, frameSize);
                    const currentHash = polynomialHash.hash(currentWord);
                    const currentRollingHash = polynomialHash.roll(previousHash, previousWord, currentWord);
                    // Check that rolling hash is the same as directly calculated hash.
                    assert.deepEqual(currentRollingHash, currentHash);
                    previousWord = currentWord;
                    previousHash = currentHash;
                }
            });
        });
    });
    QUnit.test('should generate numeric hashed', (assert) => {
        const polynomialHash = new ds.SimplePolynomialHash();
        assert.deepEqual(polynomialHash.hash('Test'), 604944);
        assert.deepEqual(polynomialHash.hash('a'), 97);
        assert.deepEqual(polynomialHash.hash('b'), 98);
        assert.deepEqual(polynomialHash.hash('c'), 99);
        assert.deepEqual(polynomialHash.hash('d'), 100);
        assert.deepEqual(polynomialHash.hash('e'), 101);
        assert.deepEqual(polynomialHash.hash('ab'), 1763);
        assert.deepEqual(polynomialHash.hash('abc'), 30374);
    });
});
QUnit.module('Trie', () => {
    QUnit.test('should create trie', (assert) => {
        const trie = new ds.Trie();
        assert.deepEqual(trie != null, true);
        assert.deepEqual(trie.head.toString(), '*');
    });
    QUnit.test('should add words to trie', (assert) => {
        const trie = new ds.Trie();
        trie.addWord('cat');
        assert.deepEqual(trie.head.toString(), '*:c');
        assert.deepEqual(trie.head.getChild('c').toString(), 'c:a');
        trie.addWord('car');
        assert.deepEqual(trie.head.toString(), '*:c');
        assert.deepEqual(trie.head.getChild('c').toString(), 'c:a');
        assert.deepEqual(trie.head.getChild('c').getChild('a').toString(), 'a:t,r');
        assert.deepEqual(trie.head.getChild('c').getChild('a').getChild('t').toString(), 't*');
    });
    QUnit.test('should delete words from trie', (assert) => {
        const trie = new ds.Trie();
        trie.addWord('carpet');
        trie.addWord('car');
        trie.addWord('cat');
        trie.addWord('cart');
        assert.deepEqual(trie.doesWordExist('carpet'), true);
        assert.deepEqual(trie.doesWordExist('car'), true);
        assert.deepEqual(trie.doesWordExist('cart'), true);
        assert.deepEqual(trie.doesWordExist('cat'), true);
        // Try to delete not-existing word first.
        trie.deleteWord('carpool');
        assert.deepEqual(trie.doesWordExist('carpet'), true);
        assert.deepEqual(trie.doesWordExist('car'), true);
        assert.deepEqual(trie.doesWordExist('cart'), true);
        assert.deepEqual(trie.doesWordExist('cat'), true);
        trie.deleteWord('carpet');
        assert.deepEqual(trie.doesWordExist('carpet'), false);
        assert.deepEqual(trie.doesWordExist('car'), true);
        assert.deepEqual(trie.doesWordExist('cart'), true);
        assert.deepEqual(trie.doesWordExist('cat'), true);
        trie.deleteWord('cat');
        assert.deepEqual(trie.doesWordExist('car'), true);
        assert.deepEqual(trie.doesWordExist('cart'), true);
        assert.deepEqual(trie.doesWordExist('cat'), false);
        trie.deleteWord('car');
        assert.deepEqual(trie.doesWordExist('car'), false);
        assert.deepEqual(trie.doesWordExist('cart'), true);
        trie.deleteWord('cart');
        assert.deepEqual(trie.doesWordExist('car'), false);
        assert.deepEqual(trie.doesWordExist('cart'), false);
    });
    QUnit.test('should suggests next characters', (assert) => {
        const trie = new ds.Trie();
        trie.addWord('cat');
        trie.addWord('cats');
        trie.addWord('car');
        trie.addWord('caption');
        assert.deepEqual(trie.suggestNextCharacters('ca'), ['t', 'r', 'p']);
        assert.deepEqual(trie.suggestNextCharacters('cat'), ['s']);
        assert.deepEqual(trie.suggestNextCharacters('cab'), null);
    });
    QUnit.test('should check if word exists', (assert) => {
        const trie = new ds.Trie();
        trie.addWord('cat');
        trie.addWord('cats');
        trie.addWord('carpet');
        trie.addWord('car');
        trie.addWord('caption');
        assert.deepEqual(trie.doesWordExist('cat'), true);
        assert.deepEqual(trie.doesWordExist('cats'), true);
        assert.deepEqual(trie.doesWordExist('carpet'), true);
        assert.deepEqual(trie.doesWordExist('car'), true);
        assert.deepEqual(trie.doesWordExist('cap'), false);
        assert.deepEqual(trie.doesWordExist('call'), false);
    });
});
QUnit.module('TrieNode', () => {
    QUnit.test('should create trie node', (assert) => {
        const trieNode = new ds.TrieNode('c', true);
        assert.deepEqual(trieNode.character, 'c');
        assert.deepEqual(trieNode.isCompleteWord, true);
        assert.deepEqual(trieNode.toString(), 'c*');
    });
    QUnit.test('should add child nodes', (assert) => {
        const trieNode = new ds.TrieNode('c');
        trieNode.addChild('a', true);
        trieNode.addChild('o');
        assert.deepEqual(trieNode.toString(), 'c:a,o');
    });
    QUnit.test('should get child nodes', (assert) => {
        const trieNode = new ds.TrieNode('c');
        trieNode.addChild('a');
        trieNode.addChild('o');
        assert.deepEqual(trieNode.getChild('a').toString(), 'a');
        assert.deepEqual(trieNode.getChild('a').character, 'a');
        assert.deepEqual(trieNode.getChild('o').toString(), 'o');
        assert.deepEqual(trieNode.getChild('b'), undefined);
    });
    QUnit.test('should check if node has children', (assert) => {
        const trieNode = new ds.TrieNode('c');
        assert.deepEqual(trieNode.hasChildren(), false);
        trieNode.addChild('a');
        assert.deepEqual(trieNode.hasChildren(), true);
    });
    QUnit.test('should check if node has specific child', (assert) => {
        const trieNode = new ds.TrieNode('c');
        trieNode.addChild('a');
        trieNode.addChild('o');
        assert.deepEqual(trieNode.hasChild('a'), true);
        assert.deepEqual(trieNode.hasChild('o'), true);
        assert.deepEqual(trieNode.hasChild('b'), false);
    });
    QUnit.test('should suggest next children', (assert) => {
        const trieNode = new ds.TrieNode('c');
        trieNode.addChild('a');
        trieNode.addChild('o');
        assert.deepEqual(trieNode.suggestChildren(), ['a', 'o']);
    });
    QUnit.test('should delete child node if the child node has NO children', (assert) => {
        const trieNode = new ds.TrieNode('c');
        trieNode.addChild('a');
        assert.deepEqual(trieNode.hasChild('a'), true);
        trieNode.removeChild('a');
        assert.deepEqual(trieNode.hasChild('a'), false);
    });
    QUnit.test('should NOT delete child node if the child node has children', (assert) => {
        const trieNode = new ds.TrieNode('c');
        trieNode.addChild('a');
        const childNode = trieNode.getChild('a');
        childNode.addChild('r');
        trieNode.removeChild('a');
        assert.deepEqual(trieNode.hasChild('a'), true);
    });
    QUnit.test('should NOT delete child node if the child node completes a word', (assert) => {
        const trieNode = new ds.TrieNode('c');
        const IS_COMPLETE_WORD = true;
        trieNode.addChild('a', IS_COMPLETE_WORD);
        trieNode.removeChild('a');
        assert.deepEqual(trieNode.hasChild('a'), true);
    });
});
QUnit.module('depthFirstSearch', () => {
    QUnit.test('should perform DFS operation on graph', (assert) => {
        const graph = new ds.Graph(true);
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const vertexE = new ds.GraphVertex('E');
        const vertexF = new ds.GraphVertex('F');
        const vertexG = new ds.GraphVertex('G');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC);
        const edgeCG = new ds.GraphEdge(vertexC, vertexG);
        const edgeAD = new ds.GraphEdge(vertexA, vertexD);
        const edgeAE = new ds.GraphEdge(vertexA, vertexE);
        const edgeEF = new ds.GraphEdge(vertexE, vertexF);
        const edgeFD = new ds.GraphEdge(vertexF, vertexD);
        const edgeDG = new ds.GraphEdge(vertexD, vertexG);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeCG)
            .addEdge(edgeAD)
            .addEdge(edgeAE)
            .addEdge(edgeEF)
            .addEdge(edgeFD)
            .addEdge(edgeDG);
        assert.deepEqual(graph.toString(), 'A,B,C,G,D,E,F');
        var enterVertexCallbackCalls = [];
        var leaveVertexCallbackCalls = [];
        const enterVertexCallback = ({ previousVertex, currentVertex }) => enterVertexCallbackCalls.push({ previousVertex, currentVertex });
        const leaveVertexCallback = ({ previousVertex, currentVertex }) => leaveVertexCallbackCalls.push({ previousVertex, currentVertex });
        // Traverse graphs without callbacks first to check default ones.
        ds.depthFirstSearch(graph, vertexA);
        // Traverse graph with enterVertex and leaveVertex callbacks.
        ds.depthFirstSearch(graph, vertexA, {
            enterVertex: enterVertexCallback,
            leaveVertex: leaveVertexCallback,
        });
        assert.deepEqual(enterVertexCallbackCalls.length, graph.getAllVertices().length);
        assert.deepEqual(leaveVertexCallbackCalls.length, graph.getAllVertices().length);
        const enterVertexParamsMap = [
            { currentVertex: vertexA, previousVertex: null },
            { currentVertex: vertexB, previousVertex: vertexA },
            { currentVertex: vertexC, previousVertex: vertexB },
            { currentVertex: vertexG, previousVertex: vertexC },
            { currentVertex: vertexD, previousVertex: vertexA },
            { currentVertex: vertexE, previousVertex: vertexA },
            { currentVertex: vertexF, previousVertex: vertexE },
        ];
        for (let callIndex = 0; callIndex < graph.getAllVertices().length; callIndex += 1) {
            const params = enterVertexCallbackCalls[callIndex];
            assert.deepEqual(params.currentVertex, enterVertexParamsMap[callIndex].currentVertex);
            assert.deepEqual(params.previousVertex, enterVertexParamsMap[callIndex].previousVertex);
        }
        const leaveVertexParamsMap = [
            { currentVertex: vertexG, previousVertex: vertexC },
            { currentVertex: vertexC, previousVertex: vertexB },
            { currentVertex: vertexB, previousVertex: vertexA },
            { currentVertex: vertexD, previousVertex: vertexA },
            { currentVertex: vertexF, previousVertex: vertexE },
            { currentVertex: vertexE, previousVertex: vertexA },
            { currentVertex: vertexA, previousVertex: null },
        ];
        for (let callIndex = 0; callIndex < graph.getAllVertices().length; callIndex += 1) {
            const params = leaveVertexCallbackCalls[callIndex];
            assert.deepEqual(params.currentVertex, leaveVertexParamsMap[callIndex].currentVertex);
            assert.deepEqual(params.previousVertex, leaveVertexParamsMap[callIndex].previousVertex);
        }
    });
    QUnit.test('allow users to redefine vertex visiting logic', (assert) => {
        const graph = new ds.Graph(true);
        const vertexA = new ds.GraphVertex('A');
        const vertexB = new ds.GraphVertex('B');
        const vertexC = new ds.GraphVertex('C');
        const vertexD = new ds.GraphVertex('D');
        const vertexE = new ds.GraphVertex('E');
        const vertexF = new ds.GraphVertex('F');
        const vertexG = new ds.GraphVertex('G');
        const edgeAB = new ds.GraphEdge(vertexA, vertexB);
        const edgeBC = new ds.GraphEdge(vertexB, vertexC);
        const edgeCG = new ds.GraphEdge(vertexC, vertexG);
        const edgeAD = new ds.GraphEdge(vertexA, vertexD);
        const edgeAE = new ds.GraphEdge(vertexA, vertexE);
        const edgeEF = new ds.GraphEdge(vertexE, vertexF);
        const edgeFD = new ds.GraphEdge(vertexF, vertexD);
        const edgeDG = new ds.GraphEdge(vertexD, vertexG);
        graph
            .addEdge(edgeAB)
            .addEdge(edgeBC)
            .addEdge(edgeCG)
            .addEdge(edgeAD)
            .addEdge(edgeAE)
            .addEdge(edgeEF)
            .addEdge(edgeFD)
            .addEdge(edgeDG);
        assert.deepEqual(graph.toString(), 'A,B,C,G,D,E,F');
        var enterVertexCallbackCalls = [];
        var leaveVertexCallbackCalls = [];
        const enterVertexCallback = ({ previousVertex, currentVertex }) => enterVertexCallbackCalls.push({ previousVertex, currentVertex });
        const leaveVertexCallback = ({ previousVertex, currentVertex }) => leaveVertexCallbackCalls.push({ previousVertex, currentVertex });
        ds.depthFirstSearch(graph, vertexA, {
            enterVertex: enterVertexCallback,
            leaveVertex: leaveVertexCallback,
            allowTraversal: ({ currentVertex, nextVertex }) => {
                return !(currentVertex === vertexA && nextVertex === vertexB);
            },
        });
        assert.deepEqual(enterVertexCallbackCalls.length, 7);
        assert.deepEqual(leaveVertexCallbackCalls.length, 7);
        const enterVertexParamsMap = [
            { currentVertex: vertexA, previousVertex: null },
            { currentVertex: vertexD, previousVertex: vertexA },
            { currentVertex: vertexG, previousVertex: vertexD },
            { currentVertex: vertexE, previousVertex: vertexA },
            { currentVertex: vertexF, previousVertex: vertexE },
            { currentVertex: vertexD, previousVertex: vertexF },
            { currentVertex: vertexG, previousVertex: vertexD },
        ];
        for (let callIndex = 0; callIndex < graph.getAllVertices().length; callIndex += 1) {
            const params = enterVertexCallbackCalls[callIndex];
            assert.deepEqual(params.currentVertex, enterVertexParamsMap[callIndex].currentVertex);
            assert.deepEqual(params.previousVertex, enterVertexParamsMap[callIndex].previousVertex);
        }
        const leaveVertexParamsMap = [
            { currentVertex: vertexG, previousVertex: vertexD },
            { currentVertex: vertexD, previousVertex: vertexA },
            { currentVertex: vertexG, previousVertex: vertexD },
            { currentVertex: vertexD, previousVertex: vertexF },
            { currentVertex: vertexF, previousVertex: vertexE },
            { currentVertex: vertexE, previousVertex: vertexA },
            { currentVertex: vertexA, previousVertex: null },
        ];
        for (let callIndex = 0; callIndex < graph.getAllVertices().length; callIndex += 1) {
            const params = leaveVertexCallbackCalls[callIndex];
            assert.deepEqual(params.currentVertex, leaveVertexParamsMap[callIndex].currentVertex);
            assert.deepEqual(params.previousVertex, leaveVertexParamsMap[callIndex].previousVertex);
        }
    });
});
QUnit.module('Comparator', () => {
    QUnit.test('should compare with default comparator function', (assert) => {
        const comparator = new ds.Comparator();
        assert.deepEqual(comparator.equal(0, 0), true);
        assert.deepEqual(comparator.equal(0, 1), false);
        assert.deepEqual(comparator.equal('a', 'a'), true);
        assert.deepEqual(comparator.lessThan(1, 2), true);
        assert.deepEqual(comparator.lessThan(-1, 2), true);
        assert.deepEqual(comparator.lessThan('a', 'b'), true);
        assert.deepEqual(comparator.lessThan('a', 'ab'), true);
        assert.deepEqual(comparator.lessThan(10, 2), false);
        assert.deepEqual(comparator.lessThanOrEqual(10, 2), false);
        assert.deepEqual(comparator.lessThanOrEqual(1, 1), true);
        assert.deepEqual(comparator.lessThanOrEqual(0, 0), true);
        assert.deepEqual(comparator.greaterThan(0, 0), false);
        assert.deepEqual(comparator.greaterThan(10, 0), true);
        assert.deepEqual(comparator.greaterThanOrEqual(10, 0), true);
        assert.deepEqual(comparator.greaterThanOrEqual(10, 10), true);
        assert.deepEqual(comparator.greaterThanOrEqual(0, 10), false);
    });
    QUnit.test('should compare with custom comparator function', (assert) => {
        const comparator = new ds.Comparator((a, b) => {
            if (a.length === b.length) {
                return 0;
            }
            return a.length < b.length ? -1 : 1;
        });
        assert.deepEqual(comparator.equal('a', 'b'), true);
        assert.deepEqual(comparator.equal('a', ''), false);
        assert.deepEqual(comparator.lessThan('b', 'aa'), true);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'aa'), false);
        assert.deepEqual(comparator.greaterThanOrEqual('aa', 'a'), true);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'a'), true);
        comparator.reverse();
        assert.deepEqual(comparator.equal('a', 'b'), true);
        assert.deepEqual(comparator.equal('a', ''), false);
        assert.deepEqual(comparator.lessThan('b', 'aa'), false);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'aa'), true);
        assert.deepEqual(comparator.greaterThanOrEqual('aa', 'a'), false);
        assert.deepEqual(comparator.greaterThanOrEqual('a', 'a'), true);
    });
});
//# sourceMappingURL=tests.js.map
namespace ds
{
    /**
     * 双向链接结点
     */
    export interface DoublyLinkedListNode<T>
    {
        /**
         * 值
         */
        value: T;

        /**
         * 上一个结点
         */
        previous: DoublyLinkedListNode<T>;

        /**
         * 下一个结点
         */
        next: DoublyLinkedListNode<T>;
    }
}
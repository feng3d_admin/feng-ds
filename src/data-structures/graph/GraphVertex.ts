namespace ds
{
    /**
     * 图顶点
     */
    export class GraphVertex<T>
    {
        /**
         * 值
         */
        value: T;

        /**
         * 边列表
         */
        edges: LinkedList<GraphEdge<T>>;

        /**
         * 构建图顶点
         * 
         * @param value 值
         */
        constructor(value: T)
        {
            const edgeComparator = (edgeA: GraphEdge<T>, edgeB: GraphEdge<T>) =>
            {
                if (edgeA.getKey() === edgeB.getKey())
                {
                    return 0;
                }
                return edgeA.getKey() < edgeB.getKey() ? -1 : 1;
            };

            this.value = value;
            this.edges = new LinkedList(edgeComparator);
        }

        /**
         * 新增边
         * 
         * @param edge 边
         */
        addEdge(edge: GraphEdge<T>)
        {
            this.edges.append(edge);

            return this;
        }

        /**
         * 删除边
         * 
         * @param edge 边
         */
        deleteEdge(edge: GraphEdge<T>)
        {
            this.edges.delete(edge);
        }

        /**
         * 获取相邻顶点
         */
        getNeighbors()
        {
            const edges = this.edges.toArray();

            const neighborsConverter = (edge: GraphEdge<T>) =>
            {
                return edge.startVertex === this ? edge.endVertex : edge.startVertex;
            };

            return edges.map(neighborsConverter);
        }

        /**
         * 获取边列表
         */
        getEdges()
        {
            return this.edges.toArray();
        }

        /**
         * 获取边的数量
         */
        getDegree()
        {
            return this.edges.toArray().length;
        }

        /**
         * 是否存在指定边
         * 
         * @param requiredEdge 边
         */
        hasEdge(requiredEdge: GraphEdge<T>)
        {
            const edgeNode = this.edges.findByFunc(
                edge => edge === requiredEdge,
            );

            return !!edgeNode;
        }

        /**
         * 是否有相邻顶点
         * 
         * @param vertex 顶点
         */
        hasNeighbor(vertex: GraphVertex<T>)
        {
            const vertexNode = this.edges.findByFunc(
                edge => edge.startVertex === vertex || edge.endVertex === vertex,
            );

            return !!vertexNode;
        }

        /**
         * 查找边
         * 
         * @param vertex 顶点
         */
        findEdge(vertex: GraphVertex<T>)
        {
            const edgeFinder = (edge: GraphEdge<T>) =>
            {
                return edge.startVertex === vertex || edge.endVertex === vertex;
            };

            const edge = this.edges.findByFunc(edgeFinder);

            return edge ? edge.value : null;
        }

        /**
         * 获取键值
         */
        getKey()
        {
            return <string><any>this.value;
        }

        /**
         * 删除所有边
         */
        deleteAllEdges()
        {
            this.getEdges().forEach(edge => this.deleteEdge(edge));

            return this;
        }

        /**
         * 转换为字符串
         * 
         * @param callback 转换为字符串函数
         */
        toString(callback?: (value: T) => string)
        {
            return callback ? callback(this.value) : `${this.value}`;
        }
    }
}
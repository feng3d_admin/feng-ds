namespace ds
{
    /**
     * 并查集
     * 
     * 并查集是一种树型的数据结构，用于处理一些不交集（Disjoint Sets）的合并及查询问题。
     * 
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/disjoint-set/DisjointSet.js
     * @see https://en.wikipedia.org/wiki/Disjoint-set_data_structure
     * @see https://www.youtube.com/watch?v=wU6udHRIkcc&index=14&t=0s&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     */
    export class DisjointSet<T>
    {

        private items: { [key: string]: DisjointSetItem<T> };

        /**
         * 计算键值函数
         */
        private keyCallback: (value: T) => string;

        /**
         * 构建 并查集
         * @param keyCallback 计算键值函数
         */
        constructor(keyCallback?: (value: T) => string)
        {
            this.keyCallback = keyCallback;
            this.items = {};
        }

        /**
         * 创建集合
         * 
         * @param itemValue 项值 
         */
        makeSet(itemValue: T)
        {
            const disjointSetItem = new DisjointSetItem(itemValue, this.keyCallback);

            if (!this.items[disjointSetItem.getKey()])
            {
                this.items[disjointSetItem.getKey()] = disjointSetItem;
            }

            return this;
        }

        /**
         * 查找给出值所在集合根项键值
         * 
         * @param itemValue 项值
         */
        find(itemValue: T)
        {
            const templateDisjointItem = new DisjointSetItem(itemValue, this.keyCallback);

            const requiredDisjointItem = this.items[templateDisjointItem.getKey()];

            if (!requiredDisjointItem)
            {
                return null;
            }

            return requiredDisjointItem.getRoot().getKey();
        }

        /**
         * 合并两个值所在的集合
         * 
         * @param valueA 值a
         * @param valueB 值b
         */
        union(valueA: T, valueB: T)
        {
            const rootKeyA = this.find(valueA);
            const rootKeyB = this.find(valueB);

            if (rootKeyA === null || rootKeyB === null)
            {
                throw new Error('给出值不全在集合内');
            }

            if (rootKeyA === rootKeyB)
            {
                return this;
            }

            const rootA = this.items[rootKeyA];
            const rootB = this.items[rootKeyB];

            // 小集合合并到大集合中
            if (rootA.getRank() < rootB.getRank())
            {
                rootB.addChild(rootA);

                return this;
            }

            rootA.addChild(rootB);

            return this;
        }

        /**
         * 判断两个值是否在相同集合中
         * 
         * @param valueA 值A
         * @param valueB 值B
         */
        inSameSet(valueA: T, valueB: T)
        {
            const rootKeyA = this.find(valueA);
            const rootKeyB = this.find(valueB);

            if (rootKeyA === null || rootKeyB === null)
            {
                throw new Error('给出的值不全在集合内');
            }

            return rootKeyA === rootKeyB;
        }
    }
}
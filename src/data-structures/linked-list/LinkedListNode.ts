namespace ds
{

    /**
     * 链表结点
     */
    export interface LinkedListNode<T>
    {
        /**
         * 值
         */
        value: T;
        /**
         * 下一个结点
         */
        next: LinkedListNode<T>;
    }
}
var ds;
(function (ds) {
    const DEFAULT_BASE = 37;
    const DEFAULT_MODULUS = 101;
    class PolynomialHash {
        /**
         * @param {number} [base] - Base number that is used to create the polynomial.
         * @param {number} [modulus] - Modulus number that keeps the hash from overflowing.
         */
        constructor({ base = DEFAULT_BASE, modulus = DEFAULT_MODULUS } = {}) {
            this.base = base;
            this.modulus = modulus;
        }
        /**
         * Function that creates hash representation of the word.
         *
         * Time complexity: O(word.length).
         *
         * @param {string} word - String that needs to be hashed.
         * @return {number}
         */
        hash(word) {
            const charCodes = Array.from(word).map(char => this.charToNumber(char));
            let hash = 0;
            for (let charIndex = 0; charIndex < charCodes.length; charIndex += 1) {
                hash *= this.base;
                hash += charCodes[charIndex];
                hash %= this.modulus;
            }
            return hash;
        }
        /**
         * Function that creates hash representation of the word
         * based on previous word (shifted by one character left) hash value.
         *
         * Recalculates the hash representation of a word so that it isn't
         * necessary to traverse the whole word again.
         *
         * Time complexity: O(1).
         *
         * @param {number} prevHash
         * @param {string} prevWord
         * @param {string} newWord
         * @return {number}
         */
        roll(prevHash, prevWord, newWord) {
            let hash = prevHash;
            const prevValue = this.charToNumber(prevWord[0]);
            const newValue = this.charToNumber(newWord[newWord.length - 1]);
            let prevValueMultiplier = 1;
            for (let i = 1; i < prevWord.length; i += 1) {
                prevValueMultiplier *= this.base;
                prevValueMultiplier %= this.modulus;
            }
            hash += this.modulus;
            hash -= (prevValue * prevValueMultiplier) % this.modulus;
            hash *= this.base;
            hash += newValue;
            hash %= this.modulus;
            return hash;
        }
        /**
         * Converts char to number.
         *
         * @param {string} char
         * @return {number}
         */
        charToNumber(char) {
            let charCode = char.codePointAt(0);
            // Check if character has surrogate pair.
            const surrogate = char.codePointAt(1);
            if (surrogate !== undefined) {
                const surrogateShift = Math.pow(2, 16);
                charCode += surrogate * surrogateShift;
            }
            return charCode;
        }
    }
    ds.PolynomialHash = PolynomialHash;
})(ds || (ds = {}));
var ds;
(function (ds) {
    const DEFAULT_BASE = 17;
    class SimplePolynomialHash {
        /**
         * @param {number} [base] - Base number that is used to create the polynomial.
         */
        constructor(base = DEFAULT_BASE) {
            this.base = base;
        }
        /**
         * Function that creates hash representation of the word.
         *
         * Time complexity: O(word.length).
         *
         * @assumption: This version of the function  doesn't use modulo operator.
         * Thus it may produce number overflows by generating numbers that are
         * bigger than Number.MAX_SAFE_INTEGER. This function is mentioned here
         * for simplicity and LEARNING reasons.
         *
         * @param {string} word - String that needs to be hashed.
         * @return {number}
         */
        hash(word) {
            let hash = 0;
            for (let charIndex = 0; charIndex < word.length; charIndex += 1) {
                hash += word.charCodeAt(charIndex) * (Math.pow(this.base, charIndex));
            }
            return hash;
        }
        /**
         * Function that creates hash representation of the word
         * based on previous word (shifted by one character left) hash value.
         *
         * Recalculates the hash representation of a word so that it isn't
         * necessary to traverse the whole word again.
         *
         * Time complexity: O(1).
         *
         * @assumption: This function doesn't use modulo operator and thus is not safe since
         * it may deal with numbers that are bigger than Number.MAX_SAFE_INTEGER. This
         * function is mentioned here for simplicity and LEARNING reasons.
         *
         * @param {number} prevHash
         * @param {string} prevWord
         * @param {string} newWord
         * @return {number}
         */
        roll(prevHash, prevWord, newWord) {
            let hash = prevHash;
            const prevValue = prevWord.charCodeAt(0);
            const newValue = newWord.charCodeAt(newWord.length - 1);
            hash -= prevValue;
            hash /= this.base;
            hash += newValue * (Math.pow(this.base, (newWord.length - 1)));
            return hash;
        }
    }
    ds.SimplePolynomialHash = SimplePolynomialHash;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * @typedef {Object} Callbacks
     *
     * @property {function(vertices: Object): boolean} [allowTraversal] -
     *  Determines whether DFS should traverse from the vertex to its neighbor
     *  (along the edge). By default prohibits visiting the same vertex again.
     *
     * @property {function(vertices: Object)} [enterVertex] - Called when DFS enters the vertex.
     *
     * @property {function(vertices: Object)} [leaveVertex] - Called when DFS leaves the vertex.
     */
    /**
     * @param {Callbacks} [callbacks]
     * @returns {Callbacks}
     */
    function initCallbacks(callbacks = {}) {
        const initiatedCallback = callbacks;
        const stubCallback = () => { };
        const allowTraversalCallback = (() => {
            const seen = {};
            return ({ nextVertex }) => {
                if (!seen[nextVertex.getKey()]) {
                    seen[nextVertex.getKey()] = true;
                    return true;
                }
                return false;
            };
        })();
        initiatedCallback.allowTraversal = callbacks.allowTraversal || allowTraversalCallback;
        initiatedCallback.enterVertex = callbacks.enterVertex || stubCallback;
        initiatedCallback.leaveVertex = callbacks.leaveVertex || stubCallback;
        return initiatedCallback;
    }
    /**
     * @param {Graph} graph
     * @param {GraphVertex} currentVertex
     * @param {GraphVertex} previousVertex
     * @param {Callbacks} callbacks
     */
    function depthFirstSearchRecursive(graph, currentVertex, previousVertex, callbacks) {
        callbacks.enterVertex({ currentVertex, previousVertex });
        graph.getNeighbors(currentVertex).forEach((nextVertex) => {
            if (callbacks.allowTraversal({ previousVertex, currentVertex, nextVertex })) {
                depthFirstSearchRecursive(graph, nextVertex, currentVertex, callbacks);
            }
        });
        callbacks.leaveVertex({ currentVertex, previousVertex });
    }
    /**
     * @param {Graph} graph
     * @param {GraphVertex} startVertex
     * @param {Callbacks} [callbacks]
     */
    function depthFirstSearch(graph, startVertex, callbacks = {}) {
        const previousVertex = null;
        depthFirstSearchRecursive(graph, startVertex, previousVertex, initCallbacks(callbacks));
    }
    ds.depthFirstSearch = depthFirstSearch;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 判断是否为2的指数
     *
     * @param number 整数
     */
    function isPowerOfTwo(number) {
        // 1 (2^0) is the smallest power of two.
        if (number < 1) {
            return false;
        }
        // Let's find out if we can divide the number by two
        // many times without remainder.
        let dividedNumber = number;
        while (dividedNumber !== 1) {
            if (dividedNumber % 2 !== 0) {
                // For every case when remainder isn't zero we can say that this number
                // couldn't be a result of power of two.
                return false;
            }
            dividedNumber /= 2;
        }
        return true;
    }
    ds.isPowerOfTwo = isPowerOfTwo;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 布隆过滤器 （ 在 JavaScript中 该类可由Object对象代替）
     *
     * 用于判断某元素是否可能插入
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/bloom-filter/BloomFilter.js
     * @see https://baike.baidu.com/item/%E5%B8%83%E9%9A%86%E8%BF%87%E6%BB%A4%E5%99%A8
     */
    class BloomFilter {
        /**
         *
         * @param size 尺寸
         */
        constructor(size = 100) {
            this.size = 100;
            this.size = size;
            this.storage = this.createStore(size);
        }
        /**
         * 插入
         *
         * @param item 元素
         */
        insert(item) {
            const hashValues = this.getHashValues(item);
            hashValues.forEach(val => this.storage.setValue(val));
        }
        /**
         * 可能包含
         *
         * @param item 元素
         */
        mayContain(item) {
            const hashValues = this.getHashValues(item);
            for (let hashIndex = 0; hashIndex < hashValues.length; hashIndex += 1) {
                if (!this.storage.getValue(hashValues[hashIndex])) {
                    // 我们知道项目肯定没有插入。
                    return false;
                }
            }
            // 项目可能已经插入，也可能没有插入。
            return true;
        }
        /**
         * 创建存储器
         * @param size 尺寸
         */
        createStore(size) {
            const storage = [];
            // 初始化
            for (let storageCellIndex = 0; storageCellIndex < size; storageCellIndex += 1) {
                storage.push(false);
            }
            const storageInterface = {
                getValue(index) {
                    return storage[index];
                },
                setValue(index) {
                    storage[index] = true;
                },
            };
            return storageInterface;
        }
        /**
         * 计算哈希值1
         *
         * @param item 元素
         */
        hash1(item) {
            let hash = 0;
            for (let charIndex = 0; charIndex < item.length; charIndex += 1) {
                const char = item.charCodeAt(charIndex);
                hash = (hash << 5) + hash + char;
                hash &= hash; // Convert to 32bit integer
                hash = Math.abs(hash);
            }
            return hash % this.size;
        }
        /**
         * 计算哈希值2
         *
         * @param item 元素
         */
        hash2(item) {
            let hash = 5381;
            for (let charIndex = 0; charIndex < item.length; charIndex += 1) {
                const char = item.charCodeAt(charIndex);
                hash = (hash << 5) + hash + char; /* hash * 33 + c */
            }
            return Math.abs(hash % this.size);
        }
        /**
         * 计算哈希值3
         *
         * @param item 元素
         */
        hash3(item) {
            let hash = 0;
            for (let charIndex = 0; charIndex < item.length; charIndex += 1) {
                const char = item.charCodeAt(charIndex);
                hash = (hash << 5) - hash;
                hash += char;
                hash &= hash; // Convert to 32bit integer
            }
            return Math.abs(hash % this.size);
        }
        /**
         * 获取3个哈希值组成的数组
         */
        getHashValues(item) {
            return [
                this.hash1(item),
                this.hash2(item),
                this.hash3(item),
            ];
        }
    }
    ds.BloomFilter = BloomFilter;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 并查集
     *
     * 并查集是一种树型的数据结构，用于处理一些不交集（Disjoint Sets）的合并及查询问题。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/disjoint-set/DisjointSet.js
     * @see https://en.wikipedia.org/wiki/Disjoint-set_data_structure
     * @see https://www.youtube.com/watch?v=wU6udHRIkcc&index=14&t=0s&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     */
    class DisjointSet {
        /**
         * 构建 并查集
         * @param keyCallback 计算键值函数
         */
        constructor(keyCallback) {
            this.keyCallback = keyCallback;
            this.items = {};
        }
        /**
         * 创建集合
         *
         * @param itemValue 项值
         */
        makeSet(itemValue) {
            const disjointSetItem = new ds.DisjointSetItem(itemValue, this.keyCallback);
            if (!this.items[disjointSetItem.getKey()]) {
                this.items[disjointSetItem.getKey()] = disjointSetItem;
            }
            return this;
        }
        /**
         * 查找给出值所在集合根项键值
         *
         * @param itemValue 项值
         */
        find(itemValue) {
            const templateDisjointItem = new ds.DisjointSetItem(itemValue, this.keyCallback);
            const requiredDisjointItem = this.items[templateDisjointItem.getKey()];
            if (!requiredDisjointItem) {
                return null;
            }
            return requiredDisjointItem.getRoot().getKey();
        }
        /**
         * 合并两个值所在的集合
         *
         * @param valueA 值a
         * @param valueB 值b
         */
        union(valueA, valueB) {
            const rootKeyA = this.find(valueA);
            const rootKeyB = this.find(valueB);
            if (rootKeyA === null || rootKeyB === null) {
                throw new Error('给出值不全在集合内');
            }
            if (rootKeyA === rootKeyB) {
                return this;
            }
            const rootA = this.items[rootKeyA];
            const rootB = this.items[rootKeyB];
            // 小集合合并到大集合中
            if (rootA.getRank() < rootB.getRank()) {
                rootB.addChild(rootA);
                return this;
            }
            rootA.addChild(rootB);
            return this;
        }
        /**
         * 判断两个值是否在相同集合中
         *
         * @param valueA 值A
         * @param valueB 值B
         */
        inSameSet(valueA, valueB) {
            const rootKeyA = this.find(valueA);
            const rootKeyB = this.find(valueB);
            if (rootKeyA === null || rootKeyB === null) {
                throw new Error('给出的值不全在集合内');
            }
            return rootKeyA === rootKeyB;
        }
    }
    ds.DisjointSet = DisjointSet;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 并查集项
     */
    class DisjointSetItem {
        /**
         * 构建 并查集 项
         *
         * @param value 值
         * @param keyCallback 计算键值函数
         */
        constructor(value, keyCallback) {
            this.value = value;
            this.keyCallback = keyCallback;
            this.parent = null;
            this.children = {};
        }
        /**
         * 获取键值
         */
        getKey() {
            if (this.keyCallback) {
                return this.keyCallback(this.value);
            }
            return this.value;
        }
        /**
         * 获取根项
         */
        getRoot() {
            return this.isRoot() ? this : this.parent.getRoot();
        }
        /**
         * 是否为根项
         */
        isRoot() {
            return this.parent === null;
        }
        /**
         * 获取所有子孙项数量
         */
        getRank() {
            if (this.getChildren().length === 0) {
                return 0;
            }
            let rank = 0;
            this.getChildren().forEach((child) => {
                rank += 1;
                rank += child.getRank();
            });
            return rank;
        }
        /**
         * 获取子项列表
         */
        getChildren() {
            var values = Object.keys(this.children).map(key => this.children[key]);
            return values;
        }
        /**
         * 设置父项
         * @param parentItem 父项
         */
        setParent(parentItem) {
            this.parent = parentItem;
            this.parent.children[this.getKey()] = this;
            return this;
        }
        /**
         * 添加子项
         * @param childItem 子项
         */
        addChild(childItem) {
            this.children[childItem.getKey()] = childItem;
            childItem.parent = this;
            return this;
        }
    }
    ds.DisjointSetItem = DisjointSetItem;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
    * 双向链表
    *
    * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/doubly-linked-list/DoublyLinkedList.js
    */
    class DoublyLinkedList {
        /**
         * 构建双向链表
         *
         * @param comparatorFunction 比较函数
         */
        constructor(comparatorFunction) {
            this.head = null;
            this.tail = null;
            this.compare = new ds.Comparator(comparatorFunction);
        }
        /**
         * 是否为空
         */
        isEmpty() {
            return !this.head;
        }
        /**
         * 清空
         */
        empty() {
            this.head = null;
            this.tail = null;
        }
        /**
         * 添加新结点到表头
         *
         * @param value 结点数据
         */
        prepend(value) {
            const newNode = { value: value, previous: null, next: this.head };
            if (this.head)
                this.head.previous = newNode;
            this.head = newNode;
            if (!this.tail)
                this.tail = newNode;
            return this;
        }
        /**
         * 添加新结点到表尾
         *
         * @param value 结点数据
         */
        append(value) {
            var newNode = { value: value, previous: this.tail, next: null };
            if (this.tail)
                this.tail.next = newNode;
            this.tail = newNode;
            if (!this.head)
                this.head = newNode;
            return this;
        }
        /**
         * 删除链表中第一个与指定值相等的结点
         *
         * @param value 结点值
         */
        delete(value) {
            if (!this.head)
                return null;
            let deletedNode = null;
            // 从表头删除结点
            while (this.head && !deletedNode && this.compare.equal(this.head.value, value)) {
                deletedNode = this.head;
                this.head = this.head.next;
                if (this.head)
                    this.head.previous = null;
            }
            let currentNode = this.head;
            if (!deletedNode && currentNode) {
                // 删除相等的下一个结点
                while (!deletedNode && currentNode.next) {
                    if (this.compare.equal(currentNode.next.value, value)) {
                        deletedNode = currentNode.next;
                        currentNode.next = currentNode.next.next;
                        if (currentNode.next)
                            currentNode.next.previous = currentNode;
                    }
                    else {
                        currentNode = currentNode.next;
                    }
                }
            }
            // currentNode 是否为表尾
            if (currentNode == null || currentNode.next == null) {
                this.tail = currentNode;
            }
            return deletedNode;
        }
        /**
         * 删除链表中所有与指定值相等的结点
         *
         * @param value 结点值
         */
        deleteAll(value) {
            if (!this.head)
                return null;
            let deletedNode = null;
            // 从表头删除结点
            while (this.head && this.compare.equal(this.head.value, value)) {
                deletedNode = this.head;
                this.head = this.head.next;
                if (this.head)
                    this.head.previous = null;
            }
            let currentNode = this.head;
            if (currentNode !== null) {
                // 删除相等的下一个结点
                while (currentNode.next) {
                    if (this.compare.equal(currentNode.next.value, value)) {
                        deletedNode = currentNode.next;
                        currentNode.next = currentNode.next.next;
                        if (currentNode.next)
                            currentNode.next.previous = currentNode;
                    }
                    else {
                        currentNode = currentNode.next;
                    }
                }
            }
            // currentNode 是否为表尾
            if (currentNode == null || currentNode.next == null) {
                this.tail = currentNode;
            }
            return deletedNode;
        }
        /**
         * 查找与结点值相等的结点
         *
         * @param value 结点值
         */
        find(value) {
            if (!this.head)
                return null;
            let currentNode = this.head;
            while (currentNode) {
                if (this.compare.equal(currentNode.value, value))
                    return currentNode;
                currentNode = currentNode.next;
            }
            return null;
        }
        /**
         * 查找与结点值相等的结点
         *
         * @param callback 判断是否为查找的元素
         */
        findByFunc(callback) {
            if (!this.head)
                return null;
            let currentNode = this.head;
            while (currentNode) {
                if (callback(currentNode.value))
                    return currentNode;
                currentNode = currentNode.next;
            }
            return null;
        }
        /**
         * 删除表头
         */
        deleteHead() {
            if (!this.head)
                return null;
            const deletedHead = this.head;
            if (this.head.next) {
                this.head = this.head.next;
                this.head.previous = null;
            }
            else {
                this.head = null;
                this.tail = null;
            }
            return deletedHead.value;
        }
        /**
         * 删除表尾
         */
        deleteTail() {
            if (!this.tail)
                return null;
            const deletedTail = this.tail;
            if (this.head === this.tail) {
                this.head = null;
                this.tail = null;
                return deletedTail.value;
            }
            this.tail = this.tail.previous;
            this.tail.next = null;
            return deletedTail.value;
        }
        /**
         * 从数组中初始化链表
         *
         * @param values 结点值列表
         */
        fromArray(values) {
            this.empty();
            values.forEach(value => this.append(value));
            return this;
        }
        /**
         * 转换为数组
         */
        toArray() {
            var values = [];
            var currentNode = this.head;
            while (currentNode) {
                values.push(currentNode.value);
                currentNode = currentNode.next;
            }
            return values;
        }
        /**
         * 转换为字符串
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString) {
            return this.toArray().map(value => valueToString ? valueToString(value) : `${value}`).toString();
        }
        /**
         * 反转链表
         */
        reverse() {
            let currNode = this.head;
            let prevNode = null;
            let nextNode = null;
            while (currNode) {
                // 存储当前结点的next与previous指向
                nextNode = currNode.next;
                prevNode = currNode.previous;
                // 反转结点的next与previous指向
                currNode.next = prevNode;
                currNode.previous = nextNode;
                // 存储上一个节点
                prevNode = currNode;
                // 遍历指针向后移动
                currNode = nextNode;
            }
            // 重置表头与表尾
            this.tail = this.head;
            this.head = prevNode;
            return this;
        }
        /**
         * 核查结构是否正确
         */
        checkStructure() {
            if (this.head) {
                // 核查正向链表
                var currNode = this.head;
                while (currNode.next) {
                    currNode = currNode.next;
                }
                if (this.tail !== currNode)
                    return false;
                // 核查逆向链表
                currNode = this.tail;
                while (currNode.previous) {
                    currNode = currNode.previous;
                }
                return this.head == currNode;
            }
            return !this.tail;
        }
    }
    ds.DoublyLinkedList = DoublyLinkedList;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 图
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/graph/Graph.js
     * @see https://en.wikipedia.org/wiki/Graph_(abstract_data_type)
     * @see https://www.youtube.com/watch?v=gXgEDyodOJU&index=9&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     * @see https://www.youtube.com/watch?v=k1wraWzqtvQ&index=10&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     */
    class Graph {
        /**
         * 构建图
         *
         * @param isDirected 是否有向
         */
        constructor(isDirected = false) {
            /**
             * 是否有向
             */
            this.isDirected = false;
            this.vertices = {};
            this.edges = {};
            this.isDirected = isDirected;
        }
        /**
         * 新增顶点
         *
         * @param newVertex 新顶点
         */
        addVertex(newVertex) {
            this.vertices[newVertex.getKey()] = newVertex;
            return this;
        }
        /**
         * 获取顶点
         *
         * @param vertexKey 顶点键值
         */
        getVertexByKey(vertexKey) {
            return this.vertices[vertexKey];
        }
        /**
         * 获取相邻点
         *
         * @param vertex 顶点
         */
        getNeighbors(vertex) {
            return vertex.getNeighbors();
        }
        /**
         * 获取所有顶点
         */
        getAllVertices() {
            var values = Object.keys(this.vertices).map(key => this.vertices[key]);
            return values;
        }
        /**
         * 获取所有边
         */
        getAllEdges() {
            var values = Object.keys(this.edges).map(key => this.edges[key]);
            return values;
        }
        /**
         * 新增边
         *
         * @param edge 边
         */
        addEdge(edge) {
            // 获取起点与终点
            let startVertex = this.getVertexByKey(edge.startVertex.getKey());
            let endVertex = this.getVertexByKey(edge.endVertex.getKey());
            // 新增不存在的起点
            if (!startVertex) {
                this.addVertex(edge.startVertex);
                startVertex = this.getVertexByKey(edge.startVertex.getKey());
            }
            // 新增不存在的终点
            if (!endVertex) {
                this.addVertex(edge.endVertex);
                endVertex = this.getVertexByKey(edge.endVertex.getKey());
            }
            // 新增边到边列表
            if (this.edges[edge.getKey()]) {
                throw new Error('指定边已经存在，无法再次添加');
            }
            else {
                this.edges[edge.getKey()] = edge;
            }
            // 新增边到顶点
            if (this.isDirected) {
                startVertex.addEdge(edge);
            }
            else {
                startVertex.addEdge(edge);
                endVertex.addEdge(edge);
            }
            return this;
        }
        /**
         * 删除边
         *
         * @param edge 边
         */
        deleteEdge(edge) {
            // 从列表中删除边
            if (this.edges[edge.getKey()]) {
                delete this.edges[edge.getKey()];
            }
            else {
                throw new Error('图中不存在指定边');
            }
            // 从起点与终点里删除边
            const startVertex = this.getVertexByKey(edge.startVertex.getKey());
            const endVertex = this.getVertexByKey(edge.endVertex.getKey());
            startVertex.deleteEdge(edge);
            endVertex.deleteEdge(edge);
        }
        /**
         * 查找边
         *
         * @param startVertex 起始顶点
         * @param endVertex 结束顶点
         */
        findEdge(startVertex, endVertex) {
            const vertex = this.getVertexByKey(startVertex.getKey());
            if (!vertex) {
                return null;
            }
            return vertex.findEdge(endVertex);
        }
        /**
         * 获取权重
         */
        getWeight() {
            return this.getAllEdges().reduce((weight, graphEdge) => {
                return weight + graphEdge.weight;
            }, 0);
        }
        /**
         * 反转
         */
        reverse() {
            // 遍历边
            this.getAllEdges().forEach((edge) => {
                // 删除边
                this.deleteEdge(edge);
                // 反转边
                edge.reverse();
                // 新增边
                this.addEdge(edge);
            });
            return this;
        }
        /**
         * 获取所有顶点索引
         */
        getVerticesIndices() {
            const verticesIndices = {};
            this.getAllVertices().forEach((vertex, index) => {
                verticesIndices[vertex.getKey()] = index;
            });
            return verticesIndices;
        }
        /**
         * 获取邻接矩阵
         */
        getAdjacencyMatrix() {
            const vertices = this.getAllVertices();
            const verticesIndices = this.getVerticesIndices();
            // 初始化邻接矩阵
            const adjacencyMatrix = [];
            var n = vertices.length;
            for (let i = 0; i < n; i++) {
                adjacencyMatrix[i] = [];
                for (let j = 0; j < n; j++) {
                    adjacencyMatrix[i][j] = Infinity;
                }
            }
            // 填充邻接矩阵
            vertices.forEach((vertex, vertexIndex) => {
                vertex.getNeighbors().forEach((neighbor) => {
                    const neighborIndex = verticesIndices[neighbor.getKey()];
                    adjacencyMatrix[vertexIndex][neighborIndex] = this.findEdge(vertex, neighbor).weight;
                });
            });
            return adjacencyMatrix;
        }
        /**
         * 转换为字符串
         */
        toString() {
            return Object.keys(this.vertices).toString();
        }
    }
    ds.Graph = Graph;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 图边
     */
    class GraphEdge {
        /**
         * 构建图边
         * @param startVertex 起始顶点
         * @param endVertex 结束顶点
         * @param weight 权重
         */
        constructor(startVertex, endVertex, weight = 0) {
            this.startVertex = startVertex;
            this.endVertex = endVertex;
            this.weight = weight;
        }
        /**
         * 获取键值
         */
        getKey() {
            const startVertexKey = this.startVertex.getKey();
            const endVertexKey = this.endVertex.getKey();
            return `${startVertexKey}_${endVertexKey}`;
        }
        /**
         * 反转
         */
        reverse() {
            const tmp = this.startVertex;
            this.startVertex = this.endVertex;
            this.endVertex = tmp;
            return this;
        }
        /**
         * 转换为字符串
         */
        toString() {
            return this.getKey();
        }
    }
    ds.GraphEdge = GraphEdge;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 图顶点
     */
    class GraphVertex {
        /**
         * 构建图顶点
         *
         * @param value 值
         */
        constructor(value) {
            const edgeComparator = (edgeA, edgeB) => {
                if (edgeA.getKey() === edgeB.getKey()) {
                    return 0;
                }
                return edgeA.getKey() < edgeB.getKey() ? -1 : 1;
            };
            this.value = value;
            this.edges = new ds.LinkedList(edgeComparator);
        }
        /**
         * 新增边
         *
         * @param edge 边
         */
        addEdge(edge) {
            this.edges.append(edge);
            return this;
        }
        /**
         * 删除边
         *
         * @param edge 边
         */
        deleteEdge(edge) {
            this.edges.delete(edge);
        }
        /**
         * 获取相邻顶点
         */
        getNeighbors() {
            const edges = this.edges.toArray();
            const neighborsConverter = (edge) => {
                return edge.startVertex === this ? edge.endVertex : edge.startVertex;
            };
            return edges.map(neighborsConverter);
        }
        /**
         * 获取边列表
         */
        getEdges() {
            return this.edges.toArray();
        }
        /**
         * 获取边的数量
         */
        getDegree() {
            return this.edges.toArray().length;
        }
        /**
         * 是否存在指定边
         *
         * @param requiredEdge 边
         */
        hasEdge(requiredEdge) {
            const edgeNode = this.edges.findByFunc(edge => edge === requiredEdge);
            return !!edgeNode;
        }
        /**
         * 是否有相邻顶点
         *
         * @param vertex 顶点
         */
        hasNeighbor(vertex) {
            const vertexNode = this.edges.findByFunc(edge => edge.startVertex === vertex || edge.endVertex === vertex);
            return !!vertexNode;
        }
        /**
         * 查找边
         *
         * @param vertex 顶点
         */
        findEdge(vertex) {
            const edgeFinder = (edge) => {
                return edge.startVertex === vertex || edge.endVertex === vertex;
            };
            const edge = this.edges.findByFunc(edgeFinder);
            return edge ? edge.value : null;
        }
        /**
         * 获取键值
         */
        getKey() {
            return this.value;
        }
        /**
         * 删除所有边
         */
        deleteAllEdges() {
            this.getEdges().forEach(edge => this.deleteEdge(edge));
            return this;
        }
        /**
         * 转换为字符串
         *
         * @param callback 转换为字符串函数
         */
        toString(callback) {
            return callback ? callback(this.value) : `${this.value}`;
        }
    }
    ds.GraphVertex = GraphVertex;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 默认哈希表 （建议使用Object代替）
     *
     * 哈希表的大小直接影响冲突的次数。
     * 哈希表的大小越大，冲突就越少。
     */
    const defaultHashTableSize = 32;
    /**
     * 哈希表（散列表）
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/hash-table/HashTable.js
     */
    class HashTable {
        /**
         * 构建哈希表
         * @param hashTableSize 哈希表尺寸
         */
        constructor(hashTableSize = defaultHashTableSize) {
            this.buckets = [];
            for (let i = 0; i < hashTableSize; i++) {
                this.buckets.push(new ds.LinkedList());
            }
            this.keys = {};
        }
        /**
         * 将字符串键转换为哈希数。
         *
         * @param key 字符串键
         */
        hash(key) {
            const hash = key.split("").reduce((hashAccumulator, char) => (hashAccumulator + char.charCodeAt(0)), 0);
            return hash % this.buckets.length;
        }
        /**
         * 设置值
         *
         * @param key 键
         * @param value 值
         */
        set(key, value) {
            var keyValue = { key: key, value: value };
            const keyHash = this.hash(key);
            this.keys[key] = keyHash;
            const bucketLinkedList = this.buckets[keyHash];
            const node = bucketLinkedList.findByFunc((v) => v.key === key);
            if (!node) {
                bucketLinkedList.append(keyValue);
            }
            else {
                node.value.value = value;
            }
        }
        /**
         * 删除指定键以及对于值
         *
         * @param key 键
         */
        delete(key) {
            const keyHash = this.hash(key);
            delete this.keys[key];
            const bucketLinkedList = this.buckets[keyHash];
            const node = bucketLinkedList.findByFunc((v) => v.key === key);
            if (node) {
                return bucketLinkedList.deleteAll(node.value);
            }
            return null;
        }
        /**
         * 获取与键对应的值
         *
         * @param key 键
         */
        get(key) {
            const bucketLinkedList = this.buckets[this.hash(key)];
            const node = bucketLinkedList.findByFunc((v) => v.key === key);
            return node ? node.value.value : undefined;
        }
        /**
         * 是否拥有键
         *
         * @param key 键
         */
        has(key) {
            return Object.hasOwnProperty.call(this.keys, key);
        }
        /**
         * 获取键列表
         */
        getKeys() {
            return Object.keys(this.keys);
        }
    }
    ds.HashTable = HashTable;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 堆
     *
     * 最小和最大堆的父类。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/heap/Heap.js
     */
    class Heap {
        /**
         * 构建链表
         *
         * @param comparatorFunction 比较函数
         */
        constructor(comparatorFunction) {
            if (new.target === Heap) {
                throw new TypeError('无法直接构造堆实例');
            }
            this.heapContainer = [];
            this.compare = new ds.Comparator(comparatorFunction);
        }
        /**
         * 获取左边子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getLeftChildIndex(parentIndex) {
            return (2 * parentIndex) + 1;
        }
        /**
         * 获取右边子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getRightChildIndex(parentIndex) {
            return (2 * parentIndex) + 2;
        }
        /**
         * 获取父结点索引
         *
         * @param childIndex 子结点索引
         */
        getParentIndex(childIndex) {
            return Math.floor((childIndex - 1) / 2);
        }
        /**
         * 是否有父结点
         *
         * @param childIndex 子结点索引
         */
        hasParent(childIndex) {
            return this.getParentIndex(childIndex) >= 0;
        }
        /**
         * 是否有左结点
         *
         * @param parentIndex 父结点索引
         */
        hasLeftChild(parentIndex) {
            return this.getLeftChildIndex(parentIndex) < this.heapContainer.length;
        }
        /**
         * 是否有右结点
         *
         * @param parentIndex 父结点索引
         */
        hasRightChild(parentIndex) {
            return this.getRightChildIndex(parentIndex) < this.heapContainer.length;
        }
        /**
         * 获取左结点
         *
         * @param parentIndex 父结点索引
         */
        leftChild(parentIndex) {
            return this.heapContainer[this.getLeftChildIndex(parentIndex)];
        }
        /**
         * 获取右结点
         *
         * @param parentIndex 父结点索引
         */
        rightChild(parentIndex) {
            return this.heapContainer[this.getRightChildIndex(parentIndex)];
        }
        /**
         * 获取父结点
         *
         * @param childIndex 子结点索引
         */
        parent(childIndex) {
            return this.heapContainer[this.getParentIndex(childIndex)];
        }
        /**
         * 交换两个结点数据
         *
         * @param index1 索引1
         * @param index2 索引2
         */
        swap(index1, index2) {
            const tmp = this.heapContainer[index2];
            this.heapContainer[index2] = this.heapContainer[index1];
            this.heapContainer[index1] = tmp;
        }
        /**
         * 查看堆顶数据
         */
        peek() {
            if (this.heapContainer.length === 0)
                return null;
            return this.heapContainer[0];
        }
        /**
         * 出堆
         *
         * 取出堆顶元素
         */
        poll() {
            if (this.heapContainer.length === 0)
                return null;
            if (this.heapContainer.length === 1)
                return this.heapContainer.pop();
            const item = this.heapContainer[0];
            // 将最后一个元素从末尾移动到堆顶。
            this.heapContainer[0] = this.heapContainer.pop();
            this.heapifyDown();
            return item;
        }
        /**
         * 新增元素
         *
         * @param item 元素
         */
        add(item) {
            this.heapContainer.push(item);
            this.heapifyUp();
            return this;
        }
        /**
         * 移除所有指定元素
         *
         * @param item 元素
         * @param comparator 比较器
         */
        remove(item, comparator = this.compare) {
            // 找到要删除的项的数量。
            const numberOfItemsToRemove = this.find(item, comparator).length;
            for (let iteration = 0; iteration < numberOfItemsToRemove; iteration += 1) {
                // 获取一个删除元素索引
                const indexToRemove = this.find(item, comparator).pop();
                // 删除元素为最后一个索引时
                if (indexToRemove === (this.heapContainer.length - 1)) {
                    this.heapContainer.pop();
                }
                else {
                    // 把数组最后元素移动到删除位置
                    this.heapContainer[indexToRemove] = this.heapContainer.pop();
                    const parentItem = this.parent(indexToRemove);
                    if (this.hasLeftChild(indexToRemove)
                        && (!parentItem
                            || this.pairIsInCorrectOrder(parentItem, this.heapContainer[indexToRemove]))) {
                        this.heapifyDown(indexToRemove);
                    }
                    else {
                        this.heapifyUp(indexToRemove);
                    }
                }
            }
            return this;
        }
        /**
         * 查找元素所在所有索引
         *
         * @param item 查找的元素
         * @param comparator 比较器
         */
        find(item, comparator = this.compare) {
            const foundItemIndices = [];
            for (let itemIndex = 0; itemIndex < this.heapContainer.length; itemIndex += 1) {
                if (comparator.equal(item, this.heapContainer[itemIndex])) {
                    foundItemIndices.push(itemIndex);
                }
            }
            return foundItemIndices;
        }
        /**
         * 是否为空
         */
        isEmpty() {
            return !this.heapContainer.length;
        }
        /**
         * 转换为字符串
         */
        toString() {
            return this.heapContainer.toString();
        }
        /**
         * 堆冒泡
         *
         * @param startIndex 堆冒泡起始索引
         */
        heapifyUp(startIndex) {
            let currentIndex = startIndex || this.heapContainer.length - 1;
            while (this.hasParent(currentIndex)
                && !this.pairIsInCorrectOrder(this.parent(currentIndex), this.heapContainer[currentIndex])) {
                this.swap(currentIndex, this.getParentIndex(currentIndex));
                currentIndex = this.getParentIndex(currentIndex);
            }
        }
        /**
         * 堆下沉
         *
         * @param startIndex 堆下沉起始索引
         */
        heapifyDown(startIndex = 0) {
            let currentIndex = startIndex;
            let nextIndex = null;
            while (this.hasLeftChild(currentIndex)) {
                if (this.hasRightChild(currentIndex)
                    && this.pairIsInCorrectOrder(this.rightChild(currentIndex), this.leftChild(currentIndex))) {
                    nextIndex = this.getRightChildIndex(currentIndex);
                }
                else {
                    nextIndex = this.getLeftChildIndex(currentIndex);
                }
                if (this.pairIsInCorrectOrder(this.heapContainer[currentIndex], this.heapContainer[nextIndex])) {
                    break;
                }
                this.swap(currentIndex, nextIndex);
                currentIndex = nextIndex;
            }
        }
    }
    ds.Heap = Heap;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 最大堆
     *
     * 所有父结点都大于子结点
     */
    class MaxHeap extends ds.Heap {
        /**
         * 检查堆元素对的顺序是否正确。
         * 对于MinHeap，第一个元素必须总是小于等于。
         * 对于MaxHeap，第一个元素必须总是大于或等于。
         *
         * @param firstElement 第一个元素
         * @param secondElement 第二个元素
         */
        pairIsInCorrectOrder(firstElement, secondElement) {
            return this.compare.greaterThanOrEqual(firstElement, secondElement);
        }
    }
    ds.MaxHeap = MaxHeap;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 最小堆
     *
     * 所有父结点都小于子结点
     */
    class MinHeap extends ds.Heap {
        /**
         * 检查堆元素对的顺序是否正确。
         * 对于MinHeap，第一个元素必须总是小于等于。
         * 对于MaxHeap，第一个元素必须总是大于或等于。
         *
         * @param firstElement 第一个元素
         * @param secondElement 第二个元素
         */
        pairIsInCorrectOrder(firstElement, secondElement) {
            return this.compare.lessThanOrEqual(firstElement, secondElement);
        }
    }
    ds.MinHeap = MinHeap;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 链表
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/linked-list/LinkedList.js
     */
    class LinkedList {
        /**
         * 构建双向链表
         *
         * @param comparatorFunction 比较函数
         */
        constructor(comparatorFunction) {
            this.head = null;
            this.tail = null;
            this.compare = new ds.Comparator(comparatorFunction);
        }
        /**
         * 是否为空
         */
        isEmpty() {
            return !this.head;
        }
        /**
         * 清空
         */
        empty() {
            this.head = null;
            this.tail = null;
        }
        /**
         * 获取表头值
         */
        getHeadValue() {
            return this.head && this.head.value;
        }
        /**
         * 添加新结点到表头
         *
         * @param value 结点数据
         */
        prepend(value) {
            var newNode = { value: value, next: this.head };
            this.head = newNode;
            if (!this.tail)
                this.tail = newNode;
            return this;
        }
        /**
         * 添加新结点到表尾
         *
         * @param value 结点数据
         */
        append(value) {
            var newNode = { value: value, next: null };
            if (this.tail)
                this.tail.next = newNode;
            this.tail = newNode;
            if (!this.head)
                this.head = newNode;
            return this;
        }
        /**
         * 删除链表中第一个与指定值相等的结点
         *
         * @param value 结点值
         */
        delete(value) {
            if (!this.head)
                return null;
            let deletedNode = null;
            // 从表头删除结点
            while (this.head && !deletedNode && this.compare.equal(this.head.value, value)) {
                deletedNode = this.head;
                this.head = this.head.next;
            }
            let currentNode = this.head;
            if (!deletedNode && currentNode) {
                // 删除相等的下一个结点
                while (!deletedNode && currentNode.next) {
                    if (this.compare.equal(currentNode.next.value, value)) {
                        deletedNode = currentNode.next;
                        currentNode.next = currentNode.next.next;
                    }
                    else {
                        currentNode = currentNode.next;
                    }
                }
            }
            // currentNode 是否为表尾
            if (currentNode == null || currentNode.next == null) {
                this.tail = currentNode;
            }
            return deletedNode;
        }
        /**
         * 删除链表中所有与指定值相等的结点
         *
         * @param value 结点值
         */
        deleteAll(value) {
            if (!this.head)
                return null;
            let deletedNode = null;
            // 从表头删除结点
            while (this.head && this.compare.equal(this.head.value, value)) {
                deletedNode = this.head;
                this.head = this.head.next;
            }
            let currentNode = this.head;
            if (currentNode !== null) {
                // 删除相等的下一个结点
                while (currentNode.next) {
                    if (this.compare.equal(currentNode.next.value, value)) {
                        deletedNode = currentNode.next;
                        currentNode.next = currentNode.next.next;
                    }
                    else {
                        currentNode = currentNode.next;
                    }
                }
            }
            // currentNode 是否为表尾
            if (currentNode == null || currentNode.next == null) {
                this.tail = currentNode;
            }
            return deletedNode;
        }
        /**
         * 查找与结点值相等的结点
         *
         * @param value 结点值
         */
        find(value) {
            if (!this.head)
                return null;
            let currentNode = this.head;
            while (currentNode) {
                if (this.compare.equal(currentNode.value, value))
                    return currentNode;
                currentNode = currentNode.next;
            }
            return null;
        }
        /**
         * 查找与结点值相等的结点
         *
         * @param callback 判断是否为查找的元素
         */
        findByFunc(callback) {
            if (!this.head)
                return null;
            let currentNode = this.head;
            while (currentNode) {
                if (callback(currentNode.value))
                    return currentNode;
                currentNode = currentNode.next;
            }
            return null;
        }
        /**
         * 删除表头
         *
         * 删除链表前面的元素(链表的头)并返回元素值。如果队列为空，则返回null。
         */
        deleteHead() {
            if (!this.head)
                return null;
            const deletedHead = this.head;
            if (this.head.next) {
                this.head = this.head.next;
            }
            else {
                this.head = null;
                this.tail = null;
            }
            return deletedHead.value;
        }
        /**
         * 删除表尾
         */
        deleteTail() {
            if (!this.tail)
                return null;
            const deletedTail = this.tail;
            if (this.head === this.tail) {
                this.head = null;
                this.tail = null;
                return deletedTail.value;
            }
            // 遍历链表删除表尾
            let currentNode = this.head;
            while (currentNode.next) {
                if (!currentNode.next.next) {
                    currentNode.next = null;
                }
                else {
                    currentNode = currentNode.next;
                }
            }
            this.tail = currentNode;
            return deletedTail.value;
        }
        /**
         * 从数组中初始化链表
         *
         * @param values 结点值列表
         */
        fromArray(values) {
            this.empty();
            values.forEach(value => this.append(value));
            return this;
        }
        /**
         * 转换为数组
         */
        toArray() {
            var values = [];
            var currentNode = this.head;
            while (currentNode) {
                values.push(currentNode.value);
                currentNode = currentNode.next;
            }
            return values;
        }
        /**
         * 转换为字符串
         *
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString) {
            return this.toArray().map(value => valueToString ? valueToString(value) : `${value}`).toString();
        }
        /**
         * 反转链表
         */
        reverse() {
            let currNode = this.head;
            let prevNode = null;
            let nextNode = null;
            while (currNode) {
                // 存储下一个结点
                nextNode = currNode.next;
                // 反转结点的next指向
                currNode.next = prevNode;
                // 存储上一个节点
                prevNode = currNode;
                // 遍历指针向后移动
                currNode = nextNode;
            }
            // 重置表头与表尾
            this.tail = this.head;
            this.head = prevNode;
            return this;
        }
        /**
         * 核查结构是否正确
         */
        checkStructure() {
            if (this.head) {
                var currNode = this.head;
                while (currNode.next) {
                    currNode = currNode.next;
                }
                return this.tail == currNode;
            }
            return !this.tail;
        }
    }
    ds.LinkedList = LinkedList;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 优先队列
     *
     * 与最小堆相同，只是与元素比较时不同
     * 我们考虑的不是元素的值，而是它的优先级。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/priority-queue/PriorityQueue.js
     */
    class PriorityQueue extends ds.MinHeap {
        constructor() {
            super();
            this.priorities = {};
            this.compare = new ds.Comparator(this.comparePriority.bind(this));
        }
        /**
         * 新增元素
         *
         * @param item 元素
         * @param priority 优先级
         */
        add(item, priority = 0) {
            this.priorities[item] = priority;
            super.add(item);
            return this;
        }
        /**
         * 移除元素
         *
         * @param item 元素
         * @param customFindingComparator 自定义查找比较器
         */
        remove(item, customFindingComparator = this.compare) {
            super.remove(item, customFindingComparator);
            delete this.priorities[item];
            return this;
        }
        /**
         * 改变元素优先级
         *
         * @param item 元素
         * @param priority 优先级
         */
        changePriority(item, priority) {
            this.remove(item, new ds.Comparator(this.compareValue));
            this.add(item, priority);
            return this;
        }
        /**
         * 查找元素所在索引
         *
         * @param item 元素
         */
        findByValue(item) {
            return this.find(item, new ds.Comparator(this.compareValue));
        }
        /**
         * 是否拥有元素
         *
         * @param item 元素
         */
        hasValue(item) {
            return this.findByValue(item).length > 0;
        }
        /**
         * 比较两个元素优先级
         *
         * @param a 元素a
         * @param b 元素b
         */
        comparePriority(a, b) {
            if (this.priorities[a] === this.priorities[b]) {
                return 0;
            }
            return this.priorities[a] < this.priorities[b] ? -1 : 1;
        }
        /**
         * 比较两个元素大小
         *
         * @param a 元素a
         * @param b 元素b
         */
        compareValue(a, b) {
            if (a === b) {
                return 0;
            }
            return a < b ? -1 : 1;
        }
    }
    ds.PriorityQueue = PriorityQueue;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 队列，只能从后面进，前面出
     * 使用单向链表实现
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/queue/Queue.js
     */
    class Queue {
        /**
         * 构建队列
         *
         * @param comparatorFunction 比较函数
         */
        constructor() {
            this.linkedList = new ds.LinkedList();
        }
        /**
         * 是否为空
         */
        isEmpty() {
            return this.linkedList.isEmpty();
        }
        /**
         * 清空
         */
        empty() {
            this.linkedList.empty();
        }
        /**
         * 读取队列前面的元素，但不删除它。
         */
        peek() {
            return this.linkedList.getHeadValue();
        }
        /**
         * 入队
         *
         * 在队列的末尾(链表的尾部)添加一个新元素。
         * 这个元素将在它前面的所有元素之后被处理。
         *
         * @param value 元素值
         */
        enqueue(value) {
            this.linkedList.append(value);
            return this;
        }
        /**
         * 出队
         *
         * 删除队列前面的元素(链表的头)。如果队列为空，则返回null。
         */
        dequeue() {
            const removedValue = this.linkedList.deleteHead();
            return removedValue;
        }
        /**
         * 转换为字符串
         *
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString) {
            return this.linkedList.toString(valueToString);
        }
    }
    ds.Queue = Queue;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 栈
     *
     * 后进先出
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/stack/Stack.js
     */
    class Stack {
        constructor() {
            this.linkedList = new ds.LinkedList();
        }
        /**
         * 是否为空
         */
        isEmpty() {
            return this.linkedList.isEmpty();
        }
        /**
         * 查看第一个元素值
         */
        peek() {
            return this.linkedList.getHeadValue();
        }
        /**
         * 入栈
         *
         * @param value 元素值
         */
        push(value) {
            this.linkedList.prepend(value);
            return this;
        }
        /**
         * 出栈
         */
        pop() {
            return this.linkedList.deleteHead();
        }
        /**
         * 转换为数组
         */
        toArray() {
            return this.linkedList.toArray();
        }
        /**
         * 转换为字符串
         *
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString) {
            return this.linkedList.toString(valueToString);
        }
    }
    ds.Stack = Stack;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 二叉树结点
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/BinaryTreeNode.js
     */
    class BinaryTreeNode {
        /**
         * 构建二叉树结点
         *
         * @param value 结点值
         */
        constructor(value = null) {
            this.left = null;
            this.right = null;
            this.parent = null;
            this.value = value;
            this.meta = new ds.HashTable();
            this.nodeComparator = new ds.Comparator();
        }
        /**
         * 左结点高度
         */
        get leftHeight() {
            if (!this.left) {
                return 0;
            }
            return this.left.height + 1;
        }
        /**
         * 右结点高度
         */
        get rightHeight() {
            if (!this.right) {
                return 0;
            }
            return this.right.height + 1;
        }
        /**
         * 高度
         */
        get height() {
            return Math.max(this.leftHeight, this.rightHeight);
        }
        /**
         * 平衡系数
         */
        get balanceFactor() {
            return this.leftHeight - this.rightHeight;
        }
        /**
         * 叔伯结点
         */
        get uncle() {
            if (!this.parent) {
                return undefined;
            }
            if (!this.parent.parent) {
                return undefined;
            }
            // 判断祖父结点是否有两个子结点
            if (!(this.parent.parent.left && this.parent.parent.right)) {
                return undefined;
            }
            // 现在我们知道当前节点有祖父结点，而这个祖父结点有两个子结点。让我们看看谁是叔叔。
            if (this.nodeComparator.equal(this.parent, this.parent.parent.left)) {
                // 右边的是一个叔叔。
                return this.parent.parent.right;
            }
            return this.parent.parent.left;
        }
        /**
         * 设置结点值
         *
         * @param value 值
         */
        setValue(value) {
            this.value = value;
            return this;
        }
        /**
         * 设置左结点
         *
         * @param node 结点
         */
        setLeft(node) {
            if (this.left) {
                this.left.parent = null;
            }
            this.left = node;
            if (this.left) {
                this.left.parent = this;
            }
            return this;
        }
        /**
         * 设置右结点
         *
         * @param node 结点
         */
        setRight(node) {
            if (this.right) {
                this.right.parent = null;
            }
            this.right = node;
            if (node) {
                this.right.parent = this;
            }
            return this;
        }
        /**
         * 移除子结点
         *
         * @param nodeToRemove 子结点
         */
        removeChild(nodeToRemove) {
            if (this.left && this.nodeComparator.equal(this.left, nodeToRemove)) {
                this.left = null;
                return true;
            }
            if (this.right && this.nodeComparator.equal(this.right, nodeToRemove)) {
                this.right = null;
                return true;
            }
            return false;
        }
        /**
         * 替换节点
         *
         * @param nodeToReplace 被替换的节点
         * @param replacementNode 替换后的节点
         */
        replaceChild(nodeToReplace, replacementNode) {
            if (!nodeToReplace || !replacementNode) {
                return false;
            }
            if (this.left && this.nodeComparator.equal(this.left, nodeToReplace)) {
                this.left = replacementNode;
                return true;
            }
            if (this.right && this.nodeComparator.equal(this.right, nodeToReplace)) {
                this.right = replacementNode;
                return true;
            }
            return false;
        }
        /**
         * 拷贝节点
         *
         * @param sourceNode 源节点
         * @param targetNode 目标节点
         */
        static copyNode(sourceNode, targetNode) {
            targetNode.setValue(sourceNode.value);
            targetNode.setLeft(sourceNode.left);
            targetNode.setRight(sourceNode.right);
        }
        /**
         * 左序深度遍历
         */
        traverseInOrder() {
            let traverse = [];
            if (this.left) {
                traverse = traverse.concat(this.left.traverseInOrder());
            }
            traverse.push(this.value);
            if (this.right) {
                traverse = traverse.concat(this.right.traverseInOrder());
            }
            return traverse;
        }
        /**
         * 转换为字符串
         */
        toString() {
            return this.traverseInOrder().toString();
        }
    }
    ds.BinaryTreeNode = BinaryTreeNode;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 二叉查找树
     *
     * 二叉查找树（英语：Binary Search Tree），也称为二叉搜索树、有序二叉树（ordered binary tree）或排序二叉树（sorted binary tree），是指一棵空树或者具有下列性质的二叉树：
     *
     * 1. 若任意节点的左子树不空，则左子树上所有节点的值均小于它的根节点的值；
     * 1. 若任意节点的右子树不空，则右子树上所有节点的值均大于它的根节点的值；
     * 1. 任意节点的左、右子树也分别为二叉查找树；
     * 1. 没有键值相等的节点。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/binary-search-tree/BinarySearchTree.js
     * @see https://en.wikipedia.org/wiki/Binary_search_tree
     * @see https://www.youtube.com/watch?v=wcIRPqTR3Kc&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8&index=9&t=0s
     */
    class BinarySearchTree {
        /**
         * 构建 二叉查找树
         *
         * @param nodeValueCompareFunction 结点值比较器
         */
        constructor(nodeValueCompareFunction) {
            this.root = new ds.BinarySearchTreeNode(null, nodeValueCompareFunction);
            // 从根节点中窃取节点比较器。
            this.nodeComparator = this.root.nodeComparator;
        }
        /**
         * 插入值
         *
         * @param value 值
         */
        insert(value) {
            return this.root.insert(value);
        }
        /**
         * 是否包含指定值
         *
         * @param value 值
         */
        contains(value) {
            return this.root.contains(value);
        }
        /**
         * 移除指定值
         *
         * @param value 值
         */
        remove(value) {
            return this.root.remove(value);
        }
        /**
         * 转换为字符串
         */
        toString() {
            return this.root.toString();
        }
    }
    ds.BinarySearchTree = BinarySearchTree;
})(ds || (ds = {}));
/// <reference path="../binary-search-tree/BinarySearchTree.ts" />
var ds;
(function (ds) {
    /**
     * 平衡二叉树
     *
     * AVL树（以发明者Adelson-Velsky和Landis 命名）是自平衡二叉搜索树。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/tree/master/src/data-structures/tree/avl-tree
     * @see https://en.wikipedia.org/wiki/AVL_tree
     * @see https://www.tutorialspoint.com/data_structures_algorithms/avl_tree_algorithm.htm
     * @see http://btechsmartclass.com/data_structures/avl-trees.html
     */
    class AvlTree extends ds.BinarySearchTree {
        /**
         * 插入元素
         *
         * @param value 元素
         */
        insert(value) {
            var newNode = super.insert(value);
            // 从到根结点检查平衡因子
            let currentNode = this.root.find(value);
            while (currentNode) {
                this.balance(currentNode);
                currentNode = currentNode.parent;
            }
            return newNode;
        }
        /**
         * 移除元素
         *
         * @param value 元素
         */
        remove(value) {
            var result = super.remove(value);
            // 从根节点开始平衡树。
            this.balance(this.root);
            return result;
        }
        /**
         * 从指定结点平衡树
         *
         * @param node 结点
         */
        balance(node) {
            // If balance factor is not OK then try to balance the node.
            if (node.balanceFactor > 1) {
                // Left rotation.
                if (node.left.balanceFactor > 0) {
                    // Left-Left rotation
                    this.rotateLeftLeft(node);
                }
                else if (node.left.balanceFactor < 0) {
                    // Left-Right rotation.
                    this.rotateLeftRight(node);
                }
            }
            else if (node.balanceFactor < -1) {
                // Right rotation.
                if (node.right.balanceFactor < 0) {
                    // Right-Right rotation
                    this.rotateRightRight(node);
                }
                else if (node.right.balanceFactor > 0) {
                    // Right-Left rotation.
                    this.rotateRightLeft(node);
                }
            }
        }
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateLeftLeft(rootNode) {
            // Detach left node from root node.
            const leftNode = rootNode.left;
            rootNode.setLeft(null);
            // Make left node to be a child of rootNode's parent.
            if (rootNode.parent) {
                rootNode.parent.setLeft(leftNode);
            }
            else if (rootNode === this.root) {
                // If root node is root then make left node to be a new root.
                this.root = leftNode;
            }
            // If left node has a right child then detach it and
            // attach it as a left child for rootNode.
            if (leftNode.right) {
                rootNode.setLeft(leftNode.right);
            }
            // Attach rootNode to the right of leftNode.
            leftNode.setRight(rootNode);
        }
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateLeftRight(rootNode) {
            // Detach left node from rootNode since it is going to be replaced.
            const leftNode = rootNode.left;
            rootNode.setLeft(null);
            // Detach right node from leftNode.
            const leftRightNode = leftNode.right;
            leftNode.setRight(null);
            // Preserve leftRightNode's left subtree.
            if (leftRightNode.left) {
                leftNode.setRight(leftRightNode.left);
                leftRightNode.setLeft(null);
            }
            // Attach leftRightNode to the rootNode.
            rootNode.setLeft(leftRightNode);
            // Attach leftNode as left node for leftRight node.
            leftRightNode.setLeft(leftNode);
            // Do left-left rotation.
            this.rotateLeftLeft(rootNode);
        }
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateRightLeft(rootNode) {
            // Detach right node from rootNode since it is going to be replaced.
            const rightNode = rootNode.right;
            rootNode.setRight(null);
            // Detach left node from rightNode.
            const rightLeftNode = rightNode.left;
            rightNode.setLeft(null);
            if (rightLeftNode.right) {
                rightNode.setLeft(rightLeftNode.right);
                rightLeftNode.setRight(null);
            }
            // Attach rightLeftNode to the rootNode.
            rootNode.setRight(rightLeftNode);
            // Attach rightNode as right node for rightLeft node.
            rightLeftNode.setRight(rightNode);
            // Do right-right rotation.
            this.rotateRightRight(rootNode);
        }
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateRightRight(rootNode) {
            // Detach right node from root node.
            const rightNode = rootNode.right;
            rootNode.setRight(null);
            // Make right node to be a child of rootNode's parent.
            if (rootNode.parent) {
                rootNode.parent.setRight(rightNode);
            }
            else if (rootNode === this.root) {
                // If root node is root then make right node to be a new root.
                this.root = rightNode;
            }
            // If right node has a left child then detach it and
            // attach it as a right child for rootNode.
            if (rightNode.left) {
                rootNode.setRight(rightNode.left);
            }
            // Attach rootNode to the left of rightNode.
            rightNode.setLeft(rootNode);
        }
    }
    ds.AvlTree = AvlTree;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 二叉查找树结点
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/binary-search-tree/BinarySearchTreeNode.js
     */
    class BinarySearchTreeNode extends ds.BinaryTreeNode {
        /**
         * 构建二叉查找树结点
         *
         * @param value 结点值
         * @param compareFunction 比较函数
         */
        constructor(value, compareFunction) {
            super(value);
            this.compareFunction = compareFunction;
            this.nodeValueComparator = new ds.Comparator(compareFunction);
        }
        /**
         * 插入值
         *
         * @param value 值
         */
        insert(value) {
            if (this.nodeValueComparator.equal(this.value, null)) {
                this.value = value;
                return this;
            }
            if (this.nodeValueComparator.lessThan(value, this.value)) {
                // 插入到左结点
                if (this.left) {
                    return this.left.insert(value);
                }
                const newNode = new BinarySearchTreeNode(value, this.compareFunction);
                this.setLeft(newNode);
                return newNode;
            }
            if (this.nodeValueComparator.greaterThan(value, this.value)) {
                // 插入到右结点
                if (this.right) {
                    return this.right.insert(value);
                }
                const newNode = new BinarySearchTreeNode(value, this.compareFunction);
                this.setRight(newNode);
                return newNode;
            }
            return this;
        }
        /**
         * 查找结点
         *
         * @param value 值
         */
        find(value) {
            // 核查本结点是否为所查找结点
            if (this.nodeValueComparator.equal(this.value, value)) {
                return this;
            }
            if (this.nodeValueComparator.lessThan(value, this.value) && this.left) {
                // 从左结点中查找
                return this.left.find(value);
            }
            if (this.nodeValueComparator.greaterThan(value, this.value) && this.right) {
                // 从右结点中查找
                return this.right.find(value);
            }
            return null;
        }
        /**
         * 是否包含指定值
         *
         * @param value 结点值
         */
        contains(value) {
            return !!this.find(value);
        }
        /**
         * 移除指定值
         *
         * @param value 结点值
         */
        remove(value) {
            const nodeToRemove = this.find(value);
            if (!nodeToRemove) {
                throw new Error('无法查找到值对于的结点。');
            }
            const parent = nodeToRemove.parent;
            if (!nodeToRemove.left && !nodeToRemove.right) {
                // 删除叶子结点
                if (parent) {
                    parent.removeChild(nodeToRemove);
                }
                else {
                    // 节点没有父节点。只需清除当前节点值。
                    nodeToRemove.setValue(undefined);
                }
            }
            else if (nodeToRemove.left && nodeToRemove.right) {
                // 删除拥有两个子结点的结点
                // 查找下一个最大的值(右分支中的最小值)，并用下一个最大的值替换当前值节点。
                const nextBiggerNode = nodeToRemove.right.findMin();
                if (!this.nodeComparator.equal(nextBiggerNode, nodeToRemove.right)) {
                    this.remove(nextBiggerNode.value);
                    nodeToRemove.setValue(nextBiggerNode.value);
                }
                else {
                    //如果下一个右值是下一个更大的值，它没有左子节点，那么就用右节点替换要删除的节点。
                    nodeToRemove.setValue(nodeToRemove.right.value);
                    nodeToRemove.setRight(nodeToRemove.right.right);
                }
            }
            else {
                // 删除拥有一个子结点的结点
                // 使此子节点成为当前节点的父节点的一个子节点。
                const childNode = nodeToRemove.left || nodeToRemove.right;
                if (parent) {
                    parent.replaceChild(nodeToRemove, childNode);
                }
                else {
                    ds.BinaryTreeNode.copyNode(childNode, nodeToRemove);
                }
            }
            nodeToRemove.parent = null;
            return true;
        }
        /**
         * 查找最小值
         */
        findMin() {
            if (!this.left) {
                return this;
            }
            return this.left.findMin();
        }
    }
    ds.BinarySearchTreeNode = BinarySearchTreeNode;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 树状数组
     *
     * 树状数组或二叉索引树（英语：Binary Indexed Tree），又以其发明者命名为Fenwick树，最早由Peter M. Fenwick于1994年以A New Data Structure for Cumulative Frequency Tables[1]为题发表在SOFTWARE PRACTICE AND EXPERIENCE。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/tree/master/src/data-structures/tree/fenwick-tree
     * @see https://en.wikipedia.org/wiki/Fenwick_tree
     * @see https://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/
     * @see https://www.youtube.com/watch?v=CWDQJGaN1gY&index=18&t=0s&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     */
    class FenwickTree {
        /**
         * 构建树状数组
         *
         * @param arraySize 数组尺寸
         */
        constructor(arraySize) {
            this.arraySize = arraySize;
            // 用0填充树数组
            this.treeArray = [];
            for (let i = 0, n = arraySize + 1; i < n; i++) {
                this.treeArray[i] = 0;
            }
        }
        /**
         * 增加值到指定位置上
         *
         * @param position 位置
         * @param value 值
         */
        increase(position, value) {
            if (position < 1 || position > this.arraySize) {
                throw new Error('位置超出允许范围');
            }
            for (let i = position; i <= this.arraySize; i += (i & -i)) {
                this.treeArray[i] += value;
            }
            return this;
        }
        /**
         * 从索引1到位置的查询和。
         *
         * @param position 位置
         */
        query(position) {
            if (position < 1 || position > this.arraySize) {
                throw new Error('位置超出允许范围');
            }
            let sum = 0;
            for (let i = position; i > 0; i -= (i & -i)) {
                sum += this.treeArray[i];
            }
            return sum;
        }
        /**
         * 从leftIndex到rightIndex的查询和。
         *
         * @param leftIndex  起始索引
         * @param rightIndex 结束索引
         */
        queryRange(leftIndex, rightIndex) {
            if (leftIndex > rightIndex) {
                throw new Error('leftIndex 不能大于 rightIndex');
            }
            if (leftIndex === 1) {
                return this.query(rightIndex);
            }
            return this.query(rightIndex) - this.query(leftIndex - 1);
        }
    }
    ds.FenwickTree = FenwickTree;
})(ds || (ds = {}));
var ds;
(function (ds) {
    // 红黑树节点可能的颜色。
    const RED_BLACK_TREE_COLORS = {
        red: 'red',
        black: 'black',
    };
    // 节点元信息中的颜色属性名。
    const COLOR_PROP_NAME = 'color';
    /**
     * 红黑树
     *
     * 红黑树（英语：Red–black tree）是一种自平衡二叉查找树，是在计算机科学中用到的一种数据结构，典型的用途是实现关联数组
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/red-black-tree/RedBlackTree.js
     * @see https://en.wikipedia.org/wiki/Red%E2%80%93black_tree
     */
    class RedBlackTree extends ds.BinarySearchTree {
        /**
         * 插入值
         *
         * @param value 值
         */
        insert(value) {
            const insertedNode = super.insert(value);
            // 判断是否为根结点
            if (this.nodeComparator.equal(insertedNode, this.root)) {
                // 使根永远是黑色的。
                this.makeNodeBlack(insertedNode);
            }
            else {
                // 将所有新插入的节点设置为红色。
                this.makeNodeRed(insertedNode);
            }
            // 检查所有条件并平衡节点。
            this.balance(insertedNode);
            return insertedNode;
        }
        /**
         * 移除指定值 （禁止使用）
         *
         * @param value 值
         */
        remove(value) {
            throw new Error(`不能删除 ${value}。移除方法尚未实现`);
            return true;
        }
        /**
         * 从指定结点平衡树
         *
         * @param node 结点
         */
        balance(node) {
            // 如果它是根节点，那么这里就没有什么需要平衡的了。
            if (this.nodeComparator.equal(node, this.root)) {
                return;
            }
            // 如果父结点是黑色的，则完成。这里没有什么需要平衡的。
            if (this.isNodeBlack(node.parent)) {
                return;
            }
            const grandParent = node.parent.parent;
            if (node.uncle && this.isNodeRed(node.uncle)) {
                // 如果node有red 叔伯，那么我们需要重新上色。
                // 重新上色父结点和叔伯结点到黑色。
                this.makeNodeBlack(node.uncle);
                this.makeNodeBlack(node.parent);
                if (!this.nodeComparator.equal(grandParent, this.root)) {
                    // 如果不是根结点，则将祖父结点重新上色为红色。
                    this.makeNodeRed(grandParent);
                }
                else {
                    //如果祖父结点是黑根结点，什么都不要做。
                    //因为root已经有了两个我们刚刚重新上色的黑色同胞。
                    return;
                }
                // 现在进一步检查重新着色的祖父母。
                this.balance(grandParent);
            }
            else if (!node.uncle || this.isNodeBlack(node.uncle)) {
                // 如果叔伯结点是黑色或者不存在，那么我们需要执行旋转。
                if (grandParent) {
                    // 旋转后的得到的新祖父结点
                    let newGrandParent;
                    if (this.nodeComparator.equal(grandParent.left, node.parent)) {
                        // Left case.
                        if (this.nodeComparator.equal(node.parent.left, node)) {
                            // Left-left case.
                            newGrandParent = this.leftLeftRotation(grandParent);
                        }
                        else {
                            // Left-right case.
                            newGrandParent = this.leftRightRotation(grandParent);
                        }
                    }
                    else {
                        // Right case.
                        if (this.nodeComparator.equal(node.parent.right, node)) {
                            // Right-right case.
                            newGrandParent = this.rightRightRotation(grandParent);
                        }
                        else {
                            // Right-left case.
                            newGrandParent = this.rightLeftRotation(grandParent);
                        }
                    }
                    // 如果没有父结点，则将newGrandParent设为根结点。
                    if (newGrandParent && newGrandParent.parent === null) {
                        this.root = newGrandParent;
                        // 把根结点重新上色为黑色
                        this.makeNodeBlack(this.root);
                    }
                    // 检查新祖父母是否没有违反红黑树规则。
                    this.balance(newGrandParent);
                }
            }
        }
        /**
         * Left Left Case (p is left child of g and x is left child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        leftLeftRotation(grandParentNode) {
            // Memorize the parent of grand-parent node.
            const grandGrandParent = grandParentNode.parent;
            // Check what type of sibling is our grandParentNode is (left or right).
            let grandParentNodeIsLeft;
            if (grandGrandParent) {
                grandParentNodeIsLeft = this.nodeComparator.equal(grandGrandParent.left, grandParentNode);
            }
            // Memorize grandParentNode's left node.
            const parentNode = grandParentNode.left;
            // Memorize parent's right node since we're going to transfer it to
            // grand parent's left subtree.
            const parentRightNode = parentNode.right;
            // Make grandParentNode to be right child of parentNode.
            parentNode.setRight(grandParentNode);
            // Move child's right subtree to grandParentNode's left subtree.
            grandParentNode.setLeft(parentRightNode);
            // Put parentNode node in place of grandParentNode.
            if (grandGrandParent) {
                if (grandParentNodeIsLeft) {
                    grandGrandParent.setLeft(parentNode);
                }
                else {
                    grandGrandParent.setRight(parentNode);
                }
            }
            else {
                // Make parent node a root
                parentNode.parent = null;
            }
            // Swap colors of granParent and parent nodes.
            this.swapNodeColors(parentNode, grandParentNode);
            // Return new root node.
            return parentNode;
        }
        /**
         * Left Right Case (p is left child of g and x is right child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        leftRightRotation(grandParentNode) {
            // Memorize left and left-right nodes.
            const parentNode = grandParentNode.left;
            const childNode = parentNode.right;
            // We need to memorize child left node to prevent losing
            // left child subtree. Later it will be re-assigned to
            // parent's right sub-tree.
            const childLeftNode = childNode.left;
            // Make parentNode to be a left child of childNode node.
            childNode.setLeft(parentNode);
            // Move child's left subtree to parent's right subtree.
            parentNode.setRight(childLeftNode);
            // Put left-right node in place of left node.
            grandParentNode.setLeft(childNode);
            // Now we're ready to do left-left rotation.
            return this.leftLeftRotation(grandParentNode);
        }
        /**
         * Right Right Case (p is right child of g and x is right child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        rightRightRotation(grandParentNode) {
            // Memorize the parent of grand-parent node.
            const grandGrandParent = grandParentNode.parent;
            // Check what type of sibling is our grandParentNode is (left or right).
            let grandParentNodeIsLeft;
            if (grandGrandParent) {
                grandParentNodeIsLeft = this.nodeComparator.equal(grandGrandParent.left, grandParentNode);
            }
            // Memorize grandParentNode's right node.
            const parentNode = grandParentNode.right;
            // Memorize parent's left node since we're going to transfer it to
            // grand parent's right subtree.
            const parentLeftNode = parentNode.left;
            // Make grandParentNode to be left child of parentNode.
            parentNode.setLeft(grandParentNode);
            // Transfer all left nodes from parent to right sub-tree of grandparent.
            grandParentNode.setRight(parentLeftNode);
            // Put parentNode node in place of grandParentNode.
            if (grandGrandParent) {
                if (grandParentNodeIsLeft) {
                    grandGrandParent.setLeft(parentNode);
                }
                else {
                    grandGrandParent.setRight(parentNode);
                }
            }
            else {
                // Make parent node a root.
                parentNode.parent = null;
            }
            // Swap colors of granParent and parent nodes.
            this.swapNodeColors(parentNode, grandParentNode);
            // Return new root node.
            return parentNode;
        }
        /**
         * Right Left Case (p is right child of g and x is left child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        rightLeftRotation(grandParentNode) {
            // Memorize right and right-left nodes.
            const parentNode = grandParentNode.right;
            const childNode = parentNode.left;
            // We need to memorize child right node to prevent losing
            // right child subtree. Later it will be re-assigned to
            // parent's left sub-tree.
            const childRightNode = childNode.right;
            // Make parentNode to be a right child of childNode.
            childNode.setRight(parentNode);
            // Move child's right subtree to parent's left subtree.
            parentNode.setLeft(childRightNode);
            // Put childNode node in place of parentNode.
            grandParentNode.setRight(childNode);
            // Now we're ready to do right-right rotation.
            return this.rightRightRotation(grandParentNode);
        }
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {BinarySearchTreeNode}
         */
        makeNodeRed(node) {
            node.meta.set(COLOR_PROP_NAME, RED_BLACK_TREE_COLORS.red);
            return node;
        }
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {BinarySearchTreeNode}
         */
        makeNodeBlack(node) {
            node.meta.set(COLOR_PROP_NAME, RED_BLACK_TREE_COLORS.black);
            return node;
        }
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {boolean}
         */
        isNodeRed(node) {
            return node.meta.get(COLOR_PROP_NAME) === RED_BLACK_TREE_COLORS.red;
        }
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {boolean}
         */
        isNodeBlack(node) {
            return node.meta.get(COLOR_PROP_NAME) === RED_BLACK_TREE_COLORS.black;
        }
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {boolean}
         */
        isNodeColored(node) {
            return this.isNodeRed(node) || this.isNodeBlack(node);
        }
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} firstNode
         * @param {BinarySearchTreeNode|BinaryTreeNode} secondNode
         */
        swapNodeColors(firstNode, secondNode) {
            const firstColor = firstNode.meta.get(COLOR_PROP_NAME);
            const secondColor = secondNode.meta.get(COLOR_PROP_NAME);
            firstNode.meta.set(COLOR_PROP_NAME, secondColor);
            secondNode.meta.set(COLOR_PROP_NAME, firstColor);
        }
    }
    ds.RedBlackTree = RedBlackTree;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 线段树
     *
     * 用于高效运算指定区间 [a, b] 的 operation 运算
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/segment-tree/SegmentTree.js
     * @see [Wikipedia](https://en.wikipedia.org/wiki/Segment_tree)
     * @see [YouTube](https://www.youtube.com/watch?v=ZBHKZF5w4YU&index=65&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8)
     * @see [GeeksForGeeks](https://www.geeksforgeeks.org/segment-tree-set-1-sum-of-given-range/)
     */
    class SegmentTree {
        /**
         * 构建线段树
         *
         * @param inputArray 输入数组
         * @param operation 操作函数 parent = operation(leftChild,rightChild)
         * @param operationFallback 运算无效值，对任意的 x 都满足 x == operation( x, operationFallback)
         */
        constructor(inputArray, operation, operationFallback) {
            this.inputArray = inputArray;
            this.operation = operation;
            this.operationFallback = operationFallback;
            this.segmentTree = this.initSegmentTree(this.inputArray);
            this.buildSegmentTree();
        }
        /**
         * 初始化线段树
         *
         * @param inputArray 输入数组
         */
        initSegmentTree(inputArray) {
            let segmentTreeArrayLength;
            const inputArrayLength = inputArray.length;
            if (ds.isPowerOfTwo(inputArrayLength)) {
                // 如果原始数组长度是2的幂。
                segmentTreeArrayLength = (2 * inputArrayLength) - 1;
            }
            else {
                //如果原始数组的长度不是2的幂，那么我们需要找到下一个2的幂并使用它来计算树数组的大小。这是因为我们需要用null填充完美二叉树中的空子树。这些零需要额外的空间。
                const currentPower = Math.floor(Math.log2(inputArrayLength));
                const nextPower = currentPower + 1;
                const nextPowerOfTwoNumber = Math.pow(2, nextPower);
                segmentTreeArrayLength = (2 * nextPowerOfTwoNumber) - 1;
            }
            return new Array(segmentTreeArrayLength).fill(null);
        }
        /**
         * 构建线段树
         */
        buildSegmentTree() {
            const leftIndex = 0;
            const rightIndex = this.inputArray.length - 1;
            const position = 0;
            this.buildTreeRecursively(leftIndex, rightIndex, position);
        }
        /**
         * 构建线段树递归
         *
         * @param leftInputIndex 左输入索引
         * @param rightInputIndex 右输入索引
         * @param position 位置
         */
        buildTreeRecursively(leftInputIndex, rightInputIndex, position) {
            // 如果左输入索引和右输入索引相等那就意味着我们已经完成了分裂，我们已经到了片段树的叶结点。我们需要将这个叶值从inputarray复制到段树。
            if (leftInputIndex === rightInputIndex) {
                this.segmentTree[position] = this.inputArray[leftInputIndex];
                return;
            }
            // 将输入数组分成两半，并递归地处理它们。
            const middleIndex = Math.floor((leftInputIndex + rightInputIndex) / 2);
            // 处理输入数组的左半边。
            this.buildTreeRecursively(leftInputIndex, middleIndex, this.getLeftChildIndex(position));
            // 处理输入数组的右半边。
            this.buildTreeRecursively(middleIndex + 1, rightInputIndex, this.getRightChildIndex(position));
            //一旦每个树叶不是空的，我们就可以使用提供的操作函数自底向上构建树。
            this.segmentTree[position] = this.operation(this.segmentTree[this.getLeftChildIndex(position)], this.segmentTree[this.getRightChildIndex(position)]);
        }
        /**
         * 范围查询
         *
         * @param queryLeftIndex 查询左索引
         * @param queryRightIndex 查询右索引
         */
        rangeQuery(queryLeftIndex, queryRightIndex) {
            const leftIndex = 0;
            const rightIndex = this.inputArray.length - 1;
            const position = 0;
            return this.rangeQueryRecursive(queryLeftIndex, queryRightIndex, leftIndex, rightIndex, position);
        }
        /**
         * 范围查询递归求值
         *
         * @param queryLeftIndex 查询左索引
         * @param queryRightIndex 查询右索引
         * @param leftIndex 输入数组段的左索引
         * @param rightIndex 输入数组段的右索引
         * @param position 二叉树的根位置
         */
        rangeQueryRecursive(queryLeftIndex, queryRightIndex, leftIndex, rightIndex, position) {
            if (queryLeftIndex <= leftIndex && queryRightIndex >= rightIndex) {
                // 全部重叠
                return this.segmentTree[position];
            }
            if (queryLeftIndex > rightIndex || queryRightIndex < leftIndex) {
                // 没有重叠，返回一个 运算无效值
                return this.operationFallback;
            }
            // 部分重叠
            const middleIndex = Math.floor((leftIndex + rightIndex) / 2);
            const leftOperationResult = this.rangeQueryRecursive(queryLeftIndex, queryRightIndex, leftIndex, middleIndex, this.getLeftChildIndex(position));
            const rightOperationResult = this.rangeQueryRecursive(queryLeftIndex, queryRightIndex, middleIndex + 1, rightIndex, this.getRightChildIndex(position));
            return this.operation(leftOperationResult, rightOperationResult);
        }
        /**
         * 获取左子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getLeftChildIndex(parentIndex) {
            return (2 * parentIndex) + 1;
        }
        /**
         * 获取右子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getRightChildIndex(parentIndex) {
            return (2 * parentIndex) + 2;
        }
    }
    ds.SegmentTree = SegmentTree;
})(ds || (ds = {}));
var ds;
(function (ds) {
    // Character that we will use for trie tree root.
    const HEAD_CHARACTER = '*';
    /**
     * 字典树  (在JavaScript中可由Object对象代替)
     *
     * 在计算机科学中，trie，又称前缀树或字典树，是一种有序树，用于保存关联数组，其中的键通常是字符串。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/trie/Trie.js
     * @see [Wikipedia](https://en.wikipedia.org/wiki/Trie)
     * @see [YouTube](https://www.youtube.com/watch?v=zIjfhVPRZCg&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8&index=7&t=0s)
     */
    class Trie {
        constructor() {
            this.head = new ds.TrieNode(HEAD_CHARACTER);
        }
        /**
         * @param {string} word
         * @return {Trie}
         */
        addWord(word) {
            const characters = Array.from(word);
            let currentNode = this.head;
            for (let charIndex = 0; charIndex < characters.length; charIndex += 1) {
                const isComplete = charIndex === characters.length - 1;
                currentNode = currentNode.addChild(characters[charIndex], isComplete);
            }
            return this;
        }
        /**
         * @param {string} word
         * @return {Trie}
         */
        deleteWord(word) {
            const depthFirstDelete = (currentNode, charIndex = 0) => {
                if (charIndex >= word.length) {
                    // Return if we're trying to delete the character that is out of word's scope.
                    return;
                }
                const character = word[charIndex];
                const nextNode = currentNode.getChild(character);
                if (nextNode == null) {
                    // Return if we're trying to delete a word that has not been added to the Trie.
                    return;
                }
                // Go deeper.
                depthFirstDelete(nextNode, charIndex + 1);
                // Since we're going to delete a word let's un-mark its last character isCompleteWord flag.
                if (charIndex === (word.length - 1)) {
                    nextNode.isCompleteWord = false;
                }
                // childNode is deleted only if:
                // - childNode has NO children
                // - childNode.isCompleteWord === false
                currentNode.removeChild(character);
            };
            // Start depth-first deletion from the head node.
            depthFirstDelete(this.head);
            return this;
        }
        /**
         * @param {string} word
         * @return {string[]}
         */
        suggestNextCharacters(word) {
            const lastCharacter = this.getLastCharacterNode(word);
            if (!lastCharacter) {
                return null;
            }
            return lastCharacter.suggestChildren();
        }
        /**
         * Check if complete word exists in Trie.
         *
         * @param {string} word
         * @return {boolean}
         */
        doesWordExist(word) {
            const lastCharacter = this.getLastCharacterNode(word);
            return !!lastCharacter && lastCharacter.isCompleteWord;
        }
        /**
         * @param {string} word
         * @return {TrieNode}
         */
        getLastCharacterNode(word) {
            const characters = Array.from(word);
            let currentNode = this.head;
            for (let charIndex = 0; charIndex < characters.length; charIndex += 1) {
                if (!currentNode.hasChild(characters[charIndex])) {
                    return null;
                }
                currentNode = currentNode.getChild(characters[charIndex]);
            }
            return currentNode;
        }
    }
    ds.Trie = Trie;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 单词查找树结点
     */
    class TrieNode {
        /**
         * @param {string} character
         * @param {boolean} isCompleteWord
         */
        constructor(character, isCompleteWord = false) {
            this.character = character;
            this.isCompleteWord = isCompleteWord;
            this.children = new ds.HashTable();
        }
        /**
         * @param {string} character
         * @return {TrieNode}
         */
        getChild(character) {
            return this.children.get(character);
        }
        /**
         * @param {string} character
         * @param {boolean} isCompleteWord
         * @return {TrieNode}
         */
        addChild(character, isCompleteWord = false) {
            if (!this.children.has(character)) {
                this.children.set(character, new TrieNode(character, isCompleteWord));
            }
            const childNode = this.children.get(character);
            // In cases similar to adding "car" after "carpet" we need to mark "r" character as complete.
            childNode.isCompleteWord = childNode.isCompleteWord || isCompleteWord;
            return childNode;
        }
        /**
         * @param {string} character
         * @return {TrieNode}
         */
        removeChild(character) {
            const childNode = this.getChild(character);
            // Delete childNode only if:
            // - childNode has NO children,
            // - childNode.isCompleteWord === false.
            if (childNode
                && !childNode.isCompleteWord
                && !childNode.hasChildren()) {
                this.children.delete(character);
            }
            return this;
        }
        /**
         * @param {string} character
         * @return {boolean}
         */
        hasChild(character) {
            return this.children.has(character);
        }
        /**
         * Check whether current TrieNode has children or not.
         * @return {boolean}
         */
        hasChildren() {
            return this.children.getKeys().length !== 0;
        }
        /**
         * @return {string[]}
         */
        suggestChildren() {
            return [...this.children.getKeys()];
        }
        /**
         * @return {string}
         */
        toString() {
            let childrenAsString = this.suggestChildren().toString();
            childrenAsString = childrenAsString ? `:${childrenAsString}` : '';
            const isCompleteString = this.isCompleteWord ? '*' : '';
            return `${this.character}${isCompleteString}${childrenAsString}`;
        }
    }
    ds.TrieNode = TrieNode;
})(ds || (ds = {}));
var ds;
(function (ds) {
    /**
     * 比较器
     */
    class Comparator {
        /**
         * 构建比较器
         * @param compareFunction 比较函数
         */
        constructor(compareFunction) {
            this.compare = compareFunction || Comparator.defaultCompareFunction;
        }
        /**
         * 默认比较函数。只能处理 a和b 同为string或number的比较。
         *
         * @param a 比较值a
         * @param b 比较值b
         */
        static defaultCompareFunction(a, b) {
            if (a === b)
                return 0;
            return a < b ? -1 : 1;
        }
        /**
         * 检查 a 是否等于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        equal(a, b) {
            return this.compare(a, b) === 0;
        }
        /**
         * 检查 a 是否小于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        lessThan(a, b) {
            return this.compare(a, b) < 0;
        }
        /**
         * 检查 a 是否大于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        greaterThan(a, b) {
            return this.compare(a, b) > 0;
        }
        /**
         * 检查 a 是否小于等于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        lessThanOrEqual(a, b) {
            return this.lessThan(a, b) || this.equal(a, b);
        }
        /**
         * 检查 a 是否大于等于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        greaterThanOrEqual(a, b) {
            return this.greaterThan(a, b) || this.equal(a, b);
        }
        /**
         * 反转比较函数。
         */
        reverse() {
            const compareOriginal = this.compare;
            this.compare = (a, b) => compareOriginal(b, a);
        }
    }
    ds.Comparator = Comparator;
})(ds || (ds = {}));
//# sourceMappingURL=ds.js.map
declare namespace ds {
    class PolynomialHash {
        base: number;
        modulus: number;
        /**
         * @param {number} [base] - Base number that is used to create the polynomial.
         * @param {number} [modulus] - Modulus number that keeps the hash from overflowing.
         */
        constructor({ base, modulus }?: {
            base?: number;
            modulus?: number;
        });
        /**
         * Function that creates hash representation of the word.
         *
         * Time complexity: O(word.length).
         *
         * @param {string} word - String that needs to be hashed.
         * @return {number}
         */
        hash(word: any): number;
        /**
         * Function that creates hash representation of the word
         * based on previous word (shifted by one character left) hash value.
         *
         * Recalculates the hash representation of a word so that it isn't
         * necessary to traverse the whole word again.
         *
         * Time complexity: O(1).
         *
         * @param {number} prevHash
         * @param {string} prevWord
         * @param {string} newWord
         * @return {number}
         */
        roll(prevHash: any, prevWord: any, newWord: any): any;
        /**
         * Converts char to number.
         *
         * @param {string} char
         * @return {number}
         */
        charToNumber(char: any): any;
    }
}
declare namespace ds {
    class SimplePolynomialHash {
        base: number;
        /**
         * @param {number} [base] - Base number that is used to create the polynomial.
         */
        constructor(base?: number);
        /**
         * Function that creates hash representation of the word.
         *
         * Time complexity: O(word.length).
         *
         * @assumption: This version of the function  doesn't use modulo operator.
         * Thus it may produce number overflows by generating numbers that are
         * bigger than Number.MAX_SAFE_INTEGER. This function is mentioned here
         * for simplicity and LEARNING reasons.
         *
         * @param {string} word - String that needs to be hashed.
         * @return {number}
         */
        hash(word: any): number;
        /**
         * Function that creates hash representation of the word
         * based on previous word (shifted by one character left) hash value.
         *
         * Recalculates the hash representation of a word so that it isn't
         * necessary to traverse the whole word again.
         *
         * Time complexity: O(1).
         *
         * @assumption: This function doesn't use modulo operator and thus is not safe since
         * it may deal with numbers that are bigger than Number.MAX_SAFE_INTEGER. This
         * function is mentioned here for simplicity and LEARNING reasons.
         *
         * @param {number} prevHash
         * @param {string} prevWord
         * @param {string} newWord
         * @return {number}
         */
        roll(prevHash: any, prevWord: any, newWord: any): any;
    }
}
declare namespace ds {
    /**
     * @param {Graph} graph
     * @param {GraphVertex} startVertex
     * @param {Callbacks} [callbacks]
     */
    function depthFirstSearch<T>(graph: Graph<T>, startVertex: GraphVertex<T>, callbacks?: {
        enterVertex?: ({ currentVertex, previousVertex }: {
            currentVertex: any;
            previousVertex: any;
        }) => void;
        leaveVertex?: ({ currentVertex, previousVertex }: {
            currentVertex: any;
            previousVertex: any;
        }) => void;
        allowTraversal?: ({ currentVertex, previousVertex, nextVertex }: {
            currentVertex: any;
            previousVertex: any;
            nextVertex: any;
        }) => boolean;
    }): void;
}
declare namespace ds {
    /**
     * 判断是否为2的指数
     *
     * @param number 整数
     */
    function isPowerOfTwo(number: number): boolean;
}
declare namespace ds {
    /**
     * 布隆过滤器 （ 在 JavaScript中 该类可由Object对象代替）
     *
     * 用于判断某元素是否可能插入
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/bloom-filter/BloomFilter.js
     * @see https://baike.baidu.com/item/%E5%B8%83%E9%9A%86%E8%BF%87%E6%BB%A4%E5%99%A8
     */
    class BloomFilter {
        private size;
        private storage;
        /**
         *
         * @param size 尺寸
         */
        constructor(size?: number);
        /**
         * 插入
         *
         * @param item 元素
         */
        insert(item: string): void;
        /**
         * 可能包含
         *
         * @param item 元素
         */
        mayContain(item: string): boolean;
        /**
         * 创建存储器
         * @param size 尺寸
         */
        createStore(size: number): {
            getValue(index: any): any;
            setValue(index: any): void;
        };
        /**
         * 计算哈希值1
         *
         * @param item 元素
         */
        hash1(item: string): number;
        /**
         * 计算哈希值2
         *
         * @param item 元素
         */
        hash2(item: string): number;
        /**
         * 计算哈希值3
         *
         * @param item 元素
         */
        hash3(item: string): number;
        /**
         * 获取3个哈希值组成的数组
         */
        getHashValues(item: string): number[];
    }
}
declare namespace ds {
    /**
     * 并查集
     *
     * 并查集是一种树型的数据结构，用于处理一些不交集（Disjoint Sets）的合并及查询问题。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/disjoint-set/DisjointSet.js
     * @see https://en.wikipedia.org/wiki/Disjoint-set_data_structure
     * @see https://www.youtube.com/watch?v=wU6udHRIkcc&index=14&t=0s&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     */
    class DisjointSet<T> {
        private items;
        /**
         * 计算键值函数
         */
        private keyCallback;
        /**
         * 构建 并查集
         * @param keyCallback 计算键值函数
         */
        constructor(keyCallback?: (value: T) => string);
        /**
         * 创建集合
         *
         * @param itemValue 项值
         */
        makeSet(itemValue: T): this;
        /**
         * 查找给出值所在集合根项键值
         *
         * @param itemValue 项值
         */
        find(itemValue: T): string;
        /**
         * 合并两个值所在的集合
         *
         * @param valueA 值a
         * @param valueB 值b
         */
        union(valueA: T, valueB: T): this;
        /**
         * 判断两个值是否在相同集合中
         *
         * @param valueA 值A
         * @param valueB 值B
         */
        inSameSet(valueA: T, valueB: T): boolean;
    }
}
declare namespace ds {
    /**
     * 并查集项
     */
    class DisjointSetItem<T> {
        /**
         * 值
         */
        value: T;
        /**
         * 计算键值函数
         */
        keyCallback: (value: T) => string;
        /**
         * 父项
         */
        parent: DisjointSetItem<T>;
        /**
         * 子项
         */
        children: any;
        /**
         * 构建 并查集 项
         *
         * @param value 值
         * @param keyCallback 计算键值函数
         */
        constructor(value: T, keyCallback?: (value: T) => string);
        /**
         * 获取键值
         */
        getKey(): string;
        /**
         * 获取根项
         */
        getRoot(): DisjointSetItem<T>;
        /**
         * 是否为根项
         */
        isRoot(): boolean;
        /**
         * 获取所有子孙项数量
         */
        getRank(): number;
        /**
         * 获取子项列表
         */
        getChildren(): any[];
        /**
         * 设置父项
         * @param parentItem 父项
         */
        setParent(parentItem: DisjointSetItem<T>): this;
        /**
         * 添加子项
         * @param childItem 子项
         */
        addChild(childItem: DisjointSetItem<T>): this;
    }
}
declare namespace ds {
    /**
    * 双向链表
    *
    * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/doubly-linked-list/DoublyLinkedList.js
    */
    class DoublyLinkedList<T> {
        /**
         * 表头
         */
        head: DoublyLinkedListNode<T>;
        /**
         * 表尾
         */
        tail: DoublyLinkedListNode<T>;
        /**
         * 比较器
         */
        private compare;
        /**
         * 构建双向链表
         *
         * @param comparatorFunction 比较函数
         */
        constructor(comparatorFunction?: CompareFunction<T>);
        /**
         * 是否为空
         */
        isEmpty(): boolean;
        /**
         * 清空
         */
        empty(): void;
        /**
         * 添加新结点到表头
         *
         * @param value 结点数据
         */
        prepend(value: T): this;
        /**
         * 添加新结点到表尾
         *
         * @param value 结点数据
         */
        append(value: T): this;
        /**
         * 删除链表中第一个与指定值相等的结点
         *
         * @param value 结点值
         */
        delete(value: T): DoublyLinkedListNode<T>;
        /**
         * 删除链表中所有与指定值相等的结点
         *
         * @param value 结点值
         */
        deleteAll(value: T): DoublyLinkedListNode<T>;
        /**
         * 查找与结点值相等的结点
         *
         * @param value 结点值
         */
        find(value: T): DoublyLinkedListNode<T>;
        /**
         * 查找与结点值相等的结点
         *
         * @param callback 判断是否为查找的元素
         */
        findByFunc(callback: (value: T) => Boolean): DoublyLinkedListNode<T>;
        /**
         * 删除表头
         */
        deleteHead(): T;
        /**
         * 删除表尾
         */
        deleteTail(): T;
        /**
         * 从数组中初始化链表
         *
         * @param values 结点值列表
         */
        fromArray(values: T[]): this;
        /**
         * 转换为数组
         */
        toArray(): T[];
        /**
         * 转换为字符串
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString?: (value: T) => string): string;
        /**
         * 反转链表
         */
        reverse(): this;
        /**
         * 核查结构是否正确
         */
        checkStructure(): boolean;
    }
}
declare namespace ds {
    /**
     * 双向链接结点
     */
    interface DoublyLinkedListNode<T> {
        /**
         * 值
         */
        value: T;
        /**
         * 上一个结点
         */
        previous: DoublyLinkedListNode<T>;
        /**
         * 下一个结点
         */
        next: DoublyLinkedListNode<T>;
    }
}
declare namespace ds {
    /**
     * 图
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/graph/Graph.js
     * @see https://en.wikipedia.org/wiki/Graph_(abstract_data_type)
     * @see https://www.youtube.com/watch?v=gXgEDyodOJU&index=9&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     * @see https://www.youtube.com/watch?v=k1wraWzqtvQ&index=10&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     */
    class Graph<T> {
        /**
         * 顶点列表
         */
        vertices: {
            [key: string]: GraphVertex<T>;
        };
        /**
         * 边列表
         */
        edges: {
            [key: string]: GraphEdge<T>;
        };
        /**
         * 是否有向
         */
        isDirected: boolean;
        /**
         * 构建图
         *
         * @param isDirected 是否有向
         */
        constructor(isDirected?: boolean);
        /**
         * 新增顶点
         *
         * @param newVertex 新顶点
         */
        addVertex(newVertex: GraphVertex<T>): this;
        /**
         * 获取顶点
         *
         * @param vertexKey 顶点键值
         */
        getVertexByKey(vertexKey: string): GraphVertex<T>;
        /**
         * 获取相邻点
         *
         * @param vertex 顶点
         */
        getNeighbors(vertex: GraphVertex<T>): GraphVertex<T>[];
        /**
         * 获取所有顶点
         */
        getAllVertices(): GraphVertex<T>[];
        /**
         * 获取所有边
         */
        getAllEdges(): GraphEdge<T>[];
        /**
         * 新增边
         *
         * @param edge 边
         */
        addEdge(edge: GraphEdge<T>): this;
        /**
         * 删除边
         *
         * @param edge 边
         */
        deleteEdge(edge: GraphEdge<T>): void;
        /**
         * 查找边
         *
         * @param startVertex 起始顶点
         * @param endVertex 结束顶点
         */
        findEdge(startVertex: GraphVertex<T>, endVertex: GraphVertex<T>): GraphEdge<T>;
        /**
         * 获取权重
         */
        getWeight(): number;
        /**
         * 反转
         */
        reverse(): this;
        /**
         * 获取所有顶点索引
         */
        getVerticesIndices(): {
            [key: string]: number;
        };
        /**
         * 获取邻接矩阵
         */
        getAdjacencyMatrix(): number[][];
        /**
         * 转换为字符串
         */
        toString(): string;
    }
}
declare namespace ds {
    /**
     * 图边
     */
    class GraphEdge<T> {
        /**
         * 起始顶点
         */
        startVertex: GraphVertex<T>;
        /**
         * 结束顶点
         */
        endVertex: GraphVertex<T>;
        /**
         * 权重
         */
        weight: number;
        /**
         * 构建图边
         * @param startVertex 起始顶点
         * @param endVertex 结束顶点
         * @param weight 权重
         */
        constructor(startVertex: GraphVertex<T>, endVertex: GraphVertex<T>, weight?: number);
        /**
         * 获取键值
         */
        getKey(): string;
        /**
         * 反转
         */
        reverse(): this;
        /**
         * 转换为字符串
         */
        toString(): string;
    }
}
declare namespace ds {
    /**
     * 图顶点
     */
    class GraphVertex<T> {
        /**
         * 值
         */
        value: T;
        /**
         * 边列表
         */
        edges: LinkedList<GraphEdge<T>>;
        /**
         * 构建图顶点
         *
         * @param value 值
         */
        constructor(value: T);
        /**
         * 新增边
         *
         * @param edge 边
         */
        addEdge(edge: GraphEdge<T>): this;
        /**
         * 删除边
         *
         * @param edge 边
         */
        deleteEdge(edge: GraphEdge<T>): void;
        /**
         * 获取相邻顶点
         */
        getNeighbors(): GraphVertex<T>[];
        /**
         * 获取边列表
         */
        getEdges(): GraphEdge<T>[];
        /**
         * 获取边的数量
         */
        getDegree(): number;
        /**
         * 是否存在指定边
         *
         * @param requiredEdge 边
         */
        hasEdge(requiredEdge: GraphEdge<T>): boolean;
        /**
         * 是否有相邻顶点
         *
         * @param vertex 顶点
         */
        hasNeighbor(vertex: GraphVertex<T>): boolean;
        /**
         * 查找边
         *
         * @param vertex 顶点
         */
        findEdge(vertex: GraphVertex<T>): GraphEdge<T>;
        /**
         * 获取键值
         */
        getKey(): string;
        /**
         * 删除所有边
         */
        deleteAllEdges(): this;
        /**
         * 转换为字符串
         *
         * @param callback 转换为字符串函数
         */
        toString(callback?: (value: T) => string): string;
    }
}
declare namespace ds {
    /**
     * 哈希表（散列表）
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/hash-table/HashTable.js
     */
    class HashTable {
        private keys;
        buckets: LinkedList<{
            key: string;
            value: any;
        }>[];
        /**
         * 构建哈希表
         * @param hashTableSize 哈希表尺寸
         */
        constructor(hashTableSize?: number);
        /**
         * 将字符串键转换为哈希数。
         *
         * @param key 字符串键
         */
        hash(key: string): number;
        /**
         * 设置值
         *
         * @param key 键
         * @param value 值
         */
        set(key: string, value: any): void;
        /**
         * 删除指定键以及对于值
         *
         * @param key 键
         */
        delete(key: string): LinkedListNode<{
            key: string;
            value: any;
        }>;
        /**
         * 获取与键对应的值
         *
         * @param key 键
         */
        get(key: string): any;
        /**
         * 是否拥有键
         *
         * @param key 键
         */
        has(key: string): any;
        /**
         * 获取键列表
         */
        getKeys(): string[];
    }
}
declare namespace ds {
    /**
     * 堆
     *
     * 最小和最大堆的父类。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/heap/Heap.js
     */
    abstract class Heap<T> {
        /**
         * 堆的数组表示。
         */
        private heapContainer;
        /**
         * 比较器
         */
        protected compare: Comparator<T>;
        /**
         * 构建链表
         *
         * @param comparatorFunction 比较函数
         */
        constructor(comparatorFunction?: CompareFunction<T>);
        /**
         * 获取左边子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getLeftChildIndex(parentIndex: number): number;
        /**
         * 获取右边子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getRightChildIndex(parentIndex: number): number;
        /**
         * 获取父结点索引
         *
         * @param childIndex 子结点索引
         */
        getParentIndex(childIndex: number): number;
        /**
         * 是否有父结点
         *
         * @param childIndex 子结点索引
         */
        hasParent(childIndex: number): boolean;
        /**
         * 是否有左结点
         *
         * @param parentIndex 父结点索引
         */
        hasLeftChild(parentIndex: number): boolean;
        /**
         * 是否有右结点
         *
         * @param parentIndex 父结点索引
         */
        hasRightChild(parentIndex: number): boolean;
        /**
         * 获取左结点
         *
         * @param parentIndex 父结点索引
         */
        leftChild(parentIndex: number): T;
        /**
         * 获取右结点
         *
         * @param parentIndex 父结点索引
         */
        rightChild(parentIndex: number): T;
        /**
         * 获取父结点
         *
         * @param childIndex 子结点索引
         */
        parent(childIndex: number): T;
        /**
         * 交换两个结点数据
         *
         * @param index1 索引1
         * @param index2 索引2
         */
        swap(index1: number, index2: number): void;
        /**
         * 查看堆顶数据
         */
        peek(): T;
        /**
         * 出堆
         *
         * 取出堆顶元素
         */
        poll(): T;
        /**
         * 新增元素
         *
         * @param item 元素
         */
        add(item: T): this;
        /**
         * 移除所有指定元素
         *
         * @param item 元素
         * @param comparator 比较器
         */
        remove(item: T, comparator?: Comparator<T>): this;
        /**
         * 查找元素所在所有索引
         *
         * @param item 查找的元素
         * @param comparator 比较器
         */
        find(item: T, comparator?: Comparator<T>): number[];
        /**
         * 是否为空
         */
        isEmpty(): boolean;
        /**
         * 转换为字符串
         */
        toString(): string;
        /**
         * 堆冒泡
         *
         * @param startIndex 堆冒泡起始索引
         */
        heapifyUp(startIndex?: number): void;
        /**
         * 堆下沉
         *
         * @param startIndex 堆下沉起始索引
         */
        heapifyDown(startIndex?: number): void;
        /**
         * 检查堆元素对的顺序是否正确。
         * 对于MinHeap，第一个元素必须总是小于等于。
         * 对于MaxHeap，第一个元素必须总是大于或等于。
         *
         * @param firstElement 第一个元素
         * @param secondElement 第二个元素
         */
        abstract pairIsInCorrectOrder(firstElement: T, secondElement: T): boolean;
    }
}
declare namespace ds {
    /**
     * 最大堆
     *
     * 所有父结点都大于子结点
     */
    class MaxHeap<T> extends Heap<T> {
        /**
         * 检查堆元素对的顺序是否正确。
         * 对于MinHeap，第一个元素必须总是小于等于。
         * 对于MaxHeap，第一个元素必须总是大于或等于。
         *
         * @param firstElement 第一个元素
         * @param secondElement 第二个元素
         */
        pairIsInCorrectOrder(firstElement: T, secondElement: T): boolean;
    }
}
declare namespace ds {
    /**
     * 最小堆
     *
     * 所有父结点都小于子结点
     */
    class MinHeap<T> extends Heap<T> {
        /**
         * 检查堆元素对的顺序是否正确。
         * 对于MinHeap，第一个元素必须总是小于等于。
         * 对于MaxHeap，第一个元素必须总是大于或等于。
         *
         * @param firstElement 第一个元素
         * @param secondElement 第二个元素
         */
        pairIsInCorrectOrder(firstElement: T, secondElement: T): boolean;
    }
}
declare namespace ds {
    /**
     * 链表
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/linked-list/LinkedList.js
     */
    class LinkedList<T> {
        /**
         * 表头
         */
        head: LinkedListNode<T>;
        /**
         * 表尾
         */
        tail: LinkedListNode<T>;
        /**
         * 比较器
         */
        private compare;
        /**
         * 构建双向链表
         *
         * @param comparatorFunction 比较函数
         */
        constructor(comparatorFunction?: CompareFunction<T>);
        /**
         * 是否为空
         */
        isEmpty(): boolean;
        /**
         * 清空
         */
        empty(): void;
        /**
         * 获取表头值
         */
        getHeadValue(): T;
        /**
         * 添加新结点到表头
         *
         * @param value 结点数据
         */
        prepend(value: T): this;
        /**
         * 添加新结点到表尾
         *
         * @param value 结点数据
         */
        append(value: T): this;
        /**
         * 删除链表中第一个与指定值相等的结点
         *
         * @param value 结点值
         */
        delete(value: T): LinkedListNode<T>;
        /**
         * 删除链表中所有与指定值相等的结点
         *
         * @param value 结点值
         */
        deleteAll(value: T): LinkedListNode<T>;
        /**
         * 查找与结点值相等的结点
         *
         * @param value 结点值
         */
        find(value: T): LinkedListNode<T>;
        /**
         * 查找与结点值相等的结点
         *
         * @param callback 判断是否为查找的元素
         */
        findByFunc(callback: (value: T) => Boolean): LinkedListNode<T>;
        /**
         * 删除表头
         *
         * 删除链表前面的元素(链表的头)并返回元素值。如果队列为空，则返回null。
         */
        deleteHead(): T;
        /**
         * 删除表尾
         */
        deleteTail(): T;
        /**
         * 从数组中初始化链表
         *
         * @param values 结点值列表
         */
        fromArray(values: T[]): this;
        /**
         * 转换为数组
         */
        toArray(): T[];
        /**
         * 转换为字符串
         *
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString?: (value: T) => string): string;
        /**
         * 反转链表
         */
        reverse(): this;
        /**
         * 核查结构是否正确
         */
        checkStructure(): boolean;
    }
}
declare namespace ds {
    /**
     * 链表结点
     */
    interface LinkedListNode<T> {
        /**
         * 值
         */
        value: T;
        /**
         * 下一个结点
         */
        next: LinkedListNode<T>;
    }
}
declare module ds {
    /**
     * 优先队列
     *
     * 与最小堆相同，只是与元素比较时不同
     * 我们考虑的不是元素的值，而是它的优先级。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/priority-queue/PriorityQueue.js
     */
    class PriorityQueue<T> extends MinHeap<T> {
        private priorities;
        constructor();
        /**
         * 新增元素
         *
         * @param item 元素
         * @param priority 优先级
         */
        add(item: T, priority?: number): this;
        /**
         * 移除元素
         *
         * @param item 元素
         * @param customFindingComparator 自定义查找比较器
         */
        remove(item: T, customFindingComparator?: Comparator<T>): this;
        /**
         * 改变元素优先级
         *
         * @param item 元素
         * @param priority 优先级
         */
        changePriority(item: T, priority: number): this;
        /**
         * 查找元素所在索引
         *
         * @param item 元素
         */
        findByValue(item: T): number[];
        /**
         * 是否拥有元素
         *
         * @param item 元素
         */
        hasValue(item: T): boolean;
        /**
         * 比较两个元素优先级
         *
         * @param a 元素a
         * @param b 元素b
         */
        comparePriority(a: T, b: T): 0 | 1 | -1;
        /**
         * 比较两个元素大小
         *
         * @param a 元素a
         * @param b 元素b
         */
        compareValue(a: T, b: T): 0 | 1 | -1;
    }
}
declare namespace ds {
    /**
     * 队列，只能从后面进，前面出
     * 使用单向链表实现
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/queue/Queue.js
     */
    class Queue<T> {
        private linkedList;
        /**
         * 构建队列
         *
         * @param comparatorFunction 比较函数
         */
        constructor();
        /**
         * 是否为空
         */
        isEmpty(): boolean;
        /**
         * 清空
         */
        empty(): void;
        /**
         * 读取队列前面的元素，但不删除它。
         */
        peek(): T;
        /**
         * 入队
         *
         * 在队列的末尾(链表的尾部)添加一个新元素。
         * 这个元素将在它前面的所有元素之后被处理。
         *
         * @param value 元素值
         */
        enqueue(value: T): this;
        /**
         * 出队
         *
         * 删除队列前面的元素(链表的头)。如果队列为空，则返回null。
         */
        dequeue(): T;
        /**
         * 转换为字符串
         *
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString?: (value: T) => string): string;
    }
}
declare namespace ds {
    /**
     * 栈
     *
     * 后进先出
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/stack/Stack.js
     */
    class Stack<T> {
        private linkedList;
        /**
         * 是否为空
         */
        isEmpty(): boolean;
        /**
         * 查看第一个元素值
         */
        peek(): T;
        /**
         * 入栈
         *
         * @param value 元素值
         */
        push(value: T): this;
        /**
         * 出栈
         */
        pop(): T;
        /**
         * 转换为数组
         */
        toArray(): T[];
        /**
         * 转换为字符串
         *
         * @param valueToString 值输出为字符串函数
         */
        toString(valueToString?: (value: T) => string): string;
    }
}
declare namespace ds {
    /**
     * 二叉树结点
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/BinaryTreeNode.js
     */
    class BinaryTreeNode<T> {
        /**
         * 左结点
         */
        left: BinaryTreeNode<T>;
        /**
         * 右结点
         */
        right: BinaryTreeNode<T>;
        /**
         * 父结点
         */
        parent: BinaryTreeNode<T>;
        /**
         * 结点值
         */
        value: T;
        /**
         * 结点比较器
         */
        nodeComparator: Comparator<BinaryTreeNode<T>>;
        meta: HashTable;
        /**
         * 构建二叉树结点
         *
         * @param value 结点值
         */
        constructor(value?: T);
        /**
         * 左结点高度
         */
        readonly leftHeight: any;
        /**
         * 右结点高度
         */
        readonly rightHeight: any;
        /**
         * 高度
         */
        readonly height: number;
        /**
         * 平衡系数
         */
        readonly balanceFactor: number;
        /**
         * 叔伯结点
         */
        readonly uncle: BinaryTreeNode<T>;
        /**
         * 设置结点值
         *
         * @param value 值
         */
        setValue(value: T): this;
        /**
         * 设置左结点
         *
         * @param node 结点
         */
        setLeft(node: BinaryTreeNode<T> | null): this;
        /**
         * 设置右结点
         *
         * @param node 结点
         */
        setRight(node: BinaryTreeNode<T> | null): this;
        /**
         * 移除子结点
         *
         * @param nodeToRemove 子结点
         */
        removeChild(nodeToRemove: BinaryTreeNode<T>): boolean;
        /**
         * 替换节点
         *
         * @param nodeToReplace 被替换的节点
         * @param replacementNode 替换后的节点
         */
        replaceChild(nodeToReplace: BinaryTreeNode<T>, replacementNode: BinaryTreeNode<T>): boolean;
        /**
         * 拷贝节点
         *
         * @param sourceNode 源节点
         * @param targetNode 目标节点
         */
        static copyNode<T>(sourceNode: BinaryTreeNode<T>, targetNode: BinaryTreeNode<T>): void;
        /**
         * 左序深度遍历
         */
        traverseInOrder(): any[];
        /**
         * 转换为字符串
         */
        toString(): string;
    }
}
declare namespace ds {
    /**
     * 二叉查找树
     *
     * 二叉查找树（英语：Binary Search Tree），也称为二叉搜索树、有序二叉树（ordered binary tree）或排序二叉树（sorted binary tree），是指一棵空树或者具有下列性质的二叉树：
     *
     * 1. 若任意节点的左子树不空，则左子树上所有节点的值均小于它的根节点的值；
     * 1. 若任意节点的右子树不空，则右子树上所有节点的值均大于它的根节点的值；
     * 1. 任意节点的左、右子树也分别为二叉查找树；
     * 1. 没有键值相等的节点。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/binary-search-tree/BinarySearchTree.js
     * @see https://en.wikipedia.org/wiki/Binary_search_tree
     * @see https://www.youtube.com/watch?v=wcIRPqTR3Kc&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8&index=9&t=0s
     */
    class BinarySearchTree<T> {
        /**
         * 根结点
         */
        root: BinarySearchTreeNode<T>;
        /**
         * 结点比较器
         */
        nodeComparator: Comparator<BinaryTreeNode<T>>;
        /**
         * 构建 二叉查找树
         *
         * @param nodeValueCompareFunction 结点值比较器
         */
        constructor(nodeValueCompareFunction?: CompareFunction<T>);
        /**
         * 插入值
         *
         * @param value 值
         */
        insert(value: T): BinarySearchTreeNode<T>;
        /**
         * 是否包含指定值
         *
         * @param value 值
         */
        contains(value: T): boolean;
        /**
         * 移除指定值
         *
         * @param value 值
         */
        remove(value: T): boolean;
        /**
         * 转换为字符串
         */
        toString(): string;
    }
}
declare namespace ds {
    /**
     * 平衡二叉树
     *
     * AVL树（以发明者Adelson-Velsky和Landis 命名）是自平衡二叉搜索树。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/tree/master/src/data-structures/tree/avl-tree
     * @see https://en.wikipedia.org/wiki/AVL_tree
     * @see https://www.tutorialspoint.com/data_structures_algorithms/avl_tree_algorithm.htm
     * @see http://btechsmartclass.com/data_structures/avl-trees.html
     */
    class AvlTree<T> extends BinarySearchTree<T> {
        /**
         * 插入元素
         *
         * @param value 元素
         */
        insert(value: T): BinarySearchTreeNode<T>;
        /**
         * 移除元素
         *
         * @param value 元素
         */
        remove(value: T): boolean;
        /**
         * 从指定结点平衡树
         *
         * @param node 结点
         */
        balance(node: BinarySearchTreeNode<T>): void;
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateLeftLeft(rootNode: BinarySearchTreeNode<T>): void;
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateLeftRight(rootNode: BinarySearchTreeNode<T>): void;
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateRightLeft(rootNode: BinarySearchTreeNode<T>): void;
        /**
         * @param {BinarySearchTreeNode} rootNode
         */
        rotateRightRight(rootNode: BinarySearchTreeNode<T>): void;
    }
}
declare namespace ds {
    /**
     * 二叉查找树结点
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/binary-search-tree/BinarySearchTreeNode.js
     */
    class BinarySearchTreeNode<T> extends BinaryTreeNode<T> {
        /**
         * 左结点
         */
        left: BinarySearchTreeNode<T>;
        /**
         * 右结点
         */
        right: BinarySearchTreeNode<T>;
        /**
         * 父结点
         */
        parent: BinarySearchTreeNode<T>;
        /**
         * 叔伯结点
         *
         * 覆盖 super.uncle 返回类型
         */
        uncle: BinarySearchTreeNode<T>;
        /**
         * 比较函数
         */
        private compareFunction;
        /**
         * 结点值比较器
         */
        private nodeValueComparator;
        /**
         * 构建二叉查找树结点
         *
         * @param value 结点值
         * @param compareFunction 比较函数
         */
        constructor(value?: T, compareFunction?: CompareFunction<T>);
        /**
         * 插入值
         *
         * @param value 值
         */
        insert(value: T): BinarySearchTreeNode<T>;
        /**
         * 查找结点
         *
         * @param value 值
         */
        find(value: T): BinarySearchTreeNode<T>;
        /**
         * 是否包含指定值
         *
         * @param value 结点值
         */
        contains(value: T): boolean;
        /**
         * 移除指定值
         *
         * @param value 结点值
         */
        remove(value: T): boolean;
        /**
         * 查找最小值
         */
        findMin(): BinarySearchTreeNode<T>;
    }
}
declare namespace ds {
    /**
     * 树状数组
     *
     * 树状数组或二叉索引树（英语：Binary Indexed Tree），又以其发明者命名为Fenwick树，最早由Peter M. Fenwick于1994年以A New Data Structure for Cumulative Frequency Tables[1]为题发表在SOFTWARE PRACTICE AND EXPERIENCE。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/tree/master/src/data-structures/tree/fenwick-tree
     * @see https://en.wikipedia.org/wiki/Fenwick_tree
     * @see https://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/
     * @see https://www.youtube.com/watch?v=CWDQJGaN1gY&index=18&t=0s&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8
     */
    class FenwickTree {
        arraySize: number;
        treeArray: number[];
        /**
         * 构建树状数组
         *
         * @param arraySize 数组尺寸
         */
        constructor(arraySize: number);
        /**
         * 增加值到指定位置上
         *
         * @param position 位置
         * @param value 值
         */
        increase(position: number, value: number): this;
        /**
         * 从索引1到位置的查询和。
         *
         * @param position 位置
         */
        query(position: number): number;
        /**
         * 从leftIndex到rightIndex的查询和。
         *
         * @param leftIndex  起始索引
         * @param rightIndex 结束索引
         */
        queryRange(leftIndex: number, rightIndex: number): number;
    }
}
declare namespace ds {
    /**
     * 红黑树
     *
     * 红黑树（英语：Red–black tree）是一种自平衡二叉查找树，是在计算机科学中用到的一种数据结构，典型的用途是实现关联数组
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/red-black-tree/RedBlackTree.js
     * @see https://en.wikipedia.org/wiki/Red%E2%80%93black_tree
     */
    class RedBlackTree<T> extends BinarySearchTree<T> {
        /**
         * 插入值
         *
         * @param value 值
         */
        insert(value: T): BinarySearchTreeNode<T>;
        /**
         * 移除指定值 （禁止使用）
         *
         * @param value 值
         */
        remove(value: T): boolean;
        /**
         * 从指定结点平衡树
         *
         * @param node 结点
         */
        balance(node: BinarySearchTreeNode<T>): void;
        /**
         * Left Left Case (p is left child of g and x is left child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        leftLeftRotation(grandParentNode: BinarySearchTreeNode<T>): BinarySearchTreeNode<T>;
        /**
         * Left Right Case (p is left child of g and x is right child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        leftRightRotation(grandParentNode: BinarySearchTreeNode<T>): BinarySearchTreeNode<T>;
        /**
         * Right Right Case (p is right child of g and x is right child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        rightRightRotation(grandParentNode: BinarySearchTreeNode<T>): BinarySearchTreeNode<T>;
        /**
         * Right Left Case (p is right child of g and x is left child of p)
         * @param {BinarySearchTreeNode|BinaryTreeNode} grandParentNode
         * @return {BinarySearchTreeNode}
         */
        rightLeftRotation(grandParentNode: BinarySearchTreeNode<T>): BinarySearchTreeNode<T>;
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {BinarySearchTreeNode}
         */
        makeNodeRed(node: BinarySearchTreeNode<T>): BinarySearchTreeNode<T>;
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {BinarySearchTreeNode}
         */
        makeNodeBlack(node: BinarySearchTreeNode<T>): BinarySearchTreeNode<T>;
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {boolean}
         */
        isNodeRed(node: BinarySearchTreeNode<T>): boolean;
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {boolean}
         */
        isNodeBlack(node: BinarySearchTreeNode<T>): boolean;
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} node
         * @return {boolean}
         */
        isNodeColored(node: BinarySearchTreeNode<T>): boolean;
        /**
         * @param {BinarySearchTreeNode|BinaryTreeNode} firstNode
         * @param {BinarySearchTreeNode|BinaryTreeNode} secondNode
         */
        swapNodeColors(firstNode: BinarySearchTreeNode<T>, secondNode: BinarySearchTreeNode<T>): void;
    }
}
declare namespace ds {
    /**
     * 线段树
     *
     * 用于高效运算指定区间 [a, b] 的 operation 运算
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/tree/segment-tree/SegmentTree.js
     * @see [Wikipedia](https://en.wikipedia.org/wiki/Segment_tree)
     * @see [YouTube](https://www.youtube.com/watch?v=ZBHKZF5w4YU&index=65&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8)
     * @see [GeeksForGeeks](https://www.geeksforgeeks.org/segment-tree-set-1-sum-of-given-range/)
     */
    class SegmentTree {
        /**
         * 输入数组
         */
        inputArray: number[];
        /**
         * 线段树数组
         */
        segmentTree: number[];
        /**
         * 运算无效值
         *
         * 对任意的 x 都满足 x == operation( x, operationFallback)
         *
         * 例如 Infinity 是 min(a,b) 的 运算无效值， 0 是 add(a,b) 的运算无效值。
         */
        operationFallback: number;
        /**
         * 操作函数
         */
        operation: (a: number, b: number) => number;
        /**
         * 构建线段树
         *
         * @param inputArray 输入数组
         * @param operation 操作函数 parent = operation(leftChild,rightChild)
         * @param operationFallback 运算无效值，对任意的 x 都满足 x == operation( x, operationFallback)
         */
        constructor(inputArray: number[], operation: (a: number, b: number) => number, operationFallback: number);
        /**
         * 初始化线段树
         *
         * @param inputArray 输入数组
         */
        initSegmentTree(inputArray: number[]): any[];
        /**
         * 构建线段树
         */
        buildSegmentTree(): void;
        /**
         * 构建线段树递归
         *
         * @param leftInputIndex 左输入索引
         * @param rightInputIndex 右输入索引
         * @param position 位置
         */
        buildTreeRecursively(leftInputIndex: number, rightInputIndex: number, position: number): void;
        /**
         * 范围查询
         *
         * @param queryLeftIndex 查询左索引
         * @param queryRightIndex 查询右索引
         */
        rangeQuery(queryLeftIndex: number, queryRightIndex: number): number;
        /**
         * 范围查询递归求值
         *
         * @param queryLeftIndex 查询左索引
         * @param queryRightIndex 查询右索引
         * @param leftIndex 输入数组段的左索引
         * @param rightIndex 输入数组段的右索引
         * @param position 二叉树的根位置
         */
        rangeQueryRecursive(queryLeftIndex: number, queryRightIndex: number, leftIndex: number, rightIndex: number, position: number): number;
        /**
         * 获取左子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getLeftChildIndex(parentIndex: number): number;
        /**
         * 获取右子结点索引
         *
         * @param parentIndex 父结点索引
         */
        getRightChildIndex(parentIndex: number): number;
    }
}
declare namespace ds {
    /**
     * 字典树  (在JavaScript中可由Object对象代替)
     *
     * 在计算机科学中，trie，又称前缀树或字典树，是一种有序树，用于保存关联数组，其中的键通常是字符串。
     *
     * @see https://github.com/trekhleb/javascript-algorithms/blob/master/src/data-structures/trie/Trie.js
     * @see [Wikipedia](https://en.wikipedia.org/wiki/Trie)
     * @see [YouTube](https://www.youtube.com/watch?v=zIjfhVPRZCg&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8&index=7&t=0s)
     */
    class Trie {
        head: TrieNode;
        constructor();
        /**
         * @param {string} word
         * @return {Trie}
         */
        addWord(word: string): this;
        /**
         * @param {string} word
         * @return {Trie}
         */
        deleteWord(word: string): this;
        /**
         * @param {string} word
         * @return {string[]}
         */
        suggestNextCharacters(word: string): string[];
        /**
         * Check if complete word exists in Trie.
         *
         * @param {string} word
         * @return {boolean}
         */
        doesWordExist(word: string): boolean;
        /**
         * @param {string} word
         * @return {TrieNode}
         */
        getLastCharacterNode(word: string): TrieNode;
    }
}
declare namespace ds {
    /**
     * 单词查找树结点
     */
    class TrieNode {
        character: string;
        isCompleteWord: boolean;
        children: HashTable;
        /**
         * @param {string} character
         * @param {boolean} isCompleteWord
         */
        constructor(character: string, isCompleteWord?: boolean);
        /**
         * @param {string} character
         * @return {TrieNode}
         */
        getChild(character: string): any;
        /**
         * @param {string} character
         * @param {boolean} isCompleteWord
         * @return {TrieNode}
         */
        addChild(character: string, isCompleteWord?: boolean): any;
        /**
         * @param {string} character
         * @return {TrieNode}
         */
        removeChild(character: string): this;
        /**
         * @param {string} character
         * @return {boolean}
         */
        hasChild(character: string): any;
        /**
         * Check whether current TrieNode has children or not.
         * @return {boolean}
         */
        hasChildren(): boolean;
        /**
         * @return {string[]}
         */
        suggestChildren(): string[];
        /**
         * @return {string}
         */
        toString(): string;
    }
}
declare namespace ds {
    type CompareFunction<T> = (a: T, b: T) => number;
    /**
     * 比较器
     */
    class Comparator<T> {
        /**
         * 默认比较函数。只能处理 a和b 同为string或number的比较。
         *
         * @param a 比较值a
         * @param b 比较值b
         */
        static defaultCompareFunction(a: string | number, b: string | number): 0 | 1 | -1;
        private compare;
        /**
         * 构建比较器
         * @param compareFunction 比较函数
         */
        constructor(compareFunction?: CompareFunction<T>);
        /**
         * 检查 a 是否等于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        equal(a: T, b: T): boolean;
        /**
         * 检查 a 是否小于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        lessThan(a: T, b: T): boolean;
        /**
         * 检查 a 是否大于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        greaterThan(a: T, b: T): boolean;
        /**
         * 检查 a 是否小于等于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        lessThanOrEqual(a: T, b: T): boolean;
        /**
         * 检查 a 是否大于等于 b 。
         *
         * @param a 值a
         * @param b 值b
         */
        greaterThanOrEqual(a: T, b: T): boolean;
        /**
         * 反转比较函数。
         */
        reverse(): void;
    }
}
//# sourceMappingURL=ds.d.ts.map